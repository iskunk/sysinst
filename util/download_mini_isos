#!/bin/bash
#
# This script securely downloads the netboot/mini.iso file for Debian
# or Ubuntu, for i386 and amd64
#
# (NOTE: Ubuntu no longer provides a mini.iso file for newer releases)
#
# usage: download_mini_isos [DISTRO [RELEASE]]
#
# e.g. download_mini_isos debian stable
#      download_mini_isos ubuntu trusty
#

APT_SERVER="http://apt.example.com"

arch_list="i386 amd64"
distro=debian

[ -n "$1" ] && distro="$1"

case "$distro" in
	debian) release=stable ;;
	ubuntu) release=trusty ;;
	*) echo "$0: unknown distribution \"$distro\""; exit 1 ;;
esac

[ -n "$2" ] && release="$2"

prefix=$APT_SERVER/$distro/dists/$release
wget="wget --max-redirect=0 --no-verbose"
PATH=/bin:/usr/bin

set -e

rm -f tmp.*

case "$distro" in

	debian)

	$wget -O tmp.Release     $prefix/Release
	$wget -O tmp.Release.gpg $prefix/Release.gpg

	gpgv --keyring=/usr/share/keyrings/debian-archive-keyring.gpg tmp.Release.gpg tmp.Release

	for arch in $arch_list
	do
		egrep "^ [0-9a-f]{64} +[0-9]+ main/installer-$arch/current/images/SHA256SUMS\$" tmp.Release \
		| sed -r 's, +[0-9]+ +main/installer-([0-9a-z]+)/.*/,  tmp.\1-,'
	done >tmp.Release-sums

	test -s tmp.Release-sums

	for arch in $arch_list
	do
		$wget -O tmp.$arch-SHA256SUMS $prefix/main/installer-$arch/current/images/SHA256SUMS
	done

	sha256sum -c tmp.Release-sums

	;;

	ubuntu)

	for arch in $arch_list
	do
		$wget -O tmp.$arch-SHA256SUMS     $prefix/main/installer-$arch/current/images/SHA256SUMS
		$wget -O tmp.$arch-SHA256SUMS.gpg $prefix/main/installer-$arch/current/images/SHA256SUMS.gpg

		gpgv --keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg tmp.$arch-SHA256SUMS.gpg tmp.$arch-SHA256SUMS
	done

	;;
esac

for arch in $arch_list
do
	fgrep 'netboot/mini.iso' tmp.$arch-SHA256SUMS | sed "s,netboot/,tmp.$distro-$release-$arch.,"
done >tmp.images-sums

for arch in $arch_list
do
	$wget -O tmp.$distro-$release-$arch.mini.iso $prefix/main/installer-$arch/current/images/netboot/mini.iso
done

sha256sum -c tmp.images-sums

for arch in $arch_list
do
	mv -f tmp.$distro-$release-$arch.mini.iso $distro-$release-$arch.mini.iso
done

echo
ls -l $distro-$release-*.mini.iso

# EOF
