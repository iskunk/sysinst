#!/bin/bash

IMAGE_DIR=/sysinst/image

set -e

umask 022

export LANG=C
export LC_ALL=C

if [ $# -ne 1 ]
then
	echo "usage: $0 { IMAGEDIR | IMAGENAME }"
	echo "(e.g. \"$0 ubuntu-afs\", \"$0 /path/to/ubuntu-afs\")"
	exit 1
fi

if [ "$(id -u)" -ne 0 ]
then
	echo 'This script must be executed as root'
	exit 1
fi

# PATH sanity check
#
sane=yes
for dir in /bin /sbin /usr/bin /usr/sbin
do
	if ! echo ":$PATH:" | grep -q ":$dir:"
	then
		echo "$0: error: directory \"$dir\" not in PATH"
		sane=no
	fi
done
test $sane = yes || exit

imagedir=

for dir in "$1" "$IMAGE_DIR/$1"
do
	test -f "$dir/root.tar.xz" || continue
	imagedir=$(cd "$dir" && pwd)
	break
done

if [ -z "$imagedir" ]
then
	echo "$0: \"$1\" does not name a valid image"
	exit 1
fi

imagename=$(basename "$imagedir")

if [ -n "$SYSINST_VERIFIED" -a -d "$SYSINST_VERIFIED" ]
then
	imagedir_verified="$SYSINST_VERIFIED/image/$imagename"
else
	imagedir_verified="$imagedir"
fi

affirmative()
{
	echo

	while :
	do
		printf "%s [y/n] " "$1"
		read reply

		case "$reply" in
			[Yy]*) echo; return 0 ;;
			[Nn]*) echo; return 1 ;;
			*) echo 'Please answer "y" or "n".' ;;
		esac
	done
}

is_zfs()
{
	case "$1" in ZFS=*) return 0 ;; esac
	return 1
}

zfs_import()
{
	local fs="$1"

	local pool=$(echo "$fs" | sed 's/^ZFS=//; s!/.*!!')

	if ! zpool list "$pool" >/dev/null 2>&1
	then
		(set -x; zpool import -N -f "$pool")
	fi
}

zfs_export()
{
	local fs="$1"

	local pool=$(echo "$fs" | sed 's/^ZFS=//; s!/.*!!')

	(set -x; zpool export "$pool")
}

zfs_check_mountpoint()
{
	local fs="$1"
	local mountpoint="$2"

	fs=$(echo "$fs" | sed 's/^ZFS=//')
	local zfs_mnt=$(zfs list -H -o mountpoint "$fs")

	if [ "_$mountpoint" != "_$zfs_mnt" ]
	then
		echo "error: ZFS filesystem \"$fs\" does not have required mountpoint \"$mountpoint\""
		cleanup
		exit 1
	fi
}

zfs_mount_recursive()
{
	local fs="$1"
	local base="$2"

	fs=$(echo "$fs" | sed 's/^ZFS=//')

	local name= mnt=

	zfs list -H -r -o name,mountpoint "$fs" \
	| while read name mnt
	do
		local dir="$base$mnt"
		test -d "$dir" || mkdir -p "$dir"
		mount -t zfs -o zfsutil "$name" "$dir" || return
	done
}

zfs_rollback_recursive()
{
	local fs="$1"
	local snapname="$2"

	fs=$(echo "$fs" | sed 's/^ZFS=//')
	local fs2=

	zfs list -H -r -o name "$fs" \
	| while read fs2
	do
		(set -x; zfs rollback -R -f "$fs2@$snapname")
	done
}

cleanup()
{
	local arg="$1"

	for mnt in \
		$(test "_$arg" = _partial || echo /config) \
		/target/home \
		/target/boot/efi \
		/target/boot \
		/target/dev/pts \
		/target/dev \
		/target/proc \
		/target/run \
		/target/sys \
		/target
	do
		umount -f "$mnt" 2>/dev/null || :
	done

	for dev in \
		$(test "_$arg" = _partial || echo \
			$config_partition \
			$misc_partition \
			$home_partition \
			$scratch_partition \
		) \
		$efi_partition \
		$boot_partition \
		$afscache_partition \
		$rootfs_partition
	do
		! is_zfs $dev || continue
		test -b $dev || continue
		umount -f $dev 2>/dev/null || :
	done

	mount -t zfs \
	| awk '{ print $3 }' \
	| tac \
	| while read mnt
	do
		mountpoint -q "$mnt" || continue
		umount -f "$mnt" 2>/dev/null || :
	done

	for dev in $swap_partitions
	do
		swapoff $dev 2>/dev/null || :
	done
}

####
## Find the disk partition containing the system image config file
##

# Note: config_partition can be set in the environment to override the
# partition found here

if [ -z "$config_partition" ]
then
	config_partition=$(fdisk -x 2>/dev/null \
		| grep '^/dev/' \
		| grep -E '( 6 FAT16 | [Ss]ysinst config\b)' \
		| head -1 \
		| awk '{ print $1 }')
fi

if [ -z "$config_partition" ]
then
	config_partition=$(fdisk -l 2>/dev/null \
		| grep '^/dev/' \
		| grep -E ' 6 FAT16$' \
		| head -1 \
		| awk '{ print $1 }')
fi

if [ -z "$config_partition" ]
then
	cat <<EOF
This system does not appear to have a configuration partition.

The partition must have an MBR partition type of 6h (FAT16) or a GPT
partition name/label of "Sysinst config". (Alternately, you can set
the environment variable "config_partition" to the partition device.)
EOF
	cleanup
	exit 1
fi

####
## Read in the configuration file
##

test -d /config || mkdir /config

if ! mount | grep -q "^$config_partition on /config type "
then
	mount -o ro "$config_partition" /config
fi

x=/config/system.conf
if [ -f $x ]
then
	fs_type=ext4	# default

	. $x
else
	echo "error: $config_partition does not contain a system configuration file" | fmt
	cleanup
	exit 1
fi

# Export variables for the benefit of external preinst/postinst scripts
#
export imagedir imagename
export hostname domain role primary_user
export efi_partition swap_partitions misc_partition boot_partition
export afscache_partition rootfs_partition home_partition scratch_partition
export fs_type

####
## Pre-installation setup (to bring up RAID arrays, etc.)
##

x=/config/preinst
if [ -x "$x" ]
then
	echo 'Running machine-specific pre-installation script ...'
	(cd /config && exec "$x")
	echo 'Done.'
fi

####
## Do some basic validation
##

case "$role" in
	server|workstation) ;;
	*) echo "error: invalid role \"$role\""; cleanup; exit 1 ;;
esac

for dev in \
	$efi_partition \
	$swap_partitions \
	$misc_partition \
	$boot_partition \
	$afscache_partition
do
	if [ ! -b "$dev" ]
	then
		echo "error: $dev is not a block device"
		cleanup
		exit 1
	fi
done

for dev in \
	$rootfs_partition \
	$home_partition \
	$scratch_partition
do
	if [ ! -b "$dev" ] && ! is_zfs "$dev"
	then
		echo "error: $dev is neither a block device nor ZFS filesystem"
		cleanup
		exit 1
	fi
done

if [ -z "$boot_partition" ] && is_zfs "$rootfs_partition"
then
	echo 'error: root filesystem on ZFS requires a separate /boot partition'
	cleanup
	exit 1
fi

if [ "_$reformat_scratch" = _yes ]
then
	echo 'error: reformat_scratch=yes is no longer honored'
	cleanup
	exit 1
fi

have_efi_part=$(test -n "$efi_partition" && echo yes || echo no)
is_efi_sys=$(test -d /sys/firmware/efi/efivars && echo yes || echo no)
sb_enabled=$(mokutil --sb-state 2>&1 | fgrep -q enabled && echo yes || echo no)

case "$have_efi_part,$is_efi_sys,$sb_enabled" in
	no,no,no | yes,yes,*) ;;

	*,no,yes)
	echo 'error: impossible case'
	cleanup
	exit 1
	;;

	no,yes,yes)
	echo 'error: this is an EFI-based system with Secure Boot enabled,'
	echo 'and yet no EFI partition is defined in the system config!'
	cleanup
	exit 1
	;;

	no,yes,no)
	echo 'warning: this is an EFI-based system (without Secure Boot),'
	echo 'and yet no EFI partition is defined in the system config;'
	echo 'a PC/BIOS bootloader will be installed, which may fail to boot the system'
	;;

	yes,no,no)
	echo 'warning: this is a PC/BIOS system, and yet an EFI partition'
	echo 'is defined in the system config; an EFI bootloader will be'
	echo 'installed, which may fail to boot the system'
	;;
esac

# Don't unmount the config partition yet; we still need to copy out the SSH
# host keys and potentially other files

####
## Dereference symlinks (e.g. /dev/disk/by-label/rootfs -> /dev/hda7)
##

get_real() { (test -b "$1" && readlink -f "$1") || (is_zfs "$1" && echo "$1") || true; }

efi_partition_real=$(get_real "$efi_partition")
swap_partitions_real=$(for p in $swap_partitions; do get_real "$p"; done)
misc_partition_real=$(get_real "$misc_partition")
boot_partition_real=$(get_real "$boot_partition")
afscache_partition_real=$(get_real "$afscache_partition")
rootfs_partition_real=$(get_real "$rootfs_partition")
home_partition_real=$(get_real "$home_partition")
scratch_partition_real=$(get_real "$scratch_partition")

####
## Ask for confirmation
##

rootfs_fs_type=$fs_type
need_zfs_support=no

if is_zfs "$rootfs_partition"
then
	zfs_import "$rootfs_partition"
	zfs_check_mountpoint "$rootfs_partition" /
	rootfs_fs_type=zfs
	need_zfs_support=yes
fi
if [ -n "$home_partition" ] && is_zfs "$home_partition"
then
	zfs_import "$home_partition"
	zfs_check_mountpoint "$home_partition" /home
fi
if [ -n "$scratch_partition" ] && is_zfs "$scratch_partition"
then
	zfs_import "$scratch_partition"
	zfs_check_mountpoint "$scratch_partition" /scratch
fi

cat <<EOF

System image installer
======================

Hostname/domain...: $hostname / $(echo $domain | grep . || echo '(none)')

Image to install..: $imagename (compressed size: $(ls -lh $imagedir/root.tar.xz | awk '{print $5}'))

Disk partitions:
+ config..........: $config_partition
EOF

if [ -n "$efi_partition" ]
then
	echo "+ EFI system......: $efi_partition_real (F, fat32)"
fi

if echo "$swap_partitions" | fgrep -q /
then
	echo "+ swap............: $swap_partitions_real (F, swap)"
fi

if [ -n "$misc_partition" ]
then
	echo "+ misc............: $misc_partition_real"
fi

if [ -n "$boot_partition" ]
then
	echo "+ boot............: $boot_partition_real (F, ext4)"
fi

if [ -n "$afscache_partition" ]
then
	echo "+ AFS cache.......: $afscache_partition_real (F, ext2)"
fi

cat <<EOF
+ rootfs..........: $rootfs_partition_real (F, $rootfs_fs_type)
EOF

if [ -n "$home_partition" ]
then
	echo "+ home............: $home_partition_real"
fi

if [ -n "$scratch_partition" ]
then
	echo "+ scratch.........: $scratch_partition_real"
fi

echo
echo 'NOTE: The above partitions marked with "(F, __)" will be reformatted.'
printf '\033[01;05;37;41m!!!! ALL DATA ON THESE PARTITIONS WILL BE DESTROYED !!!!\033[00m\n'

echo
echo 'This is your only warning.'

if ! affirmative 'Proceed with installation?'
then
	echo 'Installation aborted.'
	cleanup
	exit 0
fi

####
## Make filesystems
##

cleanup partial

echo 'Creating filesystems ...'

get_badblocks()
{
	local device="$1"

	local id=
	for id in $(cd /dev/disk/by-id && echo *)
	do
		local id_dev=$(readlink -f /dev/disk/by-id/$id)
		test "_$id_dev" = "_$device" || continue
		local file=/config/badblocks/$id.txt
		if [ -f $file ]
		then
			echo "-l $file"
			return
		fi
	done
}

run_mkfs_btrfs()
{
	label="$1"
	device="$2"

	# use --leafsize 16384 --nodesize 16384 ?

	(set -x; mkfs.btrfs -f -L "$label" "$device")

	# Give udev a moment to (re)set the label in /dev/disk/by-label
	#
	sleep 1
}

run_mkfs_ext2()
{
	label="$1"
	device="$2"

	# Do not reserve filesystem blocks for the super-user on any
	# filesystem other than the root fs (the default of 5% needlessly
	# wastes a lot of space!)
	#
	reserved=
	test "$label" = rootfs || reserved='-m 0'

	badblocks=$(get_badblocks "$device")

	(set -x; mkfs.ext2 -q $reserved -F -L "$label" $badblocks "$device")

	# Give udev a moment to (re)set the label in /dev/disk/by-label
	#
	sleep 1
}

run_mkfs_ext3()
{
	label="$1"
	device="$2"

	reserved=
	test "$label" = rootfs || reserved='-m 0'

	badblocks=$(get_badblocks "$device")

	(set -x; mkfs.ext3 -q $reserved -F -L "$label" $badblocks "$device")

	# Give udev a moment to (re)set the label in /dev/disk/by-label
	#
	sleep 1

	(set -x; tune2fs -o user_xattr "$device" >/dev/null)
}

run_mkfs_ext4()
{
	label="$1"
	device="$2"

	reserved=
	test "$label" = rootfs || reserved='-m 0'

	badblocks=$(get_badblocks "$device")

	(set -x; mkfs.ext4 -q $reserved -F -L "$label" $badblocks "$device")

	# Give udev a moment to (re)set the label in /dev/disk/by-label
	#
	sleep 1

	(set -x; tune2fs -o user_xattr "$device" >/dev/null)

	# Disable some newer filesystem features that are incompatible
	# with GRUB or fsck in Debian bullseye or Ubuntu jammy:
	#
	# https://bugs.debian.org/1030846
	# https://bugs.debian.org/1031622
	#
	for feat in \
		metadata_csum_seed \
		orphan_file
	do
		if tune2fs -l "$device" \
		   | grep '^Filesystem features:' \
		   | grep -Eq "\\b$feat\\b"
		then
			(set -x; tune2fs -O "^$feat" "$device" >/dev/null)
		fi
	done
}

run_mkfs()
{
	label="$1"
	device="$2"

	case "$label,$device" in
		rootfs,ZFS=*)
		zfs_rollback_recursive "$device" zero
		return
		;;
	esac

	case "$fs_type" in
		btrfs) run_mkfs_btrfs "$@" ;;
		ext2) run_mkfs_ext2 "$@" ;;
		ext3) run_mkfs_ext3 "$@" ;;
		ext4) run_mkfs_ext4 "$@" ;;
		*) echo "unknown filesystem type \"$fs_type\""; exit 1 ;;
	esac
}

if [ -n "$efi_partition" ]
then
	(set -x; mkfs.fat -F 32 -n EFI_SYSTEM $efi_partition_real)
fi

i=0
for part in $swap_partitions_real ''
do
	test -n "$part" || continue
	(set -x; mkswap -L swap$i $part)
	i=$((i + 1))
done

if [ -n "$misc_partition" ]
then
	echo "(Not [re]formatting misc partition $misc_partition_real)"
fi

if [ -n "$boot_partition" ]
then
	run_mkfs_ext4 boot $boot_partition_real
fi

run_mkfs rootfs $rootfs_partition_real

if [ -n "$afscache_partition" ]
then
	# The AFS cache uses ext2 (two) for performance reasons
	#
	run_mkfs_ext2 afscache $afscache_partition_real
fi

if [ -n "$home_partition" ]
then
	echo "(Not [re]formatting home partition $home_partition_real)"
fi

if [ -n "$scratch_partition" ]
then
	echo "(Not [re]formatting scratch partition $scratch_partition_real)"
fi

echo

####
## Copy image
##

test -d /target || mkdir /target

if is_zfs "$rootfs_partition"
then
	zfs_mount_recursive "$rootfs_partition" /target
else
	mount "$rootfs_partition" /target
fi

if [ -n "$boot_partition" ]
then
	mkdir /target/boot
	mount $boot_partition /target/boot
fi

echo "Unpacking image \"$imagename\" onto $rootfs_partition_real ..."

# Make use of Bash process substitution
#
# Note: The "cat >/dev/null" is to prevent tar(1) from causing a SIGPIPE
#
(cd /target && tee --output-error=warn \
	>(sha512sum >/tmp/root-sum.txt) \
	| (TIMEFORMAT='Done, took %1R seconds.'; \
		time sh -ec 'tar xJf -; cat >/dev/null; sync')) \
	<"$imagedir/root.tar.xz"

sum_orig=$(awk '/root\.tar\.xz/{print $1}' "$imagedir_verified/SHA512SUMS")
sum_calc=$(awk '{print $1}' /tmp/root-sum.txt)
if [ "_$sum_calc" = "_$sum_orig" ]
then
	echo 'Image SHA-512 sums match.'
else
	echo 'Image is CORRUPTED!'
	mv -f /target/boot /target/boot.CORRUPT
	cleanup
	exit 1
fi

echo

####
## Mount /dev, /proc, /run, and /sys to help programs inside the chroot
##

chroot_mount()
{
	local dir="$1"
	shift

	if [ -d "/target$dir" ]
	then
		mount "$@" "/target$dir"
	fi
}

chroot_mount /dev	-t devtmpfs udev
chroot_mount /dev/pts	-t devpts devpts
chroot_mount /proc	-t proc proc
chroot_mount /run	-t tmpfs -o size=8M,mode=755 tmpfs
chroot_mount /sys	-t sysfs sysfs

# Also mount the EFI System Partition if applicable
#
if [ -n "$efi_partition" ]
then
	mkdir -p /target/boot/efi
	(set -x; mount "$efi_partition_real" /target/boot/efi)
fi

if [ -n "$scratch_partition" -a -L /target/scratch ]
then
	(set -x; rm /target/scratch)

	# ZFS doesn't need an existing directory to mount
	#
	is_zfs "$scratch_partition" || (set -x; mkdir /target/scratch)
fi

if [ $need_zfs_support = yes ]
then
	if [ -f /target/usr/share/initramfs-tools/hooks/zfs -a \
	     -f /target/lib/systemd/system/zfs.target ]
	then
		(set -x; chroot /target systemctl enable zfs.target)
	else
		echo 'error: root filesystem is on ZFS, but image lacks ZFS support'
		cleanup
		exit 1
	fi
fi

####
## Generate fstab
##

echo 'Generating /target/etc/fstab ...'

( ####

get_fs_type()
{
	if is_zfs "$1"
	then
		echo zfs
		return
	fi
	blkid -p -s TYPE -o value "$1"
}

get_fs_options()
{
	case "$1" in
		btrfs)   echo 'compress=lzo' ;;
		ext2)    echo 'noatime,nodiratime' ;;
		ext[34]) echo 'commit=60' ;;
		zfs)     echo 'defaults' ;;
	esac
}

cat <<EOF
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a device;
# this may be used with UUID= as a more robust way to name devices that
# works even if disks are added and removed. See fstab(5).
#
# <file system>	<mount point>	<type>	<options>	<dump>	<pass>
proc		/proc		proc	nodev,noexec,nosuid	0	0
EOF

if [ -d /target/root/private/config ]
then
	label=config
	test -L /dev/disk/by-label/CONFIG && label=CONFIG
	cat <<EOF
LABEL=$label	/root/private/config	vfat	noauto	0	2
EOF
fi

[ -z "$efi_partition" ] || cat <<EOF
LABEL=EFI_SYSTEM	/boot/efi	vfat	umask=0077	0	2
EOF

if [ -n "$misc_partition" ]
then
	mkdir -p /target/boot/misc
	fs=$(get_fs_type $misc_partition_real)
	opts=$(get_fs_options $fs)
	cat <<EOF
LABEL=misc	/boot/misc		$fs	$opts	0	2
EOF
fi

if [ -n "$boot_partition" ]
then
	fs=$(get_fs_type $boot_partition_real)
	opts=$(get_fs_options $fs)
	cat <<EOF
LABEL=boot	/boot		$fs	$opts	0	2
EOF
fi

for part in $swap_partitions ''
do
	test -n "$part" || continue
	cat <<EOF
$part	none		swap	sw		0	0
EOF
done

if [ -n "$afscache_partition" ]
then
	c=$(test -d /target/var/cache/openafs || echo '#')
	fs=$(get_fs_type $afscache_partition_real)
	opts=$(get_fs_options $fs)

	cat <<EOF
$c$afscache_partition	/var/cache/openafs	$fs	$opts	0	2
EOF
fi

if ! is_zfs "$rootfs_partition"
then
	fs=$(get_fs_type $rootfs_partition_real)
	opts=$(get_fs_options $fs)
	cat <<EOF
$rootfs_partition	/		$fs	$opts,errors=remount-ro	0	1
EOF
fi

if [ -n "$home_partition" ] && ! is_zfs "$home_partition"
then
	fs=$(get_fs_type $home_partition_real)
	opts=$(get_fs_options $fs)

	cat <<EOF
$home_partition	/home		$fs	$opts	0	2
EOF
fi

if [ -n "$scratch_partition" ] && ! is_zfs "$scratch_partition"
then
	c=$(test -d /target/scratch || echo '#')
	fs=$(get_fs_type $scratch_partition_real)
	opts=$(get_fs_options $fs)

	cat <<EOF
$c$scratch_partition	/scratch	$fs	$opts,nosuid	0	2
EOF
fi

opts=
if [ "$role" = workstation ]
then
	opts=',user'
fi

if grep -q ' fd0 ' /proc/diskstats
then
	cat <<EOF
/dev/fd0	/media/floppy0	auto	rw$opts,noauto	0	0
EOF
fi

if grep -q ' sr0 ' /proc/diskstats
then
	cat <<EOF
/dev/cdrom	/media/cdrom0	udf,iso9660	ro$opts,noauto,exec,utf8	0	0
EOF
fi

# Append fstab addenda file(s), if present
#
for x in \
	/target/root/fstab.add \
	/config/fstab.add
do
	if [ -s $x ]
	then
		echo
		cat $x
	fi
done

) >/target/etc/fstab ####

echo

####
## Populate image from config partition
##

echo 'Copying files from config partition:'

(cd /config && find etc root usr/local var/local -type f 2>/dev/null | sort) \
| while read file
do
	dir=$(dirname $file)

	case "$file" in
		# Do not copy these files
		#
		*.noinst | *~ | \
		etc/ssh/gen.*)
		continue
		;;

		# These files must be present in the image already
		# in order to be updated
		#
		etc/crypttab | \
		etc/mailname | \
		etc/mdadm/* | \
		etc/xrdp/*.pem)
		[ -f /target/$file ] || continue
		;;

		# These files must be deleted before updating,
		# as they could be a symlink in the image
		#
		etc/resolv.conf)
		rm -f /target/$file
		;;

		# If the directory of the file is absent in the image, but
		# the dir one level up is present, then create the former
		#
		etc/openafs/server/* | \
		root/.ssh/*)
		dir_parent=$(dirname $dir)
		[ ! -d /target/$dir_parent -o -d /target/$dir ] \
		|| mkdir /target/$dir
		;;
	esac

	# Set appropriate file permissions (if not 644)
	#
	mode=
	case "$file" in
		*/.ssh/id_*.pub)
		;;

		etc/NetworkManager/system-connections/*.nmconnection | \
		etc/openafs/server/KeyFile | \
		etc/ssh/ssh_host_*_key | \
		etc/wireguard/*.private.key | \
		etc/xrdp/key.pem | \
		*/.ssh/id_*)
		mode=600
		;;

		etc/cron.daily/* | \
		etc/cron.hourly/* | \
		etc/cron.monthly/* | \
		etc/cron.weekly/* | \
		etc/grub.d/* | \
		*/bin/* | \
		*/sbin/*)
		mode=755
		;;
	esac

	[ -d /target/$dir ] || continue

	case "$file" in
		*.symlink)
		link="${file%%.symlink}"
		link_target=$(cat "/config/$file")
		echo "  $link -> $link_target"
		ln -s "$link_target" "/target/$link"
		;;

		*)
		echo "  $file${mode:+ (mode $mode)}"
		cat /config/$file >/target/$file
		[ -z "$mode" ] || chmod $mode /target/$file
		;;
	esac
done

echo

####
## Basic hostname configuration
##

# Fully-qualified domain name
#
fqdn="$hostname"
test -z "$domain" || fqdn="$fqdn.$domain"

x=etc/hostname
if [ ! -f /config/$x ]
then
	echo "Updating /target/$x ..."
	echo "$hostname" >/target/$x
fi

if [ -n "$domain" ]
then
	n="$fqdn $hostname"
else
	n="$hostname"
fi

x=etc/hosts
if fgrep -q 127.0.1.1 /target/$x && [ ! -f /config/$x ]
then
	echo "Updating /target/$x ..."
	perl -pi -e 's/^(127\.0\.1\.1)\b.*$/$1\t'"$n"'/' /target/$x
fi

# NOTE: No need to edit /etc/motd (if present) as it is regenerated
# automatically on boot

####
## Networking
##

# Applicable:
#   etc/network/interfaces.d/*
#   etc/NetworkManager/conf.d/*
#   etc/resolv.conf

# Delete the generic ifupdown config iff other config(s) were provided,
# and none of those is named "generic"
#
dir=etc/network/interfaces.d
if ! echo /config/$dir/* | fgrep -q '*' && \
   [ -f /target/$dir/generic -a ! -f /config/$dir/generic ]
then
	echo "Deleting /target/$dir/generic ..."
	rm -f /target/$dir/generic
fi

x=etc/resolv.conf
if [ ! -f /config/$x ]
then
	# Not a great solution, but we need a working
	# resolv.conf inside the chroot
	#
	echo "Copying /$x from host system ..."
	rm -f /target/$x	# may be a symlink
	(echo '# sysinst-install-image-temp'; grep -v '^#' /$x) >/target/$x
fi

####
## Mail
##

# Applicable:
#   etc/mailname
#   etc/ssmtp/ssmtp.conf

# Note that the ssmtp debconf scripts treat the config file as
# authoritative, and update the debconf database from that, rather than
# the other way around.
#
# Also, /etc/mailname is never updated (only created). I'd file a bug
# report with Debian, but for all practical purposes, ssmtp appears to
# be unmaintained.
#
x=etc/mailname
if [ -f /target/$x -a ! -f /config/$x ]
then
	echo "Updating /target/$x ..."
	echo "$fqdn" >/target/$x
fi

x=etc/ssmtp/ssmtp.conf
if [ -f /target/$x ]
then
	if [ ! -f /config/$x ]
	then
		echo "Updating /target/$x ..."
		perl -pi \
			-e "s/^(rewriteDomain)=.*/\$1=$fqdn/;" \
			-e "s/^(hostname)=.*/\$1=$fqdn/" \
			/target/$x
	fi

	chroot /target dpkg-reconfigure -phigh ssmtp
fi

####
## GRUB
##

x=/target/etc/default/grub
if [ -f $x ]; then ####

if [ -f /config/etc/crypttab ] && \
   ! grep -q '^GRUB_CMDLINE_LINUX_DEFAULT=.*splash' $x
then
	echo 'Adding "splash" boot option for a nicer disk passphrase prompt ...'

	perl -pi \
		-e 'if (/^GRUB_CMDLINE_LINUX_DEFAULT=/)' \
		-e '{ s/"$/ splash"/; s/=" /="/ }' \
		$x
fi

# Make sure that this gets regenerated
#
: >/target/boot/grub/grub.cfg

if [ -z "$efi_partition" ]
then
	# No EFI system partition defined; set up PC/BIOS bootloader

	if [ -f /target/var/lib/dpkg/info/grub-pc.list ]
	then
		# Avoid the "GRUB was previously installed to a disk that
		# is no longer present" warning
		#
		printf 'grub-pc\tgrub-pc/install_devices\tmultiselect\t%s\n' '' \
		| chroot /target debconf-set-selections

		x=/target/boot/grub/core.img
		if [ ! -f $x ]
		then
			# Needed in order for grub-pc.postinst to run
			# grub-install(8); will be deleted by same
			#
			touch $x
		fi

		(set -x; chroot /target dpkg-reconfigure grub-pc)

		# Sanity check
		#
		if [ -f $x -o ! -d /target/boot/grub/i386-pc ]
		then
			echo 'error: grub-pc install did not proceed as expected'
			cleanup
			exit 1
		fi
	fi
else # have efi_partition
	#
	# Set up EFI bootloader

	# Note: The logic in grub-efi-amd64.postinst only runs
	# grub-install(8) if a certain condition is true, and it
	# differs per distribution:
	#   Debian: EFI bootloader-ID subdirectory is present
	#   Ubuntu: /boot/grub/x86_64-efi/core.efi is present
	# References:
	#   https://bugs.debian.org/990497
	#   https://bugs.debian.org/1006541
	#   https://bugs.launchpad.net/bugs/1783044 (fixed in Ubuntu)
	#   https://bugs.launchpad.net/bugs/1879558
	#
	id=$(chroot /target lsb_release -is 2>/dev/null | tr A-Z a-z)
	test -n "$id" || id=debian
	x=/target/boot/efi
	mountpoint -q $x && mkdir -p $x/EFI/$id
	x=/target/boot/grub/x86_64-efi
	mkdir $x
	touch $x/core.efi

	if [ -f /target/var/lib/dpkg/info/grub-efi-amd64.list ]
	then
		(set -x; chroot /target dpkg-reconfigure grub-efi-amd64)

	elif [ -f /target/var/lib/dpkg/info/grub-efi-amd64-signed.postinst ]
	then
		(set -x; chroot /target dpkg-reconfigure grub-efi-amd64-signed)
	else
		# Need to install GRUB for EFI
		# (see https://bugs.debian.org/904062)
		(set -x; chroot /target apt-get --error-on=any update)
		(set -x; DEBIAN_PRIORITY=low chroot /target apt-get --autoremove install grub-efi-amd64)

		# We should purge grub-pc, but this bug blocks the way:
		# https://bugs.launchpad.net/bugs/1879466
	fi

	# Needed when installing via grub-efi-amd64-signed
	#
	if [ ! -s /target/boot/grub/grub.cfg ]
	then
		(set -x; chroot /target update-grub)
	fi

	# Sanity check
	#
	x=$(find /target/boot/efi -type f)
	if ! echo "$x" | grep -q grubx64 || \
	   ! echo "$x" | grep -q shimx64 || \
	   [ ! -s /target/boot/grub/x86_64-efi/core.efi ]
	then
		# Ugh, need to revisit grub-efi-amd64(-signed).postinst
		#
		echo 'error: EFI boot files were not installed'
		cleanup
		exit 1
	fi
fi # have efi_partition

if [ ! -s /target/boot/grub/grub.cfg ]
then
	echo 'error: grub.cfg was not regenerated'
	cleanup
	exit 1
fi

fi ####

####
## Disk label syntax
##

# The full /dev/disk/by-label/* path is valid and usable in these files,
# but the LABEL= syntax is more concise
#
for x in /target/boot/grub/menu.lst /target/etc/fstab
do
	test -f $x || continue

	perl -pi -e 's,/dev/disk/by-label/([-\w]+),LABEL=$1,' $x
done

####
## Server configuration
##

if [ "$role" = server ]
then
	for x in NetworkManager display-manager
	do
		if [ -e /target/etc/systemd/system/$x.service ]
		then
			echo "Disabling $x ..."
			chroot /target systemctl disable $x
			chroot /target systemctl mask    $x
		fi
	done

	x=/target/etc/grub.d/50_fallback
	echo "Creating $x ..."
	cat >$x <<END
#!/bin/sh
# $(basename $x)
#
# Fallback menu entry, for when the default one fails to boot
# (and only when the boot attempt was non-interactive)
#
# This is helpful when using grub-reboot(8) on a headless system.
#

echo 'set fallback=0'
END
	chmod 755 $x
fi

####
## xrdp
##

# Applicable:
#   etc/xrdp/cert.pem
#   etc/xrdp/key.pem

x=etc/xrdp/key.pem
if [ -x /target/etc/xrdp/make-cert.sh -a ! -f /config/$x ]
then
	echo 'Generating new xrdp key and (self-signed) certificate ...'
	rm -f /target/etc/xrdp/*.pem
	env FQDN="$fqdn" chroot /target \
		sh -c 'cd /etc/xrdp && ./make-cert.sh'
fi

####
## AFS
##

# Applicable:
#   etc/openafs/cacheinfo
#   etc/openafs/server/KeyFile (for AFS file server)

# Set the AFS cache size (see cacheinfo(5) for details)
#
x=etc/openafs/cacheinfo
if [ -f /target/$x -a ! -f /config/$x -a -n "$afscache_partition" ]
then
	part=$(basename $afscache_partition_real)

	# Size in 1024-byte blocks
	#
	raw_size=$(awk "/ $part\$/ { print \$3 }" /proc/partitions)

	adj_size=$(perl -e "print int($raw_size * 0.9)")

	echo "Updating /target/$x (cache size = $adj_size kB) ..."

	echo "/afs:/var/cache/openafs:$adj_size" >/target/$x

	# This will copy the new cache size value into the debconf database
	#
	chroot /target dpkg-reconfigure -f noninteractive openafs-client
fi

####
## Debconf essentials
##

# Save a copy of the current console keymap, since reconfiguring
# console-setup may cause the keymap to change
#
x=/tmp/saved-keymap.txt
rm -f $x
case "$(tty)" in
	/dev/tty[0-9]) dumpkeys >$x ;;
esac

ARCH=$(chroot /target dpkg --print-architecture)

cat <<EOF

You may wish to reconfigure certain important packages, including debconf,
keyboard-configuration, and console-setup (if installed).
EOF

if affirmative 'Reconfigure packages?'
then
	# Note: keyboard-configuration needs to go before console-setup,
	# because the latter resets the keymap to the configured setting

	reconf_packages=

	for package in \
		debconf \
		keyboard-configuration \
		console-setup \
		dma \
		locales \
		nullmailer \
		ssmtp \
		tzdata
	do
		if [ -f "/target/var/lib/dpkg/info/$package.list" -o \
		     -f "/target/var/lib/dpkg/info/$package:$ARCH.list" ]
		then
			reconf_packages+=" $package"
		fi
	done

	chroot /target dpkg-reconfigure $reconf_packages || :

	x=/tmp/saved-keymap.txt
	test ! -f $x || loadkeys -s $x || :
fi

# Some packages need reconfiguration unconditionally:
#
# iproute2, iputils-*, mtr*, wireshark-common: run setcap on executable to
#   enable functionality for non-root users (optional in some cases)
# libgstreamer1.0-0: run setcap on gst-ptp-helper (?)
#
# Note: On Debian, libgstreamer1.0-0 needs $ARCH disambiguation when both
# the amd64 + i386 packages are installed: https://bugs.debian.org/1040471

reconf_packages=

for package in \
	iproute2 \
	iputils-ping \
	iputils-tracepath \
	libgstreamer1.0-0:$ARCH \
	mtr \
	mtr-tiny \
	wireshark-common
do
	if [ -f "/target/var/lib/dpkg/info/$package.list" -o \
	     -f "/target/var/lib/dpkg/info/$package:$ARCH.list" ]
	then
		reconf_packages+=" $package"
	fi
done

chroot /target dpkg-reconfigure $reconf_packages || :

####
## SSH config
##

# Applicable:
#   etc/ssh/ssh_host_*_key
#   etc/ssh/ssh_host_*_key.pub
#   etc/ssh/sshd_config.d/*

x=etc/ssh/sshd_config.d/local.conf
if [ -d $(dirname /target/$x) -a ! -f /config/$x ]
then
	user_pw=yes
	root_pw=yes
	affirmative 'Allow SSH password authentication for users?' || user_pw=no
	affirmative 'Allow SSH password authentication for root?'  || root_pw=no

	([ $user_pw = yes ] || echo 'PasswordAuthentication no'
	 [ $root_pw = no  ] || echo 'PermitRootLogin yes'
	) >/target/$x

	test -s /target/$x || rm -f /target/$x
fi

####
## Root password
##

if [ -x /target/usr/bin/passwd ] && \
   ( (grep -q '^root:\*:' /target/etc/shadow && echo 'Please enter a root password for this system.') || \
     affirmative 'Would you like to set a new root password?' )
then
	printf '  Wait...\r'

	# Loop in case the password is entered incorrectly
	#
	while :
	do
		chroot /target passwd && break
		echo
	done
fi

echo

####
## Primary user setup
##

if [ -n "$primary_user" ]
then
	echo "Setting \"$primary_user\" as primary user:"

	# Add user to the adm group, so s/he can read system logs, and other
	# groups that enable typical console-oriented activity
	#
	x=/target/etc/group
	echo "Updating $x ..."
	perl -pi \
		-e "s/^((adm|audio|cdrom|dialout|floppy|kvm|plugdev|scanner|vboxusers|video):.*)\$/\$1,$primary_user/;" \
		-e 's/:,/:/;' \
		$x

	x=etc/ssh/sshd_config.d/local.conf
	if [ -d $(dirname /target/$x) -a ! -f /config/$x ]
	then
		echo "Updating /target/$x ..."
		echo "AllowUsers root $primary_user" >>/target/$x
	fi

	x=etc/aliases
	if  [ -f /target/$x -a ! -f /config/$x ] && ! grep -q '^root:' /target/$x
	then
		echo "Updating /target/$x ..."
		echo "root: $primary_user" >>/target/$x
	fi

	echo
fi

####
## Reset files
##

echo 'Resetting log files ...'

for f in \
	auth.log \
	btmp \
	daemon.log \
	debug \
	dmesg \
	faillog \
	fontconfig.log \
	fsck/checkfs \
	fsck/checkroot \
	kern.log \
	lastlog \
	mail.err \
	mail.log \
	messages \
	news/news.crit \
	news/news.err \
	news/news.notice \
	syslog \
	udev \
	ufw.log \
	user.log \
	wtmp \
	xrdp-sesman.log \
	xrdp.log
do
	x=/target/var/log/$f

	test -s $x || continue

	:    >$x
	rm -f $x.*
done

rm -f /target/var/log/sysstat/sa*

####
## Image-specific setup
##

x="$imagedir_verified/postinst"
if [ -x "$x" ]
then
	echo 'Running image-specific post-installation script ...'
	(cd /target && exec "$x")
	echo 'Done.'
fi

####
## Machine-specific setup
##

x=/config/postinst
if [ -x "$x" ]
then
	echo 'Running machine-specific post-installation script ...'
	(cd /target && exec "$x")
	echo 'Done.'
fi

####
## Ensure that we have a fresh initrd (so that e.g. mdadm.conf is in there)
##

if ! find /target/boot -type f -name 'initrd.img-*' -mmin -10 | grep -q .
then
	(set -x; chroot /target update-initramfs -k all -u)
fi

####
## Leave an updated APT package index
##

(set -x; chroot /target apt-get update)

####
## Wrap up
##

x=/target/etc/resolv.conf
if grep -q sysinst-install-image-temp $x
then
	echo "De-configuring $x ..."
	echo '# UNCONFIGURED' >$x
fi

# Version stamp
#
x=/target/etc/sysinst_version
echo "Generating $x ..."
cat >$x <<EOF
Image: $imagename
Date-Installed: $(chroot /target date --rfc-email)

$imagedir:
$(ls -l "$imagedir" | grep -v '^total')
EOF

cleanup

if is_zfs "$rootfs_partition" && ! zfs_export "$rootfs_partition"
then
	echo 'warning: remember to export the ZFS pool before booting into the new system!'
fi

echo
echo 'Installation complete.'

# EOF
