#!/bin/sh
# sysbootstrap
#
# Script to create a new Debian or Ubuntu base system in a chroot
#
# Procedure:
#
#   1. Boot up Debian/Ubuntu live CD environment
#
#   2. Open a terminal, "sudo bash" to get a root shell
#
#   3. "apt-get update", "apt-get install debootstrap"
#
#   4. You might need to "apt-get install debian-archive-keyring"
#
#   5. Copy in this script, and run it
#

if [ $# -ne 4 -a -z "$SYSBOOTSTRAP_INSIDE" ]
then
	cat <<EOF
usage: $0 DISTRO RELEASE ARCH ROOT-DIR

    DISTRO: "debian" or "ubuntu"
   RELEASE: "bullseye", "focal", etc.
      ARCH: "i386" or "amd64"
  ROOT-DIR: where the new system will be created
EOF
	exit 1
fi

DISTRO="$1"
RELEASE="$2"
ARCH="$3"
ROOT="$4"

# Note: A missing --variant argument creates a base system, which is what
# we want. The "minbase" variant leaves out many necessary packages.
#
VARIANT=

#COMPONENTS=main,non-free
#COMPONENTS=main,restricted,universe,multiverse

case "$DISTRO" in
	debian) DEB_MIRROR="http://cdn.debian.net/debian" ;;
	ubuntu) DEB_MIRROR="http://archive.ubuntu.com/ubuntu" ;;
esac

case "$DISTRO,$ARCH" in
	debian,i386)  KERNEL=linux-image-686-pae ;;
	debian,amd64) KERNEL=linux-image-amd64 ;;
	ubuntu,*)     KERNEL=linux-generic ;;
esac

export LC_ALL=C

################################################################

if [ -z "$SYSBOOTSTRAP_INSIDE" ]
then
	case "$DISTRO" in
		debian | ubuntu) ;;
		*) echo "$0: invalid distro \"$DISTRO\""; exit 1 ;;
	esac

	if [ ! -d /usr/share/debootstrap/scripts ]
	then
		echo "$0: error: debootstrap is not installed"
		exit 1
	fi

	case "$ARCH" in
		i386 | amd64) ;;
		*) echo "$0: invalid arch \"$ARCH\""; exit 1 ;;
	esac

	if [ -d $ROOT ]
	then
		echo "$0: error: $ROOT: directory already exists"
		exit 1
	fi

	if [ 0 -ne "$(id -u)" ]
	then
		echo "$0: error: this script must run as root"
		echo "(even fakeroot(1) isn't enough)"
		exit 1
	fi

	if [ ! -x "$0" ]
	then
		echo "$0: error: script file (\$0) must be accessible and executable"
		exit 1
	fi

	if ! (mount | grep -q '^udev on /dev type devtmpfs ' && \
	      mount | grep -q '^devpts on /dev/pts type devpts ') && \
	   [ -d /etc/apt -a ! -f /.dockerenv ]
	then
		echo "$0: error: /dev filesystem setup has changed"
		exit 1
	fi
fi

set -e

case "$SYSBOOTSTRAP_INSIDE" in
	debian)
	(set -ex
		apt-get -y install $KERNEL
		apt-get -y install grub-pc
		DEBIAN_FRONTEND=noninteractive apt-get -y install console-setup locales
		apt-get -y --no-install-recommends install ssh
		apt-get clean
	)
	exit
	;;

	ubuntu)
	(set -ex
		apt-get -y install $KERNEL libx11-data-
		apt-get -y --no-install-recommends install ssh
		apt-get clean
	)
	exit
	;;
esac

(set -ex; debootstrap \
	--force-check-gpg \
	--arch=$ARCH \
	${VARIANT:+--variant=$VARIANT} \
	$RELEASE \
	$ROOT \
	$DEB_MIRROR
)

echo

# When bootstrapping an Ubuntu system, resolv.conf will typically be a
# symlink to a systemd stub under /run/. This causes DNS resolution to
# break if we set up a new, empty /run/, so replace it with a proper file
# in that case.
#
x=$ROOT/etc/resolv.conf
if [ -L $x ] && readlink $x | grep -q '^\.\./run/'
then
	echo "Replacing $x with regular file ..."
	rm -f $x
	grep '^[a-z]' /etc/resolv.conf >$x
	echo
fi

# See comment in active-chroot script
#
if [ -f /.dockerenv ]
then
	dev_mount='--bind --make-private /dev'
else
	dev_mount='-t devtmpfs udev'
fi

(set -ex
	mount $dev_mount	$ROOT/dev
	#ount -t devpts devpts	$ROOT/dev/pts
	mount --bind --make-private /dev/pts	$ROOT/dev/pts

	mount -t proc proc $ROOT/proc
	mount -t tmpfs -o size=8M,mode=755 tmpfs $ROOT/run
	mount -t sysfs sysfs $ROOT/sys
)

echo

cp $0 $ROOT/tmp/inside.sh

SYSBOOTSTRAP_INSIDE=$DISTRO KERNEL=$KERNEL chroot $ROOT /tmp/inside.sh

rm -f $ROOT/tmp/inside.sh

echo

(set -ex
	umount $ROOT/sys
	umount $ROOT/run
	umount $ROOT/proc
	umount $ROOT/dev/pts
	umount $ROOT/dev
)

echo

################################

x=$ROOT/etc/apt/sources.list
echo "Replacing $x with generic version ..."

case $DISTRO in
	debian)
	cat >$x <<EOF
deb     http://cdn.debian.net/debian $RELEASE main contrib non-free
deb-src http://cdn.debian.net/debian $RELEASE main contrib non-free

deb     http://security.debian.org/debian-security $RELEASE-security main contrib non-free
deb-src http://security.debian.org/debian-security $RELEASE-security main contrib non-free

# $RELEASE-updates, to get updates before a point release is made;
# see https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_updates_and_backports
deb     http://cdn.debian.net/debian $RELEASE-updates main contrib non-free
deb-src http://cdn.debian.net/debian $RELEASE-updates main contrib non-free

# $RELEASE-backports, for unsupported updates to the stable release;
# see https://backports.debian.org/
#deb     http://cdn.debian.net/debian $RELEASE-backports main contrib non-free
#deb-src http://cdn.debian.net/debian $RELEASE-backports main contrib non-free

# For information about how to configure apt package sources,
# see the sources.list(5) manual.
EOF
	perl -pi -e '/ (unstable|sid)-(security|updates) / and s/^/#/' $x
	;;

	ubuntu)
	cat >$x <<EOF
## See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
## newer versions of the distribution.
deb     http://archive.ubuntu.com/ubuntu $RELEASE main restricted
deb-src http://archive.ubuntu.com/ubuntu $RELEASE main restricted

## Major bug fix updates produced after the final release of the
## distribution.
deb     http://archive.ubuntu.com/ubuntu $RELEASE-updates main restricted
deb-src http://archive.ubuntu.com/ubuntu $RELEASE-updates main restricted

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
## team. Also, please note that software in universe WILL NOT receive any
## review or updates from the Ubuntu security team.
deb     http://archive.ubuntu.com/ubuntu $RELEASE universe
deb-src http://archive.ubuntu.com/ubuntu $RELEASE universe
deb     http://archive.ubuntu.com/ubuntu $RELEASE-updates universe
deb-src http://archive.ubuntu.com/ubuntu $RELEASE-updates universe

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
## team, and may not be under a free licence. Please satisfy yourself as to
## your rights to use the software. Also, please note that software in
## multiverse WILL NOT receive any review or updates from the Ubuntu
## security team.
deb     http://archive.ubuntu.com/ubuntu $RELEASE multiverse
deb-src http://archive.ubuntu.com/ubuntu $RELEASE multiverse
deb     http://archive.ubuntu.com/ubuntu $RELEASE-updates multiverse
deb-src http://archive.ubuntu.com/ubuntu $RELEASE-updates multiverse

## N.B. software from this repository may not have been tested as
## extensively as that contained in the main release, although it includes
## newer versions of some applications which may provide useful features.
## Also, please note that software in backports WILL NOT receive any review
## or updates from the Ubuntu security team.
# deb     http://archive.ubuntu.com/ubuntu $RELEASE-backports main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu $RELEASE-backports main restricted universe multiverse

deb     http://security.ubuntu.com/ubuntu $RELEASE-security main restricted
deb-src http://security.ubuntu.com/ubuntu $RELEASE-security main restricted
deb     http://security.ubuntu.com/ubuntu $RELEASE-security universe
deb-src http://security.ubuntu.com/ubuntu $RELEASE-security universe
deb     http://security.ubuntu.com/ubuntu $RELEASE-security multiverse
deb-src http://security.ubuntu.com/ubuntu $RELEASE-security multiverse

## For information about how to configure apt package sources,
## see the sources.list(5) manual.
EOF
	;;
esac

x=$ROOT/var/lib/apt/lists
echo "Removing stale APT lists from $x/ ..."

rm -f $x/*_dists_*

# Official servers could be slow/inaccessible/offline
#chroot $ROOT sh -x -c 'apt-get update'

echo

if [ -d $ROOT/etc/network/interfaces.d ]
then
	# Note: /etc/network/interfaces should have only a "source"
	# directive that reads in the files under interfaces.d/
	#
	if egrep -v '^(#|source) ' $ROOT/etc/network/interfaces
	then
		echo
		echo "$0: error: review $ROOT/etc/network/interfaces file"
		exit 1
	fi

	x=$ROOT/etc/network/interfaces.d/generic
	echo "Creating $x ..."

	cat >$x <<EOF
# Generated by sysbootstrap

# Generic config: DHCP on the first Ethernet interface
auto /en*/1=main /eth*/1=main
iface main inet dhcp
EOF
fi

if [ -d $ROOT/etc/netplan ]
then
	x=$ROOT/etc/netplan/01-generic.yaml
	echo "Creating generic $x ..."

	cat >$x <<EOF
# Generated by sysbootstrap

# Generic config: DHCP on any Ethernet interface
network:
  version: 2
  ethernets:
    id0:
      match:
        name: en*
      dhcp4: true
EOF
fi

if [ ! -f $ROOT/etc/default/grub ]
then
	echo
	echo 'WARNING: GRUB bootloader is not present in the new system!'
fi

if [ ! -f $ROOT/etc/ssh/sshd_config ]
then
	echo
	echo 'WARNING: sshd is not present in the new system'
fi

echo
echo '**** System bootstrap complete ****'

# EOF
