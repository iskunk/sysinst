# customize-parts/ssh.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site ID
#

# Disable some older/insecure SSH ciphers/algorithms, per the evaluation
# at https://stribika.github.io/2015/01/04/secure-secure-shell.html
#
# (Also: https://github.com/stribika/sshlabs/blob/master/main/algorithms.py)

kexalgs_option='KexAlgorithms -diffie-hellman-*-sha1,*nistp*'

ciphers_option='Ciphers -arcfour*,*-cbc,*-cbc@*'

macs_option='MACs -hmac-md5*,hmac-sha1*,umac-64*'

hkalgs_option='HostKeyAlgorithms -ssh-dss*,*nistp*'

for kex in $(ssh -Q kex)
do
	case "$kex" in
		# BAD
		diffie-hellman-group1-sha1) ;; # 1024 bits is not enough
		diffie-hellman-*-sha1) ;; # SHA1 is broken
		ecdh-sha2-nistp*) ;; # NIST curves suck

		# GOOD
		curve25519-sha256) ;;
		curve25519-sha256@libssh.org) ;; # older variant
		diffie-hellman-group-exchange-sha256) ;;
		diffie-hellman-group14-sha256) ;;
		diffie-hellman-group1[68]-sha512) ;;
		sntrup4591761x25519-sha512@tinyssh.org) ;;
		sntrup761x25519-sha512@openssh.com) ;;

		*) die "new in KexAlgorithms: $kex" ;;
	esac
done

for cipher in $(ssh -Q cipher)
do
	case "$cipher" in
		# BAD
		3des-cbc) ;; # DES is broken
		aes???-cbc) ;; # CBC mode not good
		arcfour*) ;; # RC4 is broken
		blowfish-cbc) ;; # 64-bit block size
		cast128-cbc) ;; # 64-bit block size
		rijndael-cbc@lysator.liu.se) ;; # CBC mode not good

		# GOOD
		aes???-ctr) ;;
		aes???-gcm@openssh.com) ;;
		chacha20-poly1305@openssh.com) ;;

		*) die "new in Ciphers: $cipher" ;;
	esac
done

for mac in $(ssh -Q mac)
do
	case "$mac" in
		# BAD
		hmac-md5*) ;; # MD5 is (very) broken
		hmac-sha1*) ;; # SHA1 is broken
		umac-64*) ;; # Need >= 128 bits

		# GOOD
		hmac-ripemd*) ;;
		hmac-sha2-*) ;;
		umac-128*) ;;

		*) die "new in MACs: $mac" ;;
	esac
done

for hkey in $(ssh -Q key)
do
	case "$hkey" in
		# BAD
		ecdsa-sha2-nistp*) ;; # NIST curves suck
		sk-ecdsa-sha2-nistp*) ;; # ditto
		ssh-dss*) ;; # 1024-bit keys only

		# GOOD
		sk-ssh-ed25519@openssh.com) ;;
		sk-ssh-ed25519-cert-v01@openssh.com) ;;
		ssh-ed25519) ;;
		ssh-ed25519-cert-v01@openssh.com) ;;
		ssh-rsa) ;;
		ssh-rsa-cert-v01@openssh.com) ;;

		*) die "new in HostKeyAlgorithms: $hkey" ;;
	esac
done

x=/etc/ssh/sshd_config
if grep '^#HostKey' $x | grep -Ev 'ssh_host_(rsa|ecdsa|ed25519)_key'
then
	die "$x has unrecognized commented-out HostKey directives"
fi

x=/etc/ssh/sshd_config.d/$SITE_ID.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key

#Banner /etc/issue.net

GatewayPorts clientspecified
StreamLocalBindUnlink yes

$kexalgs_option
$ciphers_option
$macs_option
EOF

	show_file $x
fi

x=/etc/ssh/ssh_config.d/$SITE_ID.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

HashKnownHosts no

$hkalgs_option
$kexalgs_option
$ciphers_option
$macs_option
EOF

	show_file $x
fi

x=/etc/ssh/moduli
if grep -q '^20120821044040 ' $x
then
	save_config_file $x
	section "Editing $x ..."

	# Comment out all lines with a "Size" value less than 2000
	#
	y=/root/moduli.new
	awk '{ c=""; if ($5 < 2000) c="#:"; print c $0 }' $x >$y
	cat $y >$x
	rm -f $y

	# The diff is huge and not all that interesting
	#
	diff_config_file >/dev/null
fi

# Test the new SSH configuration
#
if ssh -p 12345 localhost 2>&1 | fgrep -q ' Bad SSH'
then
	die 'SSH configuration is not valid'
fi

# Workaround for https://bugs.debian.org/993459
#
x=/etc/pam.d/sshd
if grep -q 'pam_mail.* noenv' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e '/^session/ and /pam_mail\.so/ and s/\b(noenv)\b/#$1/' $x

	diff_config_file
fi

# end customize-parts/ssh.sh
