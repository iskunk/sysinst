# customize-parts/nodejs.sh

if [ "$distro/$(lsb_release -cs)" = ubuntu/jammy ] && \
   ! package_installed npm
then
	# The Node.js version in Ubuntu 22.04/jammy is out of date
	# (version 12.x). Do some temporary APT hackery to install
	# the one from 22.10/kinetic (version 18.x) instead.

	section 'Installing npm from Ubuntu kinetic release ...'

	x=/tmp/apt-sources.list

	# Note that kinetic is no longer available on the normal mirrors;
	# we have to reach into the old-releases server

	perl -p \
		-e 'if (/^deb/) {' \
		-e '  s/jammy/kinetic/g;' \
		-e '  s!(/ubuntu)/? !$1-old !;' \
		-e '  s!(/ubuntu)-security/? !$1-old !;' \
		-e '}' \
		/etc/apt/sources.list >$x

	apt_get_alt="apt-get -o Dir::Etc::SourceList=$x $batch"

	run_command $apt_get_alt --error-on=any update

	run_command $apt_get_alt --no-install-recommends install npm

	section 'Restoring original package index ...'

	run_command apt-get --error-on=any update
else
	# No hackery needed
	#
	install_packages --no-install-recommends npm
fi

# end customize-parts/nodejs.sh
