# customize-parts/locale.sh

x=/etc/locale.gen
if ! grep -v '^#' $x | grep -q .
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi -e '/ (C|en_US)\.UTF-8/ and s/^# *//' $x

	diff_config_file

	y=/etc/default/locale
	save_config_file $y
	section "Editing $y ..."

	run_command update-locale LANG=C.UTF-8

	diff_config_file

	run_command dpkg-reconfigure -f noninteractive locales

	# This is known to fail in Debian bullseye
	#
	debconf-show locales \
	| grep -q 'default_environment_locale: C\.UTF-8' \
	|| warning 'locales/default_environment_locale debconf selection was not set from file'
fi

# EOF
