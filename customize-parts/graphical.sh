# customize-parts/graphical.sh

install_packages numlockx

x=/etc/default/numlockx
if [ -f $x ] && ! grep -q '^NUMLOCK=off' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's/^(NUMLOCK)=.+$/$1=off/' $x

	diff_config_file
fi

# Use a nice Debian theme for Plymouth, if applicable
#
x=/etc/plymouth/plymouthd.conf
if [ -f $x -a -d /usr/share/plymouth/themes/homeworld ] && \
   ! grep -q homeworld $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi \
		-e 's/^#(\[Daemon\]|Theme=)/$1/;' \
		-e 's/^(Theme)=.+$/$1=homeworld/' \
		$x

	diff_config_file
fi

# Emacs! (graphical)
#
package_installed emacs-common || die 'Emacs is not installed'
install_packages  emacs-gtk
#
# Installing emacs-gtk should have gotten rid of the non-X version.
# (Note that the X version can also run in text mode, thus making the
# non-X version redundant.)
#
! package_installed emacs-nox || die 'non-X version of Emacs is still installed'

# TeX/LaTeX
# (minimal installation, just enough to build TGLib docs)
#
install_packages --no-install-recommends <<EOF
	lmodern
	texlive-font-utils
	texlive-fonts-recommended
	texlive-latex-extra
	texlive-plain-generic	# needed to build Texinfo docs
EOF

# https://bugs.launchpad.net/bugs/1296466
#
if ! tl-paper list | head -n 1 | grep -q letter
then
	run_command tl-paper set all letter
fi

# The Microsoft Core Truetype fonts, always appreciated
#
( ####
	# Workaround for https://bugs.debian.org/1033283
	# (the Debian ttf-mscorefonts-installer package refuses to install
	# with a non-HTTP(S) proxy in $http_proxy, and otherwise records
	# the proxy setting in debconf)
	#
	if [ $distro = debian -a -n "$http_proxy" ] && \
	   ! package_installed ttf-mscorefonts-installer
	then
		section 'Working around ttf-mscorefonts-installer bug ...'
		run_command apt-get $batch --download-only install ttf-mscorefonts-installer
		unset http_proxy
	fi

	install_packages ttf-mscorefonts-installer
) || exit ####

if package_installed build-essential
then
	# Graphical development tools
	#
	install_packages <<EOF
		ddd		# fancy GDB frontend
		git-gui
EOF

	# X development libraries
	# (xutils-dev is needed to get lndir(1))
	#
	install_packages xorg-dev xutils-dev

	# GTK+ and Qt4 development
	#
	#:install_packages libgtk2.0-dev libqt4-dev qt4-dev-tools

	# OpenGL development
	#
	install_packages <<EOF
		libgl-dev
		libglu1-mesa-dev
		libglvnd-dev

		freeglut3-dev
		libglew-dev	# needed to build mesa-demos
EOF

	# Documentation for GTK+ and GLib
	# (Note: Qt docs should have been pulled in by libqt4-dev)
	#
	#:install_packages libglib2.0-doc libgtk2.0-doc

	# So that we can do .NET stuff (barf) on Linux
	#
	#:install_packages mono-devel
fi

# Have at least one decent non-mainstream Web browser
#
install_packages \
	epiphany-browser \
	NO:pipewire- \
	NO:xdg-desktop-portal-

# Random useful/fun stuff (graphical)
#
install_packages <<EOF
	dirdiff
	freerdp2-x11	# xfreerdp client
	geeqie		# good image viewer
	gifsicle	# nice tool for optimizing GIF files
	gnuplot
	gnuplot-doc
	graphviz	# generates pretty-looking graph diagrams
	gsfonts-x11
	gtkterm		# GUI serial terminal
	gv		# Postscript viewer
	gxmessage	# successor to xmessage(1)
	imagemagick
	keepassxc	# good password-storage utility
	meld		# great tool for merging source-code changes
	mesa-utils	# glxgears, glxinfo et al.
	nedit		# excellent text editor
	openbox NO:obsession-
			# for minimal X sessions (avoid "obsession"
			# due to https://bugs.debian.org/959922)
	openexr		# because HDR images are _cool_
	librsvg2-bin
	poppler-utils	# PDF file utilities (note: X11 dep on Debian)
	psutils		# not graphical, but Recommends: ghostscript
	remmina		# nice frontend to RDP/VNC, doesn't grab WM keystrokes
	sng		# not graphical, but it depends on x11-common
	tigervnc-standalone-server
	tigervnc-viewer
	tkcvs		# for tkdiff(1)
	unifont
	wireshark
	x11vnc		# allows VNC access to an existing X session
	xclip
	$(if_available xdiskusage || echo xdu) # [Debian testing]
			# no-frills graphical version of du(1)
	xdotool		# good for automatic GUI testing/frobbing
	$(if_available xpdf)
	xserver-xephyr

	NO:modemmanager-	# absolutely not

	# Install a frontend for DConf if the service is present
	#
	$(package_installed dconf-service && echo dconf-cli)

	# Don't bump out lpr if it's already installed
	#
	$(package_installed lpr && echo cups-bsd-)
EOF

# end customize-parts/graphical.sh
