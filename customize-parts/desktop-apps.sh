# customize-parts/desktop-apps.sh

# GIMP, of course, now that xubuntu-desktop doesn't include it
#
install_packages gimp $(if_available gimp-help-en)

# Inkscape: the GIMP of vector images
#
install_packages inkscape

# Pidgin works dandily with Lync
# (but Outlook Web Access is a lot easier)
#
#:install_packages pidgin pidgin-sipe

#### VirtualBox

# Note: Expect trouble when running this on a system with Secure Boot,
# due to the kernel modules compiled at install time with DKMS

if package_available virtualbox && package_installed build-essential
then
	install_packages <<-EOF
		virtualbox
		virtualbox-guest-x11
		virtualbox-guest-additions-iso
	EOF

	x=/etc/default/virtualbox
	test -f $x
	if ! grep -q '^SHUTDOWN_USERS="all"' $x
	then
		section "Editing $x ..."
		save_config_file $x

		perl -pi -e 's/^(SHUTDOWN_USERS)=.*$/$1="all"/' $x
		#perl -pi -e 's/^(SHUTDOWN)=.*$/$1=savestate/' $x

		diff_config_file
	fi

	x=/etc/profile.d/vbox-user-home.sh
	if [ -f /usr/local$x -a ! -L $x ]
	then
		section "Symlinking $x ..."

		rm -f $x
		run_command ln -sv /usr/local$x $x
	fi

	x=/etc/X11/Xsession.d/98vboxadd-xclient
	if [ -f $x ] && egrep -q '^[^#]+ --vmsvga' $x
	then
		section "Editing $x ..."
		save_config_file $x

		# https://bugs.launchpad.net/bugs/1973291
		# https://bugs.launchpad.net/bugs/1973302
		#
		perl -pi -e '/VBoxClient --(draganddrop|vmsvga)/ && !/^\s*#/ and s/^(\s*)/$1#:/' $x

		diff_config_file
	fi
fi

# Media players
#
install_packages --no-install-recommends mpv vlc

# LibreOffice
#
install_packages <<EOF
	libreoffice-calc	# spreadsheet
	#libreoffice-gtk3	# GTK+ user interface
	libreoffice-impress	# presentation
	libreoffice-writer	# word processor
	libreoffice-help-en-us	# documentation

	NO:snapd-		# ugh
	$(if_not_installed build-essential-)
EOF

# Some additional non-free goodies
#
if [ $distro = ubuntu ]
then
	install_packages <<EOF
		ubuntu-restricted-extras
		chromium-codecs-ffmpeg-extra-	# snap transition package
EOF
fi

# end customize-parts/desktop-apps.sh
