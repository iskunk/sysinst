# customize-parts/unattended-upgrades.sh
#
# Arguments:
#	SITE_ID = site identifier (in lowercase)
#	SITE_NAME = site name
#

package_installed unattended-upgrades \
|| die 'unattended-upgrades is not installed'

# Less hassle to place site-specific directives in a separate file,
# especially if a config-file update comes along
#
x=/etc/apt/apt.conf.d/51unattended-upgrades-$SITE_ID
if [ ! -f $x ]
then
	section "Creating $x ..."

	# The boilerplate text is from Debian
	#
	cat >$x <<EOF
/* $x */

// $SITE_NAME config

// Unattended-Upgrade::Origins-Pattern controls which packages are
// upgraded.
//
// Lines below have the format format is "keyword=value,...".  A
// package will be upgraded only if the values in its metadata match
// all the supplied keywords in a line.  (In other words, omitted
// keywords are wild cards.) The keywords originate from the Release
// file, but several aliases are accepted.  The accepted keywords are:
//   a,archive,suite (eg, "stable")
//   c,component     (eg, "main", "contrib", "non-free")
//   l,label         (eg, "Debian", "Debian-Security")
//   o,origin        (eg, "Debian", "Unofficial Multimedia Packages")
//   n,codename      (eg, "jessie", "jessie-updates")
//     site          (eg, "http.debian.net")
// The available values on the system are printed by the command
// "apt-cache policy", and can be debugged by running
// "unattended-upgrades -d" and looking at the log file.
//
// Within lines unattended-upgrades allows 2 macros whose values are
// derived from /etc/debian_version:
//   \${distro_id}            Installed origin.
//   \${distro_codename}      Installed codename (eg, "buster")
Unattended-Upgrade::Origins-Pattern {
	"origin=Debian,codename=\${distro_codename},label=Debian";
	"origin=Debian,codename=\${distro_codename},label=Debian-Security";
	"origin=Debian,codename=\${distro_codename}-debug";
	"origin=Debian,codename=\${distro_codename}-security-debug";
	"origin=Debian,codename=\${distro_codename}-updates";

	"origin=Ubuntu,suite=\${distro_codename}";
	"origin=Ubuntu,suite=\${distro_codename}-security";
	"origin=Ubuntu,suite=\${distro_codename}-updates";
};

// Splitting the upgrade into small chunks adds a lot of overhead
Unattended-Upgrade::MinimalSteps "false";

// Send email to this address for problems or packages upgrades
// If empty or unset then no email is sent, make sure that you
// have a working mail setup on your system. A package that provides
// 'mailx' must be installed. E.g. "user@example.com"
Unattended-Upgrade::Mail "root";

/* end $x */
EOF

	show_file $x
fi

# end customize-parts/unattended-upgrades.sh
