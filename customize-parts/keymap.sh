# customize-parts/keymap.sh

x=/usr/local/bin/asdf
if [ ! -f $x ]
then
	section "Creating $x convenience script ..."

	cat >$x <<'EOF'
#!/bin/sh
# asdf: switch current keymap to Dvorak

if [ -n "$DISPLAY" ]
then
	setxkbmap us dvorak

	printf '\n  Keyboard map: DVORAK  \n\n' \
	| xmessage -file - -buttons "" -center -timeout 2
else
	loadkeys dvorak
fi

# end asdf
EOF

	run_command chmod 755 $x
	show_file $x
fi

x=/usr/local/bin/aoeu
if [ ! -f $x ]
then
	section "Creating $x convenience script ..."

	cat >$x <<'EOF'
#!/bin/sh
# aoeu: switch current keymap to QWERTY

if [ -n "$DISPLAY" ]
then
	setxkbmap us

	printf '\n  Keyboard map: QWERTY  \n\n' \
	| xmessage -file - -buttons "" -center -timeout 2
else
	loadkeys us
fi

# end aoeu
EOF

	run_command chmod 755 $x
	show_file $x
fi

# end customize-parts/keymap.sh
