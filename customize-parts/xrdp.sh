# customize-parts/xrdp.sh
#
# Arguments:
#	CERT_SUBJECT   = Subject name for server certificate / signing request
#			 (optional, can use "$hostname")
#	XRDP_LOGO_FILE = 240x120 BMP file to show on login screen
#

install_packages xrdp

x=/etc/xrdp/make-cert.sh
if [ -n "$CERT_SUBJECT" -a ! -f $x ]
then
	section "Creating $x ..."

	# Note: $FQDN is set in the install_image script
	#
	cat >$x <<EOF
#!/bin/sh
# Create private key and self-signed certificate

hostname=\$( (echo "\$FQDN" | grep .) || hostname --fqdn 2>/dev/null || hostname)

set -ex

test ! -f key.pem

openssl req -x509 -newkey rsa:2048 -nodes -days 3650 \\
	-subj "$CERT_SUBJECT" \\
	-keyout key.pem \\
	-out cert.pem

chown xrdp:root key.pem
EOF

	run_command chmod 755 $x
	show_file $x
fi

x=/etc/xrdp/make-csr.sh
if [ -n "$CERT_SUBJECT" -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
#!/bin/sh
# Create private key and certificate signing request

hostname=\$( (echo "\$FQDN" | grep .) || hostname --fqdn 2>/dev/null || hostname)

set -ex

test ! -f key.pem

openssl req -new -newkey rsa:2048 -nodes \\
	-subj "$CERT_SUBJECT" \\
	-keyout key.pem \\
	-out xrdp-cert.csr

: chown xrdp:root key.pem
EOF

	run_command chmod 755 $x
	show_file $x
fi

x=/etc/xrdp/cert.pem
y=/etc/xrdp/key.pem
if [ -L /etc/xrdp/cert.pem ]
then
	section 'Creating private key and self-signed certificate ...'

	run_command rm -f $x $y

	(cd /etc/xrdp && ./make-cert.sh)
fi

x=/etc/xrdp/xrdp.ini
if fgrep -q 'security_layer=negotiate' $x
then
	#if fgrep -q TLSv1.4 $x || fgrep -q TLSv2 $x
	#then
	#	die "$x has new TLS-related options; please review"
	#fi

	if [ -n "$XRDP_LOGO_FILE" ]
	then
		section 'Adding custom logo file for xrdp ...'
		run_command cp $XRDP_LOGO_FILE /etc/xrdp/
	fi

	section "Editing $x ..."
	save_config_file $x

	perl -pi \
		-e 's/^(security_layer)=.+$/$1=tls/;' \
		-e 's/^(blue)=.+$/$1=08246b/;' \
		-e 's/^#?(ls_top_window_bg_color)=.+$/$1=1a3b5a/;' \
		-e 's/^(LogLevel)=.+$/$1=INFO/;' \
		-e 's/^(SyslogLevel)=.+$/$1=INFO/;' \
		$x

	if [ -n "$XRDP_LOGO_FILE" ]
	then
		logo=$(basename $XRDP_LOGO_FILE)
		perl -pi -e 's,^(ls_logo_filename)=.*$,$1=/etc/xrdp/'"$logo"',;' $x
	fi

	# Disable all but the "Xorg" session type
	#
	perl -pi \
		-e '/^\[(Xvnc|vnc-any|neutrinordp-any)\]/ and $c = 1;' \
		-e '/^$/ and $c = 0;' \
		-e '$c and s/^/#:/;' \
		$x

	# Keep xrdp listening on localhost until we can get the
	# AppArmor hardening deployed. (Be careful with this edit,
	# as other sections use port=... for a different purpose)
	#
	perl -pi -e 's,^(port)=3389$,$1=tcp://.:3389,' $x

	diff_config_file
fi
#
# Double-check this
#
grep -q '^security_layer=tls$' $x \
|| die "$x: security_layer not set to \"tls\""

x=/etc/xrdp/sesman.ini
if ! fgrep -q 'DisconnectedTimeLimit=1000000' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi \
		-e 's/^(AllowRootLogin)=.+$/$1=false/;' \
		-e 's/^(AlwaysGroupCheck)=.+$/$1=true/;' \
		-e 's/^(DisconnectedTimeLimit)=.+$/$1=1000000/;' \
		-e 's/^(LogLevel)=.+$/$1=INFO/;' \
		-e 's/^(SyslogLevel)=.+$/$1=INFO/;' \
		-e 's/^(TerminalServerUsers)=.+$/$1=users/;' \
		$x

	# DESKTOP_SESSION is picked up by e.g.
	# /etc/X11/Xsession.d/60x11-common_xdg_path
	#
	# For a more full-featured remote desktop, use
	#
	#   DESKTOP_SESSION=xubuntu
	#   STARTUP=startxfce4
	#
	cat >>$x <<EOF
DESKTOP_SESSION=openbox
STARTUP=openbox-session
EOF

	diff_config_file
fi
#
# Double-check this
#
grep -Fqx 'AllowRootLogin=false' $x \
|| die "$x: root login still allowed"

for dir in \
	/etc/polkit-1/localauthority.conf.d \
	/etc/polkit-1/localauthority/50-local.d
do
	test ! -d $dir || break
done
x=$dir/colord-fix-auth.pkla
if [ -d $dir -a ! -f $x ]
then
	section "Creating $x ..."

	# Workaround cribbed from
	# https://github.com/TurboVNC/turbovnc/issues/47
	#
	cat >$x <<EOF
## $SITE_NAME config

# https://bugs.launchpad.net/bugs/1871593
# https://github.com/TurboVNC/turbovnc/issues/47#issuecomment-611853598
#
[Zap colord authentication dialogs for remote graphical users]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
ResultAny=no
EOF

	show_file $x
fi

# Fix for https://bugs.debian.org/1071556
#
x=/etc/xrdp/xrdp_keyboard.ini
if grep -q '^rdp_layout_us_dvorak=dvorak' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's/^(rdp_layout_us_dvorak)=dvorak/$1=us(dvorak)/' $x

	diff_config_file
fi

# end customize-parts/xrdp.sh
