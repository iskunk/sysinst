# customize-parts/cli.sh
#
# Arguments:
#	SITE_NAME = site name
#

# Emacs! (text-mode)
#
if ! package_installed emacs	# in case emacs-gtk is already installed
then
	# Avoid pulling in dbus, X11 (!), or mailutils on Debian
	#
	install_packages \
		emacs-nox \
		emacs \
		$(if_not_installed dbus-) \
		$(if_not_installed libx11-data-) \
		$(if_not_installed mailutils-)
fi

install_packages fortunes

# men-women: "Life's too short to dance with ugly women."
# zippy: (bizarre and not terribly funny non sequiturs)
#
for db in \
	men-women \
	zippy
do
	# The fortune(6) man page says a set of fortunes can be disabled
	# by deleting the .dat file
	#
	x=/usr/share/games/fortunes/$db.dat
	if [ -f $x ]
	then
		section "Disabling '$db' fortunes ..."
		disable_file $x
	fi
done

# Enable fscrypt [home] directory encryption
#
x=libpam-fscrypt
if package_available $x
then
	install_packages $x

	# Note: Don't run "fscrypt setup" here, as fscrypt complains if
	# the root directory is not a mountpoint (e.g. inside a chroot).
	# Also, fscrypt needs to calibrate passphrase hashing costs to
	# the specific system on which this image will be deployed.
fi

# Random useful/fun stuff (text-mode)
#
# Possible additions: debfoster deps-tools-cli im-switch mkisofs zh-autoconvert
#
install_packages <<EOF
	aha		# converts terminal output (ANSI) to HTML
	alpine
	apulse		# good for programs that need PulseAudio
	autossh
	bchunk		# for the occasional .bin/.cue CD image
	bdfresize
	beep
	bsdgames	# includes primes(1)
	bubblewrap	# sandboxed execution environment
	ccrypt		# file-encryption tool
	curl
	debootstrap
	dvd+rw-tools	# for burning (DL) DVDs
	dvdbackup
	enscript
	exfatprogs	# exFAT filesystem utilities
	fastjar
	fdupes
	fetchmail
	flac
	genius		# for quick (or advanced) math calculations
	$(if_available gnupg-curl)
			# needed for HKPS keyserver support
	gperf
	htop		# a cooler, ncurses-based take on top(1)
	id3v2
	indent
	iotop
	#iso-codes	# reference for ISO 639 codes et al.
			# (now pulled in by language-selector-common)
	isomd5sum
	isoquery
	isync		# actually "mbsync", for mirroring IMAP mailboxes
	jq		# good for JSON wrangling
	libjpeg-turbo-progs
	$(if_available linux-tools-generic)
			# includes USB/IP utilities (Ubuntu only)
	lm-sensors
	lynx
	lz4		# for handling .lz4 compressed files
	lzip
	mariadb-client	# use instead of MySQL
	midicsv		# convert MIDI files to text and back
	miscfiles	# includes a good-sized system wordlist
	modplug-tools	# better than MikMod
	moreutils	# includes useful things like errno(1)
	mp3check
	mpg321
	mpgtx		# good for slicing up MP3 files
	msitools	# for dealing with Microsoft .msi files
	mtools		# for occasional DOS floppy-disk access
	ncal		# provides cal(1)
	ncftp
	$(if_available ncurses-hexedit)	# [Debian testing]
	nmap
	opus-tools	# even better than Vorbis
	p7zip-full
	pcre2-utils
	#pcregrep	# don't install, uses PCRE1
	$(if_available pinfo)	# [Debian testing]
			# way better than info(1)
	pigz		# parallelized gzip
	pixz		# parallelized xz
	pngcrush
	pwgen		# quick-and-dirty password generation tool
	qpdf		# useful tool for manipulating PDF files
	reptyr		# great for taking over an inaccessible tty
	rpm		# since we have a lot of Red Hat / Fedora around
	scrypt		# good file-encryption tool
	sdparm
	seccomp		# for scmp_sys_resolver(1), a syscall lookup tool
	sharutils
	signify-openbsd	# simple signing/verification tool
	socat		# more advanced take on netcat(1) functionality
	sox		# mostly for play(1), which can handle odd formats
	sqlite3		# tool for fiddling with SQLite database files
	squashfs-tools	# for creating compressed filesystem images
	tcsh		# ever-present in MIT Athena
	texi2html
	texinfo		# for makeinfo(1), needed by GNU package builds
	tidy
	tofrodos
	transmission-cli	# complements the GUI BitTorrent client
	tree
	tshark		# a console version of Wireshark? yes please!
	units
	uniutils	# useful tools for examining Unicode files
	vim		# better than vim-tiny (this is for you, Div)
	vorbis-tools
	whois
	#wodim		# for burning CDs/DVDs (but not DL DVDs)
			# (superseded by xorriso)
	xorriso		# for burning CDs/DVDs
	xsltproc
	zh-autoconvert	# great for random Chinese text w/unknown encoding
	zip		# will pull in unzip as well

	NO:minissdpd-	# noisy network daemon

	# Don't install any X/graphical dependencies at this point
	#
	$(if_not_installed libx11-data-)
	$(if_not_installed x11-common-)
EOF

# Enable the Webster's English wordlist from the "miscfiles" package
#
if ! debconf-show dictionaries-common | fgrep -q 'default-wordlist: english'
then
	run_command select-default-wordlist --set-default='^english'
fi

# Set ALSA as default libao driver
#
x=/etc/libao.conf
require_file $x
if ! grep -q '^default_driver=alsa' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's/^(default_driver)=.+$/$1=alsa/' $x

	diff_config_file
fi

# Set the default Lynx home page
#
x=/etc/lynx/local.cfg
if [ -d /etc/lynx -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
# $x
# $SITE_NAME config

#STARTFILE:https://www.google.com/
STARTFILE:https://start.duckduckgo.com/lite/

# https://help.launchpad.net/API/EndUserHints
#
REFERER_WITH_QUERY:PARTIAL

# Don't use external handlers, as they pose a security risk
#
GLOBAL_MAILCAP:/dev/null

# end $x
EOF

	show_file $x
fi

# end customize-parts/cli.sh
