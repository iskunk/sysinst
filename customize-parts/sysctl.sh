# customize-parts/sysctl.sh
#
# Sysctl configuration

x=/etc/sysctl.d/60-$SITE_ID.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
# $x

# Save core files in \$XDG_RUNTIME_DIR
# (refer to core(5) for available substitutions)
#
kernel.core_pattern = /run/user/%u/core.%p

# end $x
EOF

	show_file $x
fi

# end customize-parts/sysctl.sh
