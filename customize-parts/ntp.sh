# customize-parts/ntp.sh
#
# Arguments:
#	NTP_SERVERS = space-separated list of NTP servers to use
#

if package_installed systemd-timesyncd
then
	# This makes things easy
	#
	run_command systemctl stop systemd-timesyncd
	run_command apt-get $batch --purge remove systemd-timesyncd

elif [ -x /lib/systemd/systemd-timesyncd ] && \
     systemctl --quiet is-enabled systemd-timesyncd
then
	section 'Disabling systemd-timesyncd in favor of ntpd ...'

	run_command systemctl stop systemd-timesyncd
	run_command systemctl mask systemd-timesyncd
fi

# Options: ntp, ntpsec, openntpd
#
# ntp is old, crufty, and quite likely insecure.
#
# ntpsec provides ntptrace(1), and its ntpd responds to same, but for some
# reason it will not synchronize a clock in a full install.
#
# openntpd is known to work. sntp is an alternative to ntptrace(1).
#
install_packages openntpd sntp

first_ntp_server=$(echo $NTP_SERVERS | awk '{print $1}')

x=$(which_file /etc/openntpd/ntpd.conf /etc/ntpsec/ntp.conf /etc/ntp.conf)
if [ -n "$first_ntp_server" ] && ! fgrep -q "$first_ntp_server" $x
then
	save_config_file $x
	section "Editing $x ..."

	# Don't use the NTP pool servers
	# (Note: OpenNTPD uses the keyword "servers" instead of "pool")
	#
	perl -pi -e 's/^((pool|servers)\s+[\w.]+\.pool\.ntp\.org)\b/#:$1/' $x

	server_lines=$(for s in $NTP_SERVERS; do printf 'server %s\\n' $s; done)

	if grep -q '^server ' $x
	then
		# Comment out the existing server, then add our own after it
		#
		perl -pi -e 'if (/^server /) { s/^/#:/; $_ .= "\n'"$server_lines"'" }' $x

	elif grep -q '^#server ' $x
	then
		# Add our servers after the commented-out example
		#
		perl -pi -e 'if (/^#server \S*example/) { $_ .= "\n'"$server_lines"'" }' $x

	elif grep -q '^# Specify one or more NTP servers' $x
	then
		# Add our servers after the comment
		#
		perl -pi -e 'if (/^# Specify .+ servers/) { $_ .= "\n'"$server_lines"'" }' $x
	else
		die "need new logic to edit $x"
	fi

	# Allow NTP queries
	#
	perl -pi -e '/^restrict\b/ && s/\s+noquery\b//' $x

	# "iburst" is recommended in the ntpd(8) man page
	#
	case "$x" in
		/etc/ntpsec/ntp.conf | /etc/ntp.conf)
		perl -pi -e '/^server\b/ and s/$/ iburst/' $x
		;;
	esac

	diff_config_file

	initd=$(which_file /etc/init.d/openntpd /etc/init.d/ntpsec /etc/init.d/ntp)
	run_command $initd stop
fi

x=/etc/default/openntpd
if [ -f $x ] && ! grep -q '^DAEMON_OPTS=.*-s' $x
then
	save_config_file $x
	section "Editing $x ..."

	# https://bugs.debian.org/697643
	#
	perl -pi -e 's/^(DAEMON_OPTS)="(.*)"$/$1="$2 -s"/' $x

	diff_config_file
fi

# Installing a proper NTP daemon should have bumped out systemd's cheap
# imitation (if it was ever present to begin with)
#
! package_installed systemd-timesyncd \
|| die 'systemd-timesyncd is still installed'

# end customize-parts/ntp.sh
