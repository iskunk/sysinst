# customize-parts/grub2.sh
#
# GRUB2 configuration

# Install partial support for (amd64) EFI-based systems
#
if [ $arch = amd64 ] && package_available grub-efi-amd64-signed
then
	# Note: Specifying "shim-signed" is redundant on Debian,
	# but not on Ubuntu (focal)
	#
	install_packages grub-efi-amd64-signed shim-signed
fi

install_packages console-data

for x in 41_keymap_default 45_auxboot
do
	[ ! -f /etc/grub.d/$x ] || continue
	run_command cp grub.d/$x /etc/grub.d/
done

x=/usr/local/sbin/aux-reboot
if [ ! -f $x ]
then
	section "Adding $x ..."
	run_command cp sysresccd/aux-reboot /usr/local/sbin/
fi

x=/etc/default/grub
if [ -f $x ] && ! grep -q '^GRUB_TERMINAL=console' $x
then
	save_config_file $x
	section "Editing $x ..."

	# This is needed for grub-reboot(8) to work
	# (see http://wiki.debian.org/GrubReboot)
	#
	perl -pi -e 's/^(GRUB_DEFAULT)=.*/$1=saved/' $x

	# Show the boot menu for three seconds before booting
	#
	perl -pi -e 's/^(GRUB_TIMEOUT_STYLE)=.*/$1=menu/' $x
	perl -pi -e 's/^(GRUB_TIMEOUT)=.*/$1=3/' $x

	# Remove "quiet", "splash" from the default boot options so that
	# we see all the fun kernel messages on bootup
	#
	perl -pi -e 's/^(GRUB_CMDLINE_LINUX_DEFAULT)=.*/$1=""/' $x

	# Don't use a graphical terminal
	#
	perl -pi -e 's/^#(GRUB_TERMINAL=console)/$1/' $x

	diff_config_file

	# Also needed for grub-reboot(8)
	#
	run_command grub-set-default 0

	update_grub_config
fi

# end customize-parts/grub2.sh
