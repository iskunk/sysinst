# customize-parts/java.sh

# Note: OpenJDK has numerous X11 dependencies!

install_packages <<EOF
	default-jdk
	default-jdk-doc

	$(if_not_installed libxt-dev-)

	$(if_available NO:libbonobo2-common-)

	# This is only needed on Debian stretch
	# (related info at https://bugs.debian.org/893849)
	#
	$(if_available NO:default-java-plugin-)
EOF

install_packages --no-install-recommends ant ant-optional

# end customize-parts/java.sh
