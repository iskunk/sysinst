# customize-parts/firefox.sh
#
# Arguments:
#	SITE_NAME = site name
#	HOME_PAGE_URL = URL of browser's default home page
#	PROXY_AUTOCONFIG_URL = URL for .pac file (optional)
#

for x in \
	firefox \
	firefox-esr \
	firefox-is-not-available
do
	! package_available $x || break
done

install_packages $x

x=$(which_file \
	/etc/firefox/firefox.js \
	/etc/firefox/pref/firefox.js \
	/etc/firefox/syspref.js \
	/etc/firefox-esr/firefox-esr.js \
	/etc/firefox-esr/syspref.js \
	/etc/xul-ext/ubufox.js \
	/etc/iceweasel/pref/iceweasel.js)

if [ -f $x ] && ! fgrep -q "$SITE_NAME" $x
then
	section "Editing $x ..."
	save_config_file $x

	# Is there not a blank line at the end of the file?
	#
	if tail -n 1 $x | grep -q '[^ ]'
	then
		echo >>$x
	fi

	#homepage=file:/usr/local/etc/xul-ext/homepage.properties
	homepage='about:blank'
	[ -z "$HOME_PAGE_URL" ] || homepage="$HOME_PAGE_URL"

	# Preference reference:
	# https://github.com/mozilla/gecko-dev/blob/master/modules/libpref/init/StaticPrefList.yaml

	# TODO: "datareporting...uploadEnabled" may be in the
	# default config already (on Debian it is)

	cat >>$x <<EOF
//
// $SITE_NAME config additions
//

// Set these for use with a local network-based cache
//pref("browser.cache.disk.enable", false);
//pref("browser.cache.offline.enable", false);

// Disable "Recommended by Pocket"
pref("browser.newtabpage.activity-stream.feeds.system.topstories", false);

// Disable "Firefox Home" new tab page (use blank page instead)
pref("browser.newtabpage.enabled", false);

// Set custom homepage URL
pref("browser.startup.homepage", "$homepage");
EOF
	# Debian config already has this
	#
	grep -q 'healthreport.uploadEnabled.*false' $x \
	|| cat >>$x <<EOF

// Disable health report upload
pref("datareporting.healthreport.uploadEnabled", false);
EOF
	cat >>$x <<EOF

// Disable "Privacy-Preserving Attribution (PPA)"
pref("dom.private-attribution.submission.enabled", false);

// Disable "Save to Pocket" toolbar button
pref("extensions.pocket.enabled", false);

// Fonts
pref("font.name.monospace.x-western", "DejaVu Sans Mono");
pref("font.name.sans-serif.x-western", "Arial");
pref("font.name.serif.x-western", "Times New Roman");

// Avoid IDN homograph attacks
pref("network.IDN_show_punycode", true);
EOF
	if [ -n "$PROXY_AUTOCONFIG_URL" ]
	then
		cat >>$x <<EOF

// Web proxy configuration
pref("network.proxy.autoconfig_url", "$PROXY_AUTOCONFIG_URL");
pref("network.proxy.type", 2);	// "Automatic proxy configuration URL"
EOF
	fi

	cat >>$x <<EOF

// Scrollbar behavior and appearance
pref("ui.scrollToClick", 0);	// unlisted
pref("widget.gtk.overlay-scrollbars.enabled", false);
pref("widget.non-native-theme.gtk.scrollbar.allow-buttons", true);
pref("widget.non-native-theme.gtk.scrollbar.round-thumb", false);
pref("widget.non-native-theme.gtk.scrollbar.thumb-size", "1.0");  // 100%
pref("widget.non-native-theme.scrollbar.size.override", 15);
EOF

	diff_config_file
fi

#:if ! package_installed debathena-firefox-wrapper && \
#:   ! package_installed iceweasel
#:then
#:	install_packages libnss3-tools	# dependency
#:
#:	section "Building and installing firefox-wrapper ..."
#:
#:	install_deb_from_source /mnt/sysinst/src/firefox-wrapper-SVN
#:fi

# end customize-parts/firefox.sh
