# customize-parts/nfs.sh

install_packages --no-install-recommends nfs-common

# See https://bugs.launchpad.net/bugs/261711
#
if package_installed portmap
then
	require_debconf_selection \
		portmap \
		'portmap/loopback: true' \
		'Please configure portmap to bind to the loopback address.'
fi

#:x=/etc/default/nfs-common
#:if ! grep -q '^NEED_STATD=no' $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	perl -pi -e 's/^(NEED_STATD)=.*$/$1=no/' $x
#:
#:	diff_config_file
#:fi

# end customize-parts/nfs.sh
