# customize-parts/devel.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site identifier (in lowercase)
#

x=/etc/sysctl.d/10-ptrace.conf
y=/etc/sysctl.d/10-ptrace-$SITE_ID.conf
if [ -f $x ]
then
	if ! egrep -q '^kernel\.yama\.ptrace_scope\s*=\s*0' $x
	then
		save_config_file $x
		section "Editing $x ..."

		perl -pi -e '/^kernel\.yama\.ptrace_scope/ and s/^/#:/' $x

		cat >>$x <<EOF

## $SITE_NAME developer config
kernel.yama.ptrace_scope = 0
EOF

		diff_config_file
	fi

elif [ ! -f $y ]
then
	if fgrep -q kernel.yama.ptrace_scope /etc/sysctl.d/*.conf
	then
		die 'check distro-provided ptrace_scope file in /etc/sysctl.d/'
	fi

	section "Creating $y ..."

	cat >$y <<EOF
# $SITE_NAME developer config

# The PTRACE system is used for debugging.  With it, a single user process
# can attach to any other dumpable process owned by the same user.  In the
# case of malicious software, it is possible to use PTRACE to access
# credentials that exist in memory (re-using existing SSH connections,
# extracting GPG agent information, etc).
#
# A PTRACE scope of "0" is the more permissive mode.  A scope of "1" limits
# PTRACE only to direct child processes (e.g. "gdb name-of-program" and
# "strace -f name-of-program" work, but gdb's "attach" and "strace -fp $PID"
# do not).  The PTRACE scope is ignored when a user has CAP_SYS_PTRACE, so
# "sudo strace -fp $PID" will work as before.
#
kernel.yama.ptrace_scope = 0
EOF

	show_file $y
fi

install_packages build-essential $(if_not_installed libx11-data-)

install_packages bison byacc flex

# Tools needed (or helpful) to build Debian packages
#
install_packages <<EOF
	automake	# specified so that automake1.7 doesn't go in
	autopoint
	cdbs
	#config-package-dev	# needed to build firefox-wrapper
	dctrl-tools
	debhelper
	devscripts
	#fakeroot	# pulled in by build-essential
	$(if_available libc6-prof)
	libtool-bin
	patchutils
	quilt
EOF

# We don't want e.g. "automake1.4", "automake1.10"
#
if dpkg -l | grep ^ii | awk '{print $2}' | grep -q '^automake[0-9]'
then
	die "old version of automake installed"
fi

# Sanity check
#
for p in fakeroot gettext
do
	package_installed $p || die "package \"$p\" not installed"
done

# Ensure that each installed kernel metapackage has a corresponding
# headers metapackage (on 32-bit Debian)
#
for cpu in 486 586 686-pae
do
	if package_installed linux-image-$cpu && ! package_installed linux-headers-$cpu
	then
		install_packages linux-headers-$cpu
	fi
done

# Various development tools
#
install_packages <<EOF
	cmake
	cppcheck
	cscope
	cvs
	doxygen
	equivs
	gcovr
	gdb
	gh
	git
	git-lfs
	git-review
	gitlab-cli
	rr
	subversion
	valgrind

	$(if_not_installed libx11-data-)
EOF

# Great tool for tracking down compiler bugs
#
install_packages creduce

# Documentation
#
install_packages <<EOF
	debconf-doc
	glibc-doc
	#linux-doc		# provides text files, not man pages
	manpages-dev
	manpages-posix-dev	# non-free
	perl-doc
EOF

# Python environment
#
install_packages <<EOF
	brz		# Bazaar client, for Ubuntu work
	python3-dev
	python3-github
	python3-jira	# for JIRA automation
	python3-numpy
	python3-openpyxl
			# reads/writes MS Excel .xlsx files
	python3-pip	# for installing additional stuff
	python3-pytest
	python3-scipy
	python3-setuptools
	python3-urwid	# advanced ncurses UI
	python3-venv

	# needed by seshat-client
	python3-ijson
	python3-jsonschema

	NO:gnome-keyring-
	$(if_not_installed x11-common-)
EOF

# We don't normally use any of these dev libraries, but they are handy to
# have available for third-party stuff we want to build (e.g. KDE4)
#
install_packages <<EOF
	libasound2-dev
#	libaspell-dev	# pulls in dictionaries-common
	libaudiofile-dev
#	libavahi-client-dev	# eventually pulls in dbus-x11
	libboost-dev
	libbz2-dev
	libchm-dev
	libclucene-dev
	libcppunit-dev
	libcunit1-dev	# often used for testing in CMake builds
#	libcups2-dev	# pulls in dbus
#	libdbus-1-dev	# pulls in dbus-x11 via Recommends:
	libdjvulibre-dev
#	libenchant-dev	# eventually pulls in dbus-x11
	libexiv2-dev
	libflac-dev
	libgif-dev
#	libgmm-dev	# not in Debian
	libgmp3-dev
#	libgpgme11-dev	# pulls in X11 libs on Jaunty (pinentry-gtk2)
	libgsl-dev
	libical-dev
	libidn11-dev
#	libjasper-dev	# missing as of Ubuntu Bionic
	libjpeg-dev
	libldap2-dev	# hey, we use LDAP (todo: update to libldap-dev)
	libmpfr-dev	# needed to build GCC
#	libnxcl-dev	# pulls in X11, Qt4, my god!
	libpci-dev
	libraw1394-dev
#	libsane-dev	# eventually pulls in dbus-x11
	libsasl2-dev
#	libsmbclient-dev	# pulls in Samba stuff
	libsndfile1-dev
	libsqlite3-dev
	libssl-dev	# OpenSSL dev libs
#	libsvn-dev	# pulls in MySQL stuff
	libtiff-dev
	libusb-dev
	libvncserver-dev
	libvorbis-dev
	libzip-dev
EOF

# end customize-parts/devel.sh
