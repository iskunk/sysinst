# customize-parts/basic-setup.sh
#
# Arguments:
#	SITE_NAME = site name
#

# Get rid of command-not-found, as it's rarely helpful, and has recently
# started suggesting Snap package installs
#
if package_installed command-not-found
then
	section 'Removing command-not-found ...'

	# https://bugs.launchpad.net/bugs/1877650
	run_command find /var/lib/command-not-found -type f -exec rm -fv {} +

	run_command apt-get $batch --autoremove --purge remove python3-commandnotfound
fi

install_packages <<EOF
	cryptsetup-initramfs
	debconf-utils
	debian-keyring
	debian-archive-keyring
	efitools
	memtest86+
	$(if_available ubuntu-keyring)	# [Debian testing]
	watchdog
	wireguard
EOF

# Needed for our standard LVM-on-LUKS disk encryption setup
#
install_packages --no-install-recommends lvm2

#### Don't use "staff" group for /usr/local

x=/etc/staff-group-for-usr-local
if [ -f $x ]
then
	section 'Negating use of "staff" group for /usr/local/ and /var/local/ ...'

	run_command chmod -R g-ws /usr/local /var/local
	run_command chgrp -R root /usr/local /var/local
	run_command rm -f $x
fi

#### Sysinst-related setup

x=/root/private/config
if [ ! -d $x ]
then
	section "Creating mount point $x for config partition ..."

	run_command mkdir -p $x
	run_command chmod 700 /root/private

	# Normally, install_image adds this line when generating the fstab,
	# but we additionally do so now to prevent Xfce et al. from
	# mounting the partition at the behest of a normal user (which
	# would allow said user to view/modify the critical files therein)

	x=/etc/fstab
	section "Editing $x ..."
	save_config_file $x

	label=config
	test -L /dev/disk/by-label/CONFIG && label=CONFIG

	cat >>$x <<EOF

LABEL=$label	/root/private/config	vfat	noauto	0	0
EOF

	diff_config_file
fi

#### Module blacklist

x=/etc/modprobe.d/blacklist.conf
if [ -f $x ] && grep -q '^blacklist pcspkr$' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's/^(blacklist pcspkr)$/#:$1/' $x

	diff_config_file
fi

#:x=/etc/rc.local
#:if [ "$distro" = ubuntu ] && ! grep -q '^modprobe pcspkr' $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	perl -pwi - $x <<PERLEOF
#:/^exit 0/ && print <<EOF;
#:# $SITE_NAME: Workaround for https://bugs.launchpad.net/bugs/398161
#:#
#:rmmod pcspkr
#:modprobe pcspkr
#:
#:EOF
#:PERLEOF
#:
#:	diff_config_file
#:fi

#### Cron scheduling

# Double-check that this hasn't changed
# (6-7am is a good time as it avoids Cary and Beijing work hours)
#
x=/etc/crontab
if egrep -q '^[0-9]+\s+[^6*]\s' $x
then
	die "jobs running at hourly times other than 6am in $x"
fi

#:x=/etc/crontab
#:if egrep -q '^[0-9]+\s+6\s' $x
#:then
#:	# Sanity check, in case someone fiddles with the default times
#:	#
#:	egrep -q '^[0-9]+\s+[^6*]\s' $x \
#:	&& die "jobs running at hourly times other than 6am in $x"
#:
#:	section "Editing $x ..."
#:	save_config_file $x
#:
#:	# Run daily/weekly/monthly jobs at 3-4am, not 6-7am
#:
#:	perl -pi -e 's/^(\d+\s+)6\b/${1}3/' $x
#:
#:	diff_config_file
#:fi

#### Initramfs config

x=/etc/initramfs-tools/conf.d/resume
if [ -f $x ] && grep -q UUID= $x
then
	section "Editing $x ..."
	save_config_file $x

	# Remove machine-specific device UUID from this file
	# (refer to initramfs.conf(5) for background)
	#
	# This eliminates a "initramfs-tools configuration sets
	# RESUME=... but no matching swap device is available" warning
	# printed during update-initramfs(8) invocations
	#
	perl -pi -e 's/^(RESUME)=.+$/$1=auto/' $x

	diff_config_file

elif [ ! -f $x -a -d $(dirname $x) ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

RESUME=auto
EOF

	show_file $x
fi

#### Disable MOTD news

x=/etc/default/motd-news
if [ -f $x ] && grep -q '^ENABLED=1' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's/^(ENABLED)=.+$/$1=0/' $x

	diff_config_file
fi

x=/var/cache/motd-news
if [ -s $x ]
then
	section "Zapping $x ..."
	: >$x
fi

#### Useful tools

install_packages attr gdisk iptraf-ng screen tmux

#### Fix bash completion

x=/etc/profile.d/bash_completion_fix.sh
if package_installed bash-completion && [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
# $x

if [ -f /etc/profile.d/bash_completion.sh ]
then
	# Disable advanced completion logic for scp(1)
	# as the network I/O involved is ridiculous
	#
	complete -F _longopt scp
fi

# end $x
EOF

	show_file $x
fi

#### Fix /etc/hosts

x=/etc/hosts
if ! fgrep -q 127.0.1.1 $x
then
	# The 127.0.1.1 line is needed in order for "hostname --fqdn" and
	# "dnsdomainname" to return a correct result; see "THE FQDN"
	# section in hostname(1)

	section "Adding 127.0.1.1 line to $x ..."
	save_config_file $x

	h1=$(hostname --fqdn) 2>/dev/null || :
	h2=$(hostname)
	h="${h1:+$h1 }${h2}"

	printf '127.0.1.1\t%s\n' "$h" | prepend_to_file $x

	diff_config_file
fi

# end customize-parts/basic-setup.sh
