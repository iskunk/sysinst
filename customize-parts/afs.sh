# customize-parts/afs.sh

# If there is a commented AFS cache entry in fstab, then uncomment it
#
x=/etc/fstab
if egrep -q '^#.*[[:space:]]/var/cache/openafs[[:space:]]' $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's!^#(.+\s/var/cache/openafs\s.+)$!$1!' $x

	diff_config_file
fi

install_packages <<EOF
	libpam-afs-session
	openafs-client		# will build kernel module via DKMS
	openafs-krb5
EOF

#### AFS configuration

# This bit isn't strictly necessary, but it allows /afs/example.com to
# appear [alongside all the foreign cells] before being accessed for the
# first time.
#
x=/etc/openafs/CellServDB
if ! fgrep -q example.com $x
then
	save_config_file $x
	section "Editing $x ..."

	prepend_to_file $x <<EOF
>example.com		#Example organization (see AFSDB records in DNS)
EOF

	diff_config_file
fi

x=/etc/openafs/afs.conf
if ! grep -q '^AFS_SYSNAME=' $x
then
	save_config_file $x
	section "Editing $x ..."

	case "$(uname -m)" in
		x86_64) sysname="linux64 amd64_linux26" ;;
		*) sysname="linux32 i386_linux26" ;;
	esac

	perl -pi -e 's/^#(AFS_SYSNAME)=.*$/$1="'"$sysname"'"/' $x

	diff_config_file
fi

#### libnss-afspag NSS module

if ! package_installed libnss-afspag
then
	section "Building and installing libnss-afspag ..."

	install_deb_from_source /mnt/sysinst/src/libnss-afspag-SVN
fi

x=/etc/nsswitch.conf
if ! fgrep -q afspag $x
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi -e 's/^(\s*group:.*)$/$1 afspag/' $x

	diff_config_file
fi

# end customize-parts/afs.sh
