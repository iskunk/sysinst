# customize-parts/letsencrypt.sh
#
# Arguments:
#	SITE_NAME = site name
#

install_packages certbot

# Generate self-signed SSL certificate for the default Web site
# (i.e. for requests that do not specify a recognized hostname via
# Server Name Indication [SNI])
#
dir=/root/ssl
if [ ! -d $dir ]
then
	section 'Creating self-signed SSL certificate ...'

	run_command mkdir --mode=700 $dir

	# Generate RSA private key
	run_command openssl genrsa -out $dir/selfsign.key 2048

	# Generate certificate signing request (CSR)
	run_command openssl req -batch -new \
		-key $dir/selfsign.key \
		-out /tmp/selfsign.csr

	# Produce signed certificate from CSR
	run_command openssl x509 -req -days 2000 \
		-in /tmp/selfsign.csr \
		-signkey $dir/selfsign.key \
		-out $dir/selfsign.crt
fi

x=/root/ssl/README
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
The selfsign.{key,crt} files in this directory are a private key and
self-signed SSL certificate for use by the local Web server.

To obtain an initial letsencrypt.org certificate, run

	# certbot certonly [--test-cert]

The webroot location is "/var/www/html".

Refer to /etc/lighttpd/conf-available/95-site-example.conf to configure
Lighttpd with a new SSL-protected site.

Visit https://www.ssllabs.com/ssltest/ to verify that the new site has
been configured correctly.
EOF
fi

# Generate some better Diffie-Hellman parameters
# (Reference: https://weakdh.org/sysadmin.html)
#
x=/etc/ssl/dhparam.pem
if [ ! -f $x ]
then
	section "Creating $x ..."

	run_command openssl dhparam -out $x 2048
fi

# Lighttpd Web server support
#
x=/etc/lighttpd/conf-available/20-ssl-strong.conf
if [ -d $(dirname $x) -a ! -f $x ]
then
	# We need mod_setenv, but no handy conf-available file
	# is provided to enable it, so we'll create our own
	#
	y=/etc/lighttpd/conf-available/10-setenv.conf
	if [ -f $y ]
	then
		fgrep -q "$SITE_NAME config" $y \
		|| die "check for distro-provided $y file"
	else
		section "Creating $y ..."

		cat >$y <<EOF
## $SITE_NAME config

server.modules += ( "mod_setenv" )
EOF

		run_command ln -s ../conf-available/10-setenv.conf /etc/lighttpd/conf-enabled
	fi

	y=/etc/lighttpd/server.pem
	if [ ! -f $y ]
	then
		section "Creating $y ..."

		(umask 077; cat /root/ssl/selfsign.key /root/ssl/selfsign.crt >$y)
	fi

	section "Creating $x ..."

	echo "## $SITE_NAME config" >$x
	cat >>$x <<'EOF'

# References:
#
#	./10-ssl.conf
#	https://cipherli.st/
#	https://raymii.org/s/tutorials/Strong_SSL_Security_On_lighttpd.html
#

$SERVER["socket"] == "0.0.0.0:443" {
	ssl.engine = "enable"
	ssl.pemfile = "/etc/lighttpd/server.pem"
	ssl.cipher-list = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH"
	ssl.honor-cipher-order = "enable"
	ssl.dh-file = "/etc/ssl/dhparam.pem"
	ssl.ec-curve = "secp384r1"
	ssl.use-sslv3 = "disable"

	# These are already the default
	#
	#ssl.use-compression = "disable"
	#ssl.use-sslv2 = "disable"

	setenv.add-response-header = (
	#	"Strict-Transport-Security" => "max-age=63072000; includeSubDomains; preload",
		"X-Frame-Options" => "DENY",
		"X-Content-Type-Options" => "nosniff"
	)
}
EOF

	run_command ln -s ../conf-available/20-ssl-strong.conf /etc/lighttpd/conf-enabled

	y=/etc/lighttpd/conf-available/95-site-example.conf
	[ ! -f $y ] || die "$y exists"

	section "Creating $y ..."

	echo "## $SITE_NAME config" >$y
	cat >$y <<'EOF'

# ~~~ Edit and rename me ~~~

$SERVER["socket"] == "0.0.0.0:443" {
	$HTTP["host"] == "www.example.com" {
		ssl.pemfile = "/etc/letsencrypt/live/www.example.com/combined.pem"
	}
}
EOF
fi

x=/usr/local/sbin/letsencrypt-post-hook
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
#!/bin/sh
# Certbot "post hook" script
#
# Referenced in
#	/etc/letsencrypt/cli.ini
#	/etc/letsencrypt/renewal/*.conf
#
EOF
	cat >$x <<'EOF'

set -e
umask 077

do_reload=no

for key in /etc/letsencrypt/live/*/privkey.pem
do
	test -f $key || continue

	dir=$(dirname $key)

	if test ! -f $dir/combined.pem -o \
		$key -nt $dir/combined.pem -o \
		$dir/fullchain.pem -nt $dir/combined.pem
	then
		cat $key $dir/fullchain.pem >$dir/combined.pem
		do_reload=yes
	fi
done

if [ $do_reload = yes ]
then
	service lighttpd reload
fi
EOF

	run_command chmod 744 $x
fi

x=/etc/letsencrypt/cli.ini
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

keep-until-expiring

post-hook = /usr/local/sbin/letsencrypt-post-hook
EOF
fi

# end customize-parts/letsencrypt.sh
