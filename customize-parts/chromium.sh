# customize-parts/chromium.sh
#
# Arguments:
#	PROXY_AUTOCONFIG_URL = URL for .pac file (optional)
#	SITE_NAME = site name
#	SITE_ID   = site identifier
#

for x in \
	chromium \
	chromium-browser \
	chromium-is-not-available
do
	! package_available $x || break
done

install_packages <<EOF
	$x

	# Don't install the setuid sandbox, as the namespace-based
	# one will be used instead anyway
	#
	$(if_available chromium-sandbox-)

	$(if_not_installed avahi-daemon-)
	$(if_not_installed system-config-printer-)
EOF

#:if update-alternatives --list x-www-browser | grep -q chromium && \
#:   ! update-alternatives --query x-www-browser | grep -q '^Value: /usr/bin/chromium'
#:then
#:	run_command update-alternatives --set x-www-browser /usr/bin/$x
#:fi

#:x=/etc/chromium-browser/customizations/10-$SITE_ID
#:if [ -d /etc/chromium-browser/customizations \
#:     -a ! -f $x \
#:     -a -n "$PROXY_AUTOCONFIG_URL" ]
#:then
#:	section "Creating $x ..."
#:
#:	cat >$x <<EOF
#:# $x
#:
#:CHROMIUM_FLAGS="\${CHROMIUM_FLAGS} --proxy-pac-url=$PROXY_AUTOCONFIG_URL"
#:EOF
#:fi

policy_dir_1=/etc/chromium-browser/policies/managed
policy_dir_2=/etc/chromium-browser/policies/recommended
if [ ! -d $policy_dir_1 -a -d /etc/chromium ]
then
	policy_dir_1=/etc/chromium/policies/managed
	policy_dir_2=/etc/chromium/policies/recommended
	if [ ! -d $policy_dir_1 ]
	then
		run_command mkdir -p $policy_dir_1
		run_command mkdir -p $policy_dir_2
	fi
fi

# Chromium policy configuration is described at
# https://www.chromium.org/administrators/linux-quick-start
# https://cloud.google.com/docs/chrome-enterprise/policies/

x=$policy_dir_1/$SITE_ID.json
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
{
	"AllowOutdatedPlugins": false
}
EOF

	show_file $x
fi

x=$policy_dir_2/$SITE_ID-proxy.json
if [ -n "$PROXY_AUTOCONFIG_URL" -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
{
	"ProxyMode": "pac_script",
	"ProxyPacUrl": "$PROXY_AUTOCONFIG_URL"
}
EOF

	show_file $x
fi

x=/etc/chromium/master_preferences
if [ -f $x ] && ! fgrep -q 'native_file_system_write_guard' $x
then
	section "Editing $x ..."
	save_config_file $x

	# To view current Chromium preferences, run
	# $ python3 -m json.tool ~/.config/chromium/Default/Preferences

	perl -pi -e '/"distribution":/ and $_ = <<EOF . $_;
  // begin '"$SITE_NAME"' addition
  "custom_handlers": {
     "enabled": false
  },
  // end '"$SITE_NAME"' addition
EOF
		' $x

	perl -pi -e '/"default_content_setting_values":/ and $_ .= <<EOF;
        // begin '"$SITE_NAME"' addition
        "clipboard": 2,
        "geolocation": 2,
        "media_stream_camera": 2,
        "media_stream_mic": 2,
        "midi_sysex": 2,
        "native_file_system_write_guard": 2,
        "ppapi_broker": 2,
        "sensors": 2,
        "serial_guard": 2,
        "usb_guard": 2,
        // end '"$SITE_NAME"' addition
EOF
		' $x

	perl -pi -e 's!("homepage":) ".+"!$1 "'"$HOME_PAGE_URL"'"!' $x

	diff_config_file
fi

#:x=$(which_file /etc/chromium-browser/default /etc/chromium/default)
#:if ! fgrep -q proxy-auto-detect $x
#:then
#:	section "Editing $x ..."
#:	save_config_file $x
#:
#:	# Chromium command-line switch references:
#:	#
#:	# man chromium-browser
#:	# http://peter.sh/experiments/chromium-command-line-switches/
#:	# http://src.chromium.org/svn/trunk/src/chrome/common/chrome_switches.cc
#:
#:	perl -pi \
#:		-e 's/^(CHROMIUM_FLAGS)="(.*)"$/$1="$2 --proxy-auto-detect"/;' \
#:		-e 's/^(\w+)="\s+(.*)"$/$1="$2"/' \
#:		$x
#:
#:	cat >>$x <<EOF
#:
#:# $SITE_NAME: Workaround for
#:#
#:#	https://bugs.launchpad.net/bugs/807611
#:#	http://code.google.com/p/chromium/issues/detail?id=25868
#:#
#:# (Chromium crashes with SIGBUS when \$HOME disappears, as can happen with
#:# AFS, due to the disk-cache backend using memory-mapped files in \$HOME.
#:# We work around this by putting the cache on the local disk.)
#:#
#:if [ -n "\$SCRATCH" -a -d "\$SCRATCH" ]
#:then
#:	CHROMIUM_FLAGS="\$CHROMIUM_FLAGS --disk-cache-dir=\$SCRATCH/local/chromium"
#:fi
#:EOF
#:
#:	diff_config_file
#:fi

#:# Set Chromium as the Xubuntu-Xfce default Web browser
#:#
#:x=/etc/xdg/xdg-xubuntu/xfce4/helpers.rc
#:if [ -f $x ] && ! fgrep -q WebBrowser=chromium $x
#:then
#:	section "Editing $x ..."
#:	save_config_file $x
#:
#:	perl -pi -e 's/^(WebBrowser)=.*/$1=chromium-browser/' $x
#:
#:	diff_config_file
#:fi

# end customize-parts/chromium.sh
