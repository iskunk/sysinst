# customize-parts/x11-base.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site identifier (in lowercase)
#

if [ $distro = debian ]; then ####

# Note: fontconfig-config will ask the "Enable bitmapped fonts by default?"
# debconf question twice; see https://bugs.debian.org/648869

# Install a customized Xfce desktop environment
# Last updated for 11/bullseye
#
install_packages <<EOF
	task-xfce-desktop
	abiword
	xcursor-themes
	xscreensaver

	NO:at-spi2-core:*-	# https://bugs.launchpad.net/bugs/1877532
				# (TODO: confirm bug on Debian)
	NO:avahi-daemon-	# not so useful for us
	NO:cups-		# use a remote CUPS server instead
	NO:cups-pk-helper-	# no local CUPS server, so not needed
	NO:hddtemp-		# smartmontools has this covered
	NO:firebird*-common-	# don't need a heavyweight SQL database
	NO:gnome-keyring- NO:libpam-gnome-keyring-
				# horrible default ssh-agent functionality
	NO:icedtea-netx-	# Java browser plug-in = security no-no
	$(if_available NO:libbonobo2-common-)
				# keep the GNOME under control
	NO:light-locker-	# too minimal, use better screensaver
	NO:modemmanager-	# leave my serial ports alone!
	NO:pipewire-		# like PulseAudio, but for video
	$(if_available NO:python-twisted-core-)
				# installs useless daemon(s)
	NO:quodlibet-		# has PulseAudio dependency
	NO:system-config-printer-	# https://bugs.debian.org/863227
	NO:tumbler-		# causes crashes, large images = DoS
	NO:xdg-desktop-portal-	# avoid gratuitous FUSE mounts

	# PulseAudio sucks
	#
	NO:libpulsedsp-
	NO:pavucontrol-
	NO:pulseaudio-
	NO:pulseaudio-utils-
	#NO:xfce4-pulseaudio-plugin-	# hard dependency, cannot decline

	# Install the JRE/JDK later to ensure that LibreOffice is
	# installed (and is installable) without Java support
	#
	$(if_not_installed default-jre-)
	$(if_not_installed default-jre-headless-)
	$(if_not_installed java-common-)
	$(if_not_installed libreoffice-java-common-)

	# Ugh...
	#
	$(if_not_installed systemd-)
	#$(if_not_installed systemd-shim-)
EOF

#:# We can't avoid the installation of light-locker, but we can at least
#:# disable it system-wide
#:#
#:x=/etc/xdg/autostart/light-locker.desktop
#:if [ -f $x ] && ! fgrep -q 'Hidden=true' $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	cat >>$x <<EOF
#:
#:## $SITE_NAME config additions
#:
#:Hidden=true
#:EOF
#:
#:	diff_config_file
#:fi

# https://bugs.debian.org/586700
#
if echo /etc/fonts/conf.d/10-*sub-pixel*.conf | grep -Fq '*'
then
	section 'Disabling subpixel rendering ...'

	x=$(which_file \
		/usr/share/fontconfig/conf.avail/10-no-sub-pixel.conf \
		/usr/share/fontconfig/conf.avail/10-sub-pixel-none.conf \
	)

	run_command ln -s $x /etc/fonts/conf.d/

	# Update debconf selection
	#
	run_command dpkg-reconfigure fontconfig-config
fi

fi # debian ####

if [ $distro = ubuntu ]; then ####

#:x=/tmp/$SITE_ID-xubuntu-hack.ctl
#:if ! package_installed $SITE_ID-xubuntu-hack
#:then
#:	install_packages equivs
#:
#:	section "Creating $SITE_ID-xubuntu-hack package ..."
#:
#:	y='cups, foomatic-db, foomatic-db-compressed-ppds, foomatic-db-engine, foomatic-filters, gdm, openprinting-ppds'
#:
#:	cat >$x <<EOF
#:Section: misc
#:Priority: optional
#:Standards-Version: 3.6.2
#:
#:Package: $SITE_ID-xubuntu-hack
#:Maintainer: Daniel Richard G. <drg@example.com>
#:Provides: $y
#:Conflicts: $y
#:Description: $SITE_NAME package to tweak xubuntu-desktop dependencies
#: This package exists solely to work around the following issues with the
#: xubuntu-desktop metapackage on Ubuntu Lucid:
#: .
#: * https://bugs.launchpad.net/bugs/302272
#: * https://bugs.launchpad.net/bugs/552858
#:EOF
#:	(cd /tmp && run_command equivs-build $x)
#:
#:	run_command dpkg -i /tmp/$SITE_ID-xubuntu-hack_1.0_all.deb
#:fi

# Install the bulk of Xubuntu
# Last updated for 22.04/jammy
#
install_packages <<EOF
	xubuntu-desktop

	xscreensaver
	xfce4-screensaver-	# https://bugs.launchpad.net/bugs/2027917

	NO:at-spi2-core:*-	# https://bugs.launchpad.net/bugs/1877532
	NO:avahi-autoipd-	# don't need another daemon
	NO:avahi-daemon-	# don't need another daemon
	NO:ayatana-indicator-common-
				# https://bugs.launchpad.net/bugs/1973298
	NO:cloud-init-		# not building a cloud system here
	NO:cups-		# use a remote CUPS server instead
	NO:cups-filters-	# no local CUPS server, so not needed
	NO:fwupd-		# firmware updates are not routine
	NO:gnome-keyring- NO:libpam-gnome-keyring-
				# horrible default ssh-agent functionality
	NO:gvfs-fuse-		# avoid weird mount shenanigans
	NO:hddtemp-		# smartmontools has this covered
	$(if_available NO:legacy-printer-app-)
				# don't want printer drivers, network only
	$(if_available NO:libbonobo2-common-)
				# keep the GNOME under control
	NO:light-locker-	# too minimal, use better screensaver
	NO:modemmanager-	# leave my serial ports alone!
	NO:netplan.io-		# echo of $SITE_ID-ubuntu-hack package
	NO:network-manager-pptp-
				# don't need this Microsoft protocol
	NO:pipewire-		# like PulseAudio, but for video
	NO:snapd-		# no thank you, Canonical
	NO:system-config-printer-
				# https://bugs.launchpad.net/bugs/1877528
	NO:ukui-greeter-	# just the standard greeter, please
	$(if_available NO:vala-terminal-)
				# utterly useless (as of Quantal)
	#NO:wader-core-		# same deal as modemmanager
	NO:xdg-desktop-portal-	# avoid gratuitous FUSE mounts
	NO:zeitgeist-		# no thank you, Canonical

	# We want Xfce, not GNOME!
	#
	NO:gnome-control-center-
	NO:gnome-session-*-	# matches "bin" and "common", et al.
	NO:gnome-settings-daemon-common-
	#NO:gnome-software-common-	# can't exclude this in jammy :P
	NO:gnome-system-tools-
	NO:gnome-terminal-

	# Note that some of these need a trailing ":*" because on amd64,
	# APT may try to sneak in the i386 version :P

	# who uses Bluetooth with their PC?
	#
	NO:bluez:*-
	NO:bluez-cups-
	NO:bluez-obexd-

	# PulseAudio sucks
	#
	NO:gstreamer1.0-pulseaudio-
	NO:pavucontrol-
	NO:pulseaudio:*-
	NO:xfce4-pulseaudio-plugin-

	# Unity sucks
	#
	NO:libunity9-
	NO:unity-
	NO:unity-control-center-
	NO:unity-greeter-

	# Don't install this if not already present
	#
	$(if_not_installed dpkg-dev-)
EOF

install_packages xfce4-goodies NO:hddtemp-

x=/etc/X11/Xsession.d/01local_options-fix-$SITE_ID
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

# This file is sourced by Xsession(5), not executed.

# Workaround for the Ubuntu issue described in the following reports:
#
#   https://bugs.launchpad.net/bugs/1922414
#   https://github.com/canonical/lightdm/issues/198
#

if type -t has_option >/dev/null; then
  return
fi

OPTIONS="\$(
  if [ -r "\$OPTIONFILE" ]; then
    cat "\$OPTIONFILE"
  fi
  if [ -d /etc/X11/Xsession.options.d ]; then
    run-parts --list --regex '\.conf\$' /etc/X11/Xsession.options.d | xargs -d '\n' cat
  fi
)"

has_option() {
  # Ensure that a later no-foo overrides an earlier foo
  if [ "\$(echo "\$OPTIONS" | grep -Eo "^(no-)?\$1\>" | tail -n 1)" = "\$1" ]; then
    return 0
  else
    return 1
  fi
}
EOF

	show_file $x
fi

fi # ubuntu ####

# We need this for laptops/Wi-Fi; make sure it got pulled in
#
package_installed network-manager || die 'NetworkManager is not installed'

# Ubuntu doesn't install bitmap fonts by default anymore, but we need them
# for NEdit and other oldies-but-goodies
#
install_packages xfonts-100dpi xfonts-75dpi xfonts-jmk

#### GTK+ config tweaks

x=/etc/gtk-3.0/settings.ini
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<END
## $SITE_NAME config

[Settings]
gtk-overlay-scrolling = false
gtk-primary-button-warps-slider = false
gtk-xft-rgba = none
END

	show_file $x
fi

x=/etc/gtk-3.0/gtk-user.css
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<END
/** $SITE_NAME config **/

/*
 * NOTE: This file must be symlinked from ~/.config/gtk-3.0/gtk.css
 * because GTK+ has no system-level equivalent to that file. The only
 * non-user CSS files it reads are those belonging to themes, and
 * these are not interpreted in the same way.
 */

/* Use traditional scrollbar appearance */

scrollbar {
  -GtkScrollbar-has-backward-stepper: true;
  -GtkScrollbar-has-forward-stepper:  true;
}

scrollbar slider {
  border: 0;
  border-radius: 0;
  min-width:  15px;
  min-height: 15px;
}
END

	show_file $x
fi

x=/etc/profile.d/gtk-$SITE_ID.sh
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<END
## $SITE_NAME config

[ -n "\$DISPLAY" ] || return 0

# Suppress warnings about a missing accessibility bus
#
export NO_AT_BRIDGE=1
END

	show_file $x
fi

#### Xfce config tweaks

x=/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-screensaver.xml
if [ -d $(dirname $x) -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
<?xml version="1.0" encoding="UTF-8"?>

<!-- $SITE_NAME config additions -->

<!-- Don't lock screen by default -->
<channel name="xfce4-screensaver" version="1.0">
  <property name="lock" type="empty">
    <property name="enabled" type="bool" value="false"/>
  </property>
</channel>
EOF

	show_file $x
fi

x=/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml
if [ -d $(dirname $x) -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
<?xml version="1.0" encoding="UTF-8"?>

<!-- $SITE_NAME config additions -->

<channel name="xfwm4" version="1.0">
  <property name="general" type="empty">

    <!-- https://askubuntu.com/questions/479465/how-can-i-disable-alt-scroll-zoom-in-xfce4-xubuntu -->
    <property name="zoom_desktop" type="bool" value="false"/>
    <property name="zoom_pointer" type="bool" value="false"/>

    <!-- Compositing breaks video on some systems -->
    <!--#<property name="use_compositing" type="bool" value="false"/>#-->

  </property>
</channel>
EOF

	show_file $x
fi

#### Screensaver setup

if package_installed xscreensaver
then
	# Only a handful of screensavers are installed by
	# xubuntu-desktop, due to these two packages being in the
	# universe repository, so we install them ourselves. (Refer to
	# https://bugs.launchpad.net/bugs/42890 for the reason why this
	# is necessary)
	#
	install_packages xscreensaver-data-extra xscreensaver-gl-extra

	x=/etc/X11/app-defaults/XScreenSaver
	if [ -f $x ] && ! grep -Eq '^\*unfade:\s+False$' $x
	then
		test -L $x || die "$x is not a symlink"
		require_file $x-gl $x-nogl

		for y in $x-gl $x-nogl
		do
			section "Editing $y ..."
			save_config_file $y

			# Don't do a fade-in on screen saver deactivation
			#
			perl -pi -e '/^\*unfade:/ and s/True/False/' $y

			diff_config_file
		done
	fi
fi

x=/usr/local/bin/if-local-dpy
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<'EOF'
#!/bin/sh
#
# usage: if-local-dpy COMMAND [ARG]...
#
# Run COMMAND only when we're on a local X display
# (as opposed to a remote X session)
#

test -z "$XRDP_SESSION" || exit 0

case "$DISPLAY" in
	:[1-9][0-9] | :[1-9][0-9][0-9]) exit 0 ;;
	'' | :*) ;;
	*) exit 0 ;;
esac

exec /usr/bin/env "$@"
EOF

	run_command chmod 755 $x
	show_file $x
fi

x=/usr/local/bin/if-not-vm
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<'EOF'
#!/bin/sh
#
# usage: if-not-vm COMMAND [ARG]...
#
# Run COMMAND only when running on real hardware
# (as opposed to a [VirtualBox] virtual machine)
#

lspci | grep -Eq ' VGA .* (VMware|VirtualBox) ' && exit 0

exec /usr/bin/env "$@"
EOF

	run_command chmod 755 $x
	show_file $x
fi

# Don't run screensavers when they're not needed
#
for x in \
	/etc/xdg/autostart/xscreensaver.desktop \
	/etc/xdg/autostart/xfce4-screensaver.desktop
do
	if [ -f $x ] && ! fgrep -q if-local-dpy $x
	then
		save_config_file $x
		section "Editing $x ..."

		perl -pi -e 's/^(Exec)=/$1=if-local-dpy if-not-vm /' $x

		diff_config_file
	fi
done

#:for x in /usr/share/dbus-1/services/org.gtk.vfs.*
#:do
#:	[ -f $x ] || continue
#:
#:	if ! fgrep -q if-not-vm $x
#:	then
#:		save_config_file $x
#:		section "Editing $x ..."
#:
#:		perl -pi -e 's,^(Exec)=,$1=/usr/local/bin/if-not-vm ,' $x
#:
#:		diff_config_file
#:	fi
#:done

#### X server setup

x=/etc/X11/xinit/xserverrc
if [ "_$(uname -m)" = _x86_64 ] && ! fgrep -q ulimit $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 'm,/usr/bin/X, && print <<EOF;
# Mitigate X server address-space-exhaustion attacks
#
# https://invisiblethingslab.com/resources/misc-2010/xorg-large-memory-attacks.pdf
#
ulimit -v \$((64 * 1024 * 1024))

EOF
' $x

	diff_config_file
fi

# Don't use "SI:localuser:$USER" xhost access, because it defeats the
# purpose of requiring a valid XAUTHORITY file to connect to the server
#
x=/etc/X11/Xsession.d/35x11-common_xhost-local
if [ -f $x ]
then
	section 'Disabling local-user X server access ...'
	disable_file $x
fi

# Helper script to facilitate running a desktop via remote SSH login
#
x=/usr/local/bin/run-desktop
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<'SYSINST_EOF'
#!/bin/sh

cd

if [ -z "$SSH_CONNECTION" ] || ! expr match "$DISPLAY" localhost: >/dev/null
then
	echo 'This script is for use in an SSH session with X11 forwarding.'
	exit 1
fi

if [ -n "$DESKTOP_SESSION" -o -n "$XDG_CURRENT_DESKTOP" ]
then
	echo 'A desktop session is already running.'
	exit 1
fi

if ! xmessage -buttons '' -timeout 1 'starting desktop'
then
	echo
	echo 'Cannot contact X server.'
	echo '(Is one running? Is SSH X11 forwarding active?)'
	exit 1
fi

rm -f .xsession-errors.old
mv    .xsession-errors .xsession-errors.old

cat <<EOF

* * * * * * * * * * * * * * * * * * * * * * * * *
*                                               *
*     Remote X desktop session is active!       *
*                                               *
*  Please set this terminal aside and leave it  *
*   untouched for the duration of the session   *
*                                               *
* * * * * * * * * * * * * * * * * * * * * * * * *
EOF

x-session-manager </dev/null >$HOME/.xsession-errors 2>&1

# Workaround for https://bugs.launchpad.net/bugs/1577920
#
pkill -HUP -U $USER -t $(tty | cut -d/ -f3-) dbus-launch

xsetroot -gray

echo
echo 'Session complete.'

# EOF
SYSINST_EOF

	run_command chmod 755 $x
	show_file $x
fi

# Disable tumblerd to avoid a serious crash bug (one of many):
# https://bugs.launchpad.net/bugs/1871318
#
# (We can't remove "tumbler" without also removing "xubuntu-desktop")
#
x=/usr/share/dbus-1/services/org.xfce.Tumbler.Thumbnailer1.service
if [ -f $x ]
then
	section 'Disabling tumblerd ...'
	disable_file $x
fi

# end customize-parts/x11-base.sh
