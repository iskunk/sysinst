# customize-parts/zfs.sh

if [ $distro = debian ] && \
   ! package_installed build-essential && \
   ! package_available zfs-modules
then
	die 'Debian requires developer tools to build the ZFS modules'
fi

install_packages zfs-initramfs

require_file /lib/systemd/system/zfs.target

if systemctl --quiet is-enabled zfs.target
then
	# Keep ZFS switched off by default
	#
	section 'Disabling ZFS ...'
	run_command systemctl disable zfs.target
fi

# end customize-parts/zfs.sh
