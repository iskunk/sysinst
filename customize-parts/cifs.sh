# customize-parts/cifs.sh

install_packages <<EOF
	cifs-utils
	smbclient
	NO:winbind-	# needed on Debian

	# Don't install dbus just for this
	#
	$(if_not_installed dbus-)
EOF

# end customize-parts/cifs.sh
