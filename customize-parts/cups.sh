# customize-parts/cups.sh
#
# Arguments:
#	CUPS_SERVER = CUPS print server to use
#	SITE_NAME   = site name
#

install_packages cups-client cups-bsd

x=/etc/cups/client.conf
if [ ! -f $x ]
then
	# Instead of having CUPS clients talk to a local CUPS daemon (which
	# then obtains print queue information from a central CUPS server),
	# we cut out the middleman and have clients talk to the central
	# server directly.

	# Workaround for https://bugs.launchpad.net/bugs/557818
	#
	if [ ! -d /etc/cups ]
	then
		echo
		run_command mkdir /etc/cups
	fi

	section "Creating $x ..."

	cat >$x <<EOF
# $x

ServerName $CUPS_SERVER
Encryption Required
EOF

	show_file $x

elif ! fgrep -q "ServerName $CUPS_SERVER" $x
then
	# In case distros ship with a client.conf file in the future
	#
	die "$x exists but does not contain a customized configuration"
fi

#:x=/etc/cups/cupsd.conf
#:if ! fgrep -q "## $SITE_NAME config" $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	perl -pi -e 's/^(Browsing)\s+Off$/$1 On/' $x
#:
#:	cat >>$x <<EOF
#:
#:## $SITE_NAME config additions
#:BrowseProtocols cups
#:BrowsePoll cups.example.com
#:EOF
#:
#:	diff_config_file
#:fi

# end customize-parts/cups.sh
