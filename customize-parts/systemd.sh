# customize-parts/systemd.sh
#
# Miscellaneous systemd configuration

if package_installed systemd; then ########

x=/var/log/README
if package_installed rsyslog && \
   [ -L $x ] && \
   readlink $x | fgrep -q /usr/share/doc/systemd/README.logs
then
	section "Removing $x as it is not applicable ..."
	run_command rm -f $x
fi

# TMPTIME was so much easier. References:
#
#	https://bugs.debian.org/738862
#	https://bugs.debian.org/795269
#
x=/etc/tmpfiles.d/tmp.conf
if [ -d /etc/tmpfiles.d -a ! -f $x ]
then
	y=/usr/lib/tmpfiles.d/tmp.conf
	require_file $y

	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

# Note: This file overrides $y
# See tmpfiles.d(5) for details

d /tmp 1777 root root 7d
#d /var/tmp 1777 root root 30d
EOF

	show_file $x
fi

x=/etc/default/grub
if ! fgrep -q 'fsck.repair=yes' $x
then
	save_config_file $x
	section "Editing $x ..."

	# Always fix filesystems on boot; refer to systemd-fsck(8)
	#
	perl -pi \
		-e 'if (/^GRUB_CMDLINE_LINUX_DEFAULT=/)' \
		-e '{ s/(=".*)"$/$1 fsck.repair=yes"/; s/=" /="/; }' \
		$x

	#:# Use normal network interface names, please
	#:# (see systemd.link(5), section "NamePolicy="; also
	#:# systemd-udevd(8), section "KERNEL COMMAND LINE")
	#:#
	#:# Note: adding /etc/systemd/network/99-default.link does not work
	#:#
	#:perl -pi \
	#:	-e 's/^(GRUB_CMDLINE_LINUX)="(.*)"$/$1="$2 net.ifnames=0"/;' \
	#:	-e 's/=" /="/' \
	#:	$x

	diff_config_file

	update_grub_config
fi

x=/etc/systemd/journald.conf
if ! fgrep -q 'Storage=none' $x
then
	save_config_file $x
	section "Editing $x ..."

	# Get rid of the binary journal files; they're useless and
	# take up much more disk space than normal text logs
	#
	perl -pi -e 's/^#?(Storage)=.+$/$1=none/' $x

	diff_config_file

	run_command systemctl restart systemd-journald
	run_command rm -rf /var/log/journal/*

	# The content of this file is no longer applicable
	# (only present on Debian)
	#
	y=/var/log/README
	[ ! -L $y ] || run_command rm $y
fi

fi ########

# end customize-parts/systemd.sh
