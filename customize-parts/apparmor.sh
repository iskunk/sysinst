# customize-parts/apparmor.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site identifier
#

package_installed apparmor || die 'apparmor is not installed'

install_packages \
	apparmor-profiles \
	apparmor-profiles-extra \
	apparmor-utils

dir=/etc/apparmor.d

section 'Enabling and enforcing profiles ...'
#
for profile in \
	bin.ping \
	sbin.klogd \
	sbin.syslogd \
	sbin.syslog-ng \
	OUTDATED.usr.bin.chromium-browser \
	usr.bin.evince \
	OUTDATED.usr.bin.firefox \
	usr.sbin.avahi-daemon \
	usr.sbin.dnsmasq \
	usr.sbin.mdnsd \
	usr.sbin.nscd \
	usr.sbin.ntpd \
	usr.sbin.rsyslogd \
	usr.sbin.smbd
do
	x=$dir/disable/$profile
	if [ -L $x ]
	then
		run_command rm -f $x
	fi

	x=$dir/$profile
	[ -f $x ] || continue
	grep -q 'flags=.*complain' $x || continue

	# Eventually, we should not need to edit profile flags:
	# https://bugs.launchpad.net/bugs/1575392
	#
	run_command aa-enforce --no-reload $x
done

# Supposedly fixed
#
x=/etc/apparmor.d/abstractions/nameservice
if false # [ $distro = ubuntu ] && ! fgrep -q '@userdb' $x
then
	section "Editing $x ..."
	save_config_file $x

	cat >>$x <<EOF

  ## $SITE_NAME config additions

  # https://bugs.launchpad.net/bugs/1880841
  unix (bind) type=dgram addr="@userdb-*",
EOF

	diff_config_file
fi

x=/etc/apparmor.d/local/lsb_release
if ! fgrep -q '/usr/bin/cat ' /etc/apparmor.d/lsb_release $x
then
	section "Editing $x ..."
	save_config_file $x

	cat >>$x <<EOF
## $SITE_NAME config

  /usr/bin/cat ixr,
  /usr/bin/cut ixr,
EOF

	diff_config_file
fi

section 'Adding profiles ...'
#
for profile in \
	Xorg \
	chromium_browser \
	firefox \
	usr.sbin.nslcd \
	usr.sbin.ssmtp \
	\
	rpcbind \
	sbin.portmap \
	sbin.rpc.statd \
	usr.bin.fam \
	usr.sbin.in.fingerd \
	usr.sbin.lighttpd \
	usr.sbin.sshd \
	xrdp \
	xrdp-sesman
do
	cn=$dir/$profile
	bn=$(echo "$profile" | sed 's/^.*\.//')

	if [ -f apparmor/$bn.txt ]
	then
		dest=$cn.$SITE_ID
		[ ! -f $dest ] || continue
		run_command cp apparmor/$bn.txt $dest

		if [ ! -f $dir/abi/4.0 ]
		then
			# Need to be compatible with AppArmor 3.x
			perl -pi \
				-e 's!<abi/4\.0>!<abi/3.0>!;' \
				-e 's/^(\s*)(userns,)$/$1#$2/' \
				$dest
		fi

		[ ! -f $cn ] || run_command ln -s ../$profile $dir/disable/
	else
		x=/usr/share/apparmor/extra-profiles/$profile
		require_file $x

		if [ "$distro/$(lsb_release -cs)" = debian/bullseye ]
		then
			# Workaround for Debian bullseye, which comments
			# out all the "include <local/...>" directives
			#
			dest=$cn.fixed
			[ ! -f $dest ] || continue
			echo "include-local fix: $x -> $dest"
			sed 's/## include/#include/' $x >$dest
		else
			dest=$cn
			[ ! -f $dest ] || continue
			run_command ln -s $x $dir/
		fi
	fi

	x=$dir/local/$profile
	if [ ! -f $x ] && grep -q "^ *# *include *<local/$profile>" $dest
	then
		echo "Creating $x ..."
		cat >$x <<EOF
## $SITE_NAME config

# Site-specific additions and overrides for $profile.
# For more details, please see /etc/apparmor.d/local/README.
EOF
	fi
done

# The ubuntu-browsers.d abstractions grant unnecessary and overly broad
# permissions; e.g. "user-files" allows write access to nearly all of
# $HOME. Nullify them to keep the browser profiles screwed down tight.
#
for x in \
	$dir/abstractions/ubuntu-browsers.d/chromium-browser \
	$dir/abstractions/ubuntu-browsers.d/firefox
do
	if [ ! -f $x ]
	then
		section "Creating dummy $x ..."

		cat >$x <<EOF
## $SITE_NAME config

# This file is intentionally left empty.
EOF

		show_file $x

	elif grep -q '^include' $x
	then
		section "Editing $x ..."
		save_config_file $x

		perl -pi -e 's/^(include)/#disable#$1/' $x

		diff_config_file
	fi
done

a=$dir/abstractions/authentication
x=$a.d/libpam-fscrypt
require_file $a
if [ -f $dir/abstractions/libpam-fscrypt ]
then
	die "libpam-fscrypt abstraction now present"

elif [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir -p $(dirname $x)

	cat >$x <<EOF
## $SITE_NAME config

  # libpam-fscrypt

  @{PROC}/@{pid}/mountinfo          r,
  @{run}/fscrypt/                   w,
  @{run}/fscrypt/[0-9]*.count       rwk,

  @{etc_ro}/fscrypt.conf            r,

  /.fscrypt/policies/               r,
  /.fscrypt/policies/*              r,
  /.fscrypt/protectors/             r,
  /.fscrypt/protectors/*            r,
  @{HOMEDIRS}/                      r,
  @{HOMEDIRS}/.fscrypt/policies/    r,
  @{HOMEDIRS}/.fscrypt/policies/*   r,
  @{HOMEDIRS}/.fscrypt/protectors/  r,
  @{HOMEDIRS}/.fscrypt/protectors/* r,
EOF

	show_file $x
fi

# Workaround for https://launchpad.net/bugs/1784499
#
x=/etc/apparmor.d/disable/usr.bin.man
if [ ! -L $x ]
then
	section 'Adding workaround for LP#1784499 ...'
	run_command ln -s ../usr.bin.man $x
fi

# TODO: Submit Debian bug report for this
#
x=/etc/systemd/system/rpcbind.service.d/local.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir $(dirname $x)

	cat >$x <<EOF
## $SITE_NAME config

[Unit]
After=apparmor.service
EOF

	show_file $x
fi

apparmor_version=$(dpkg-query --show apparmor | cut -f2)
update_needed=$(dpkg --compare-versions $apparmor_version lt 4.0 && echo yes || echo no)

# Already merged upstream, waiting for release
# (remove when bookworm+jammy become unsupported)
#
x=/etc/apparmor.d/usr.sbin.nscd
y=/etc/apparmor.d/local/usr.sbin.nscd
z=systemd/notify
require_file $x $y
if [ $update_needed = no ]
then
	if ! grep -q $z $x
	then
		die "$x is missing @{run}/$z rule"
	fi
elif ! grep -q $z $y
then
	section "Editing $y ..."
	save_config_file $y

	cat >>$y <<EOF
## $SITE_NAME config

# Already merged upstream:
# https://gitlab.com/apparmor/apparmor/-/merge_requests/1031

  # needed by unscd
  @{run}/systemd/notify w,
EOF

	diff_config_file
fi

# Already merged upstream, waiting for release
# (remove when bookworm+jammy become unsupported)
#
a=/etc/apparmor.d/abstractions/fonts
x=$a.d/fontconfig-user-cache
require_file $a
if [ $update_needed = no ]
then
	if ! grep -q 'cache.*fontconfig.*mrw' $a
	then
		die "$a does not allow writing to fontconfig user cache"
	fi
elif [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir -p $(dirname $x)

	cat >$x <<EOF
## $SITE_NAME config

# Already merged upstream:
# https://gitlab.com/apparmor/apparmor/-/merge_requests/1059

  owner @{HOME}/.{,cache/}fontconfig/** mrwkl,
EOF

	show_file $x
fi

a=/etc/apparmor.d/abstractions/crypto
x=$a.d/gnutls
require_file $a
if [ $update_needed = no ]
then
	grep -q /gnutls/config $a \
	|| die "$a is missing /etc/gnutls/config rule"
elif [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir -p $(dirname $x)

	cat >$x <<EOF
## $SITE_NAME config

# Already present upstream, but not yet released

  @{etc_ro}/gnutls/config r,
EOF

	show_file $x
fi

section 'Disabling stub profiles ...'
#
for profile in \
	chromium \
	firefox
do
	x=/etc/apparmor.d/$profile
	y=/etc/apparmor.d/disable/$profile

	if [ ! -L $y -a -f $x ] && \
	   grep -q '# This profile allows everything and only exists' $x
	then
		run_command ln -s ../$profile $y
	fi
done

# end customize-parts/apparmor.sh
