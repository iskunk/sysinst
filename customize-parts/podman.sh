# customize-parts/podman.sh
#
# Arguments:
#	REGISTRY  = registry to use for unqualified searches (optional)
#	SITE_NAME = site name
#	SITE_ID   = site identifier
#

install_packages <<EOF
	podman-docker
	rootlesskit	# good wrapper for entering user namespaces

	NO:docker.io-
	#NO:fuse-overlayfs-
EOF

# Disable the podman services, because we will only be running
# containers in rootless mode
#
# (Also: https://github.com/containers/podman/issues/14008)
#
if systemctl --quiet is-enabled podman
then
	for unit in \
		podman \
		podman.socket \
		podman-restart \
		podman-auto-update \
		podman-auto-update.timer
	do
		run_command systemctl stop $unit
		run_command systemctl mask $unit
	done

	# Just let the user service start up on demand,
	# no need to start it at login time
	#
	run_command systemctl --global mask podman
fi

x=/etc/containers/nodocker
if [ ! -f $x ]
then
	y=/usr/bin/docker
	require_file $y
	fgrep -q $x $y || die "$y does not refer to $x"

	# This quells the disclaimer from the emulated docker(1) command
	#
	section "Creating $x ..."
	run_command touch $x
fi

x=/etc/containers/containers.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	# This allows podman to run in a cron job
	# (see https://github.com/containers/podman/issues/5443)
	#
	cat >$x <<EOF
## $SITE_NAME config

[engine]
events_logger = "file"
cgroup_manager = "cgroupfs"
EOF

	show_file $x
fi

fgrep -q "## $SITE_NAME config" $x \
|| die "$x: distro-provided file, please review"

x=/etc/containers/registries.conf.d/$SITE_ID.conf
if [ -n "$REGISTRY" -a ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

unqualified-search-registries = ["$REGISTRY"]

[[registry]]
location = "$REGISTRY"
insecure = false
EOF

	show_file $x
fi

# Note: When running as a user, use "podman unshare rm -rf ..." to delete
# files in the container storage area

x=/etc/containers/storage.conf
if [ ! -f $x -a -d /scratch ]
then
	section "Creating $x ..."

	# https://github.com/containers/storage/blob/main/docs/containers-storage.conf.5.md
	#
	# Note: We need to specify the "driver" option or else we get a
	# warning from running "podman info"
	#
	cat >$x <<EOF
## $SITE_NAME config

[storage]
driver = "overlay"
rootless_storage_path = "/scratch/\$USER/containers/storage"
EOF

	show_file $x

	y=/usr/local/sbin/containers-clear-storage
	section "Creating $y ..."

	# Note: "unshare -r" only maps to the root user, which is not
	# sufficient to delete all files belonging to a user's subuid range
	#
	cat >$y <<'EOF'
#!/bin/sh

for dir in /scratch/*
do
	test -d "$dir" || continue

	uid=$(stat -c '%u' "$dir") || continue
	gid=$(stat -c '%g' "$dir") || continue

	test $uid -ne 0 -a $gid -ne 0 || continue

	grep -q "^$uid:" /etc/subuid || continue

	setpriv \
		--reuid=$uid \
		--regid=$gid \
		--clear-groups \
		--inh-caps=-all \
		--reset-env \
		rootlesskit \
		rm -rf "$dir/containers/storage"
done
EOF
	run_command chmod 755 $y
	show_file $y

	z=/etc/cron.d/podman-$SITE_ID
	section "Creating $z ..."

	cat >$z <<EOF
# Delete all local container storage on startup
# (containers can always be re-downloaded or re-created)
#
@reboot root  $y
EOF

	show_file $z
fi

# A stock version of storage.conf may be provided in the future
#
[ ! -f $x ] || fgrep -q "## $SITE_NAME config" $x || die "$x is not our file"

x=/etc/rsyslog.d/10-podman-$SITE_ID.conf
if package_installed rsyslog && [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

# Send podman-related log entries to a separate file
#
:programname, isequal, "podman" /var/log/containers.log
& stop

# Discard all conmon entries, as these are simply the stdout/stderr of
# running containers
#
:programname, isequal, "conmon" stop
EOF

	show_file $x
fi

x=/etc/logrotate.d/podman-$SITE_ID
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

/var/log/containers.log {
	weekly
	rotate 6
	compress
	compressoptions -9
	delaycompress
	missingok
	notifempty
}
EOF

	show_file $x
fi

# Hopefully a temporary workaround
#
x=/etc/modprobe.d/blacklist-podman-$SITE_ID.conf
if [ ! -f $x ]
then
	package_installed fuse-overlayfs \
	|| die "fuse-overlayfs package is needed"

	section "Creating $x ..."

	cat >$x <<EOF
## $SITE_NAME config

# Workaround for https://github.com/containers/podman/issues/17933
#
# By preventing the "overlay" module from loading, Podman is forced to
# use fuse-overlayfs instead of the native overlay driver. (The latter
# is extremely slow when starting a container with --userns=keep-id)
#
blacklist overlay
EOF

	show_file $x
fi

# QEMU user mode emulation, in conjunction with binfmt-support, allows
# Podman to run foreign-architecture containers
#
install_packages qemu-user-static

# EOF
