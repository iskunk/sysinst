# customize-parts/ubuntu.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site ID (in lowercase)
#
# Neutralize some Ubuntu annoyances
#

if [ $distro = ubuntu ]; then ####

x=/etc/update-motd.d/10-help-text
if [ -f $x ]
then
	section 'Disabling Ubuntu help text at login ...'
	disable_file $x
fi

# List of Ubuntu packages to banish
#
pkgs_verboten='netplan.io ubuntu-advantage-desktop-daemon ubuntu-advantage-tools ubuntu-pro-client ubuntu-pro-client-l10n'

# Provides: and Conflicts: fields for the hack package control file
#
pkgs_provides='netplan.io (= 9.9), ubuntu-advantage-desktop-daemon (= 9.9), ubuntu-advantage-tools (= 99.99), ubuntu-pro-client (= 99.99)'
pkgs_conflicts='netplan.io, ubuntu-advantage-desktop-daemon, ubuntu-advantage-tools, ubuntu-pro-client'

if ! package_installed $SITE_ID-ubuntu-hack
then
	# This approach is similar to using equivs-build(1), but does not
	# require developer tools to be installed.

	dir=/tmp/$SITE_ID-ubuntu-hack
	mkdir -p $dir/DEBIAN
	ctl=$dir/DEBIAN/control
	deb=/tmp/$SITE_ID-ubuntu-hack.deb

	section "Creating $ctl ..."

	cat >$ctl <<EOF
Package: $SITE_ID-ubuntu-hack
Version: 1.0
Architecture: all
Protected: yes
Maintainer: Daniel Richard G. <skunk@iSKUNK.ORG>
Conflicts: $pkgs_conflicts
Provides: $pkgs_provides
Section: misc
Priority: optional
Multi-Arch: foreign
Description: $SITE_NAME package to tweak Ubuntu dependencies
 This package exists solely to substitute for certain undesirable
 Ubuntu-related packages.
EOF

	show_file $ctl

	section "Building and installing $SITE_ID-ubuntu-hack package ..."

	run_command dpkg-deb --build $dir $deb

	run_command dpkg --force-conflicts --install $deb

	# Ensure that nothing (automatically) uninstalls this package
	#
	run_command apt-mark hold $SITE_ID-ubuntu-hack

	# Note: All the installed Provides+Conflicts packages need
	# to be removed in a single apt-get(8) invocation, or else
	# the dependency engine will error out.

	pkgs_to_remove=
	for pkg in $pkgs_verboten
	do
		if package_installed $pkg
		then
			pkgs_to_remove+=" $pkg"
		fi
	done

	if [ -n "$pkgs_to_remove" ]
	then
		run_command apt-get $batch --autoremove --purge remove $pkgs_to_remove
	fi

	disallow_packages $pkgs_verboten

	# For good measure
	#
	x='libnetplan*'
	if package_installed $x
	then
		run_command apt-get $batch --purge remove $x
	fi
fi

# Mark these packages as held back (if not currently installed), to avoid
# the possibility of them being pulled in automatically in the future
#
for pkg in \
	snapd \
	ubuntu-advantage-desktop-daemon \
	ubuntu-advantage-pro \
	ubuntu-advantage-tools \
	ubuntu-pro-client
do
	package_available $pkg || die "unknown package $pkg"

	if package_installed $pkg
	then
		! package_installed $SITE_ID-ubuntu-hack \
		|| die "$pkg package is already installed"

	elif ! apt-mark showhold $pkg | grep -q .
	then
		run_command apt-mark hold $pkg
	fi
done

# Ugly hack to make the software-properties-gtk dialog functional
# without ubuntu-advantage-desktop-daemon
#
x=/usr/local/share/software-properties/UbuntuProPage.fake.py
if package_installed software-properties-gtk && [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir -p $(dirname $x)

	cat >$x <<EOF
# Replacement for UbuntuProPage.py in software-properties-gtk package

def UbuntuProPage(self):
	return None
EOF

	show_file $x

	y=/usr/lib/python3/dist-packages/softwareproperties/gtk/UbuntuProPage.py
	disable_file $y

	run_command ln -s $x $y
fi
#
x=/usr/local/bin/ua
if package_installed software-properties-gtk && [ ! -f $x ]
then
	section "Creating $x ..."

	y=/usr/lib/python3/dist-packages/softwareproperties/gtk/utils.py
	require_file $y

	cat >$x <<EOF
#!/bin/sh
# $x: fake Ubuntu Advantage client

case "\$1,\$2" in
	# called from $y
	'status,--format=json')
	echo '{}'
	exit 0
	;;
esac

echo "\$0: error: this is not the actual Ubuntu Advantage tool"
exit 1

# end $x
EOF

	run_command chmod 755 $x
	show_file $x
fi

# Zap ESM advertising from update-notifier
# Related: https://bugs.launchpad.net/bugs/1992026
#
x=/etc/update-motd.d/90-updates-available
if [ -f $x ] && ! fgrep -q 'Zap any ESM' $x
then
	section "Editing $x ..."
	save_config_file $x

	# The second sed command is taken from
	# https://www.pement.org/sed/sed1line.txt
	# ("delete all CONSECUTIVE blank lines from file")

	perl -pi -e 'if (/cat "\$stamp\"/) { $sq="\047"; $_ = <<EOF . $_;
# '"$SITE_NAME"' config: Zap any ESM-related advertising
if [ -r "\$stamp" ] && grep -q ESM "\$stamp"
then
	sed -i.orig -r $sq/\\b(ESM|Expanded Security Maintenance)\\b/Id$sq "\$stamp"
	# Remove repeated blank lines
	sed -i $sq/^\$/N;/\\n\$/D$sq "\$stamp"
fi

EOF
}' $x

	diff_config_file
fi

x=/etc/cron.daily/apt-lp2003851-$SITE_ID
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<'END'
#!/bin/sh
# Mitigation for https://bugs.launchpad.net/bugs/2003851

pid=$(pgrep -fx -U root 'apt-get -qq -y update') || exit 0

elapsed_time=$(ps -o etimes --no-headers $pid) || exit 0

test $elapsed_time -gt 7200 || exit 0

kill -INT $pid || exit

logger -p daemon.notice -t $(basename $0) "killed hung apt process $pid"
END

	run_command chmod 755 $x
	show_file $x
fi

fi ####

# end customize-parts/ubuntu.sh
