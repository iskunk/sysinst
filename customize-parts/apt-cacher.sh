# customize-parts/apt-cacher.sh
#
# Arguments:
#	CACHE_DIR    = base directory for apt-cacher
#	USE_FOR_SELF = set to "yes" to edit /etc/apt/sources.list to use
#		the newly-installed local apt-cacher server
#

install_packages apt-cacher

test -n "$CACHE_DIR" || die "no value specified for CACHE_DIR"

if [ ! -d "$CACHE_DIR" ]
then
	section "Creating cache dir $CACHE_DIR ..."

	run_command mkdir -pv "$CACHE_DIR"
	run_command chown www-data:www-data "$CACHE_DIR"
fi

x=/etc/apt-cacher/conf.d/local.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	y=/var/run/apt-cacher.pid
	test -f $y || die 'no apt-cacher PID file'

	if kill -0 $(cat $y) 2>/dev/null
	then
		run_command invoke-rc.d apt-cacher stop
	fi

	cat >$x <<EOF
# $x

cache_dir = $CACHE_DIR

#daemon_addr = apt.example.com
#daemon_port = 80

allowed_hosts = *

allowed_locations = PATH_MAP

#checksum = 1

distinct_namespaces = 1

path_map = \
	debian			debian.lcs.mit.edu/debian mirrors.mit.edu/debian http.us.debian.org/debian ; \
	debian-debug		debug.debian.net/debian ; \
	debian-security		security.debian.org ; \
	debian-archive		archive.debian.org/debian ; \
	debian-archive-security	archive.debian.org/debian-security ; \
	\
	ubuntu		www.lug.bu.edu/mirror/ubuntu mirrors.mit.edu/ubuntu us.archive.ubuntu.com/ubuntu ; \
	ubuntu-ddebs	ddebs.ubuntu.com ; \
	ubuntu-extras	extras.ubuntu.com/ubuntu ; \
	ubuntu-old	old-releases.ubuntu.com/ubuntu ; \
	ubuntu-partner	archive.canonical.com/ubuntu ; \
	ubuntu-security	security.ubuntu.com/ubuntu ; \
	launchpad-ppa	ppa.launchpad.net

# end $x
EOF

	run_command invoke-rc.d apt-cacher start
fi

x=/etc/apt/sources.list
if [ "$USE_FOR_SELF" = yes ] && ! fgrep -q '/localhost:3142/' $x
then
	section "Editing $x to use apt-cacher ..."
	save_config_file $x

	perl -pi \
		-e 's,/apt\.example\.com/,/localhost:3142/,;' \
		-e 's,/security\.(debian|ubuntu)\.(com|org)\b,/localhost:3142/$1-security,;' \
		$x

	# NOTE: what about /etc/apt/sources.list.d/* ?

	diff_config_file

	section 'Reloading package lists via apt-cacher ...'
	run_command apt-get --error-on=any update
fi

# end customize-parts/apt-cacher.sh
