# customize-parts/ppa-xtradeb.sh
#
# Enable the XtraDeb PPA for Chromium and Firefox packages on Ubuntu
# (since Canonical now only provides those browsers as "snaps")
#
# https://launchpad.net/~xtradeb/+archive/ubuntu/apps
#
# Arguments:
#	APT_SERVER = hostname of apt-cacher server (if applicable)
#	SITE_ID    = site ID (in lowercase)
#

[ $distro = ubuntu ] || die 'the XtraDeb PPA only supports Ubuntu'

ext=$(grep -q Deb822SourceEntry /usr/lib/python3/dist-packages/softwareproperties/ppa.py && echo sources || echo list)

x=/etc/apt/sources.list.d/xtradeb-ubuntu-apps-$(lsb_release -cs).$ext
if [ ! -f $x ]
then
	section 'Enabling XtraDeb PPA ...'

	run_command add-apt-repository -y -n -s -P ppa:xtradeb/apps

	require_file $x

	if [ -n "$APT_SERVER" ]
	then
		perl -pi.orig -e 's,https://ppa.launchpadcontent.net/xtradeb/apps/ubuntu/?,'"http://$APT_SERVER/ubuntu-ppa-xtradeb," $x
	fi

	perl -pi -e 's/^deb\s+/deb     /' $x

	show_file $x

	run_command apt-get --error-on=any update
fi

x=/etc/apt/preferences.d/10-xtradeb-$SITE_ID.pref
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
Explanation: Allow installation of Chromium and Firefox packages from the
Explanation: XtraDeb apps repository (https://launchpad.net/~xtradeb)
Package: chromium* firefox*
Pin: release o=LP-PPA-xtradeb-apps
Pin-Priority: 999

Explanation: Disallow installation of any other XtraDeb packages
Package: *
Pin: release o=LP-PPA-xtradeb-apps
Pin-Priority: -1
EOF

	show_file $x
fi

x=/etc/apt/apt.conf.d/51unattended-upgrades-$SITE_ID
if [ -f $x ] && ! fgrep -q LP-PPA-xtradeb-apps $x
then
	section "Editing $x ..."
	save_config_file $x

	perl -pi -e '/^};$/ and print <<EOF;

	"origin=LP-PPA-xtradeb-apps,suite=\${distro_codename}";
EOF
' $x

	diff_config_file
fi

# EOF
