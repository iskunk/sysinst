# customize-parts/pam-homecheck.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site identifier (in lowercase)
#

chk=/usr/local/share/security/pam_homecheck.pl
if [ ! -f $chk ]
then
	require_file /lib/*/security/pam_exec.so

	section "Creating $chk ..."

	run_command mkdir -p $(dirname $chk)

	cat >$chk <<'EOF'
#!/usr/bin/perl -T
# pam_homecheck.pl
#
# Run this script from /etc/pam.d/* with the invocation
#
#   account  required  pam_exec.so stdout quiet /path/to/pam_homecheck.pl
#
# to prevent non-root users without a homedir from logging in.
#

use strict;
use warnings;

my $pam_user = $ENV{'PAM_USER'} or die;

exit 0 if $pam_user eq 'root';

my $pam_home = (getpwnam($pam_user))[7] or die;

# Note: This does not check ownership
#
exit 0 if -d $pam_home;

print "Could not chdir to home directory $pam_home: $!\n";
exit 1;

# end pam_homecheck.pl
EOF

	run_command chmod 755 $chk
	show_file $chk
fi

x=/etc/apparmor.d/abstractions/authentication.d/pam_homecheck
if [ ! -f $x ]
then
	section "Creating $x ..."

	run_command mkdir -p $(dirname $x)

	cat >$x <<EOF
## $SITE_NAME config

  $chk ixr,
EOF

	show_file $x
fi

# Note: Don't hook this into PAM using pam-auth-update(8), because we
# don't want to perform this check for *all* services

for service in sshd xrdp-sesman
do
	x=/etc/pam.d/$service
	if [ -f $x ] && ! grep -q pam_homecheck $x
	then
		section "Editing $x ..."
		save_config_file $x

		cat >>$x <<EOF

## $SITE_NAME config additions

# Disallow non-root logins without an accessible homedir
account    required     pam_exec.so stdout quiet $chk
EOF

		diff_config_file
	fi
done

# end customize-parts/pam-homecheck.sh
