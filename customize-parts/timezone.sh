# customize-parts/timezone.sh
#
# Arguments:
#	TIME_ZONE = desired time zone, defaults to "America/New_York"
#

# Note: The /etc/timezone file and /etc/localtime symlink drive the debconf
# setting, not the other way around. Also, selecting "US/Eastern" in this
# way results in a value of "America/New_York" due to Debian config-script
# vagaries.

[ -n "$TIME_ZONE" ] || TIME_ZONE=America/New_York

x=/etc/timezone
if ! fgrep -q "$TIME_ZONE" $x
then
	section "Setting time zone to $TIME_ZONE ..."
	rm -f /etc/localtime
	echo "$TIME_ZONE" >$x
	dpkg-reconfigure -pcritical tzdata
	require_file $x /etc/localtime
fi

# end customize-parts/timezone.sh
