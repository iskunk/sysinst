# customize-parts/virt-manager.sh

install_packages <<EOF
	virt-manager
	libvirt-daemon-system-
	qemu-block-extra-
EOF

# Note: This is currently ignored by virt-manager:
# https://github.com/virt-manager/virt-manager/issues/540
# (but virsh(1) takes heed of it, so it's still useful)
#
x=/etc/libvirt/libvirt.conf
if ! grep -q '^uri_default = .*/session' $x
then
	! grep -q '^ *uri_default' $x \
	|| die "$x has unrecognized uri_default setting"

	section "Editing $x ..."
	save_config_file $x

	perl -pi -e 's,^#?(uri_default) *=.+,$1 = "qemu:///session",' $x

	diff_config_file
fi

# https://bugs.launchpad.net/bugs/2027838
#
x=/etc/profile.d/libvirt-uri.sh
if [ -f $x ]
then
	section "Disabling $x ..."
	disable_file $x
fi

# end customize-parts/virt-manager.sh
