# customize-parts/apt.sh
#
# Configure APT to use our in-house package cache server(s)
#
# Arguments:
#	APT_SERVER = hostname of apt-cacher server (optional)
#	ENABLE_BACKPORTS = set to "yes" to enable backports repositories
#

x=/etc/apt/sources.list

if egrep -q 'http://security\.(debian\.org|ubuntu\.com)' $x
then
	# Save a copy of the original file, in case it is useful later
	#
	run_command cp -p $x $x.orig

	save_config_file $x
	section "Editing $x ..."

	# Comment out any cdrom: lines in sources.list
	#
	perl -pi -e 's/^(deb cdrom:)/#:$1/' $x

	if [ -n "$APT_SERVER" ]
	then
		# security.debian.org/debian-security ->
		#         $APT_SERVER/debian-security
		# volatile.debian.org/debian-volatile ->
		#         $APT_SERVER/debian-volatile
		# *.debian.org/debian -> $APT_SERVER/debian
		#
		perl -pi -e 's,\b(http://)security\.debian\.org/debian-security/? ,${1}'"$APT_SERVER"'/debian-security ,' $x
		perl -pi -e 's,\b(http://)volatile\.debian\.org/debian-volatile/? ,${1}'"$APT_SERVER"'/debian-volatile ,' $x
		perl -pi -e 's,\b(http://)[-\w.]+\.debian\.(net|org)/debian/? ,${1}'"$APT_SERVER"'/debian ,' $x

		# archive.canonical.com/ubuntu * partner ->
		#   $APT_SERVER/ubuntu-partner * partner
		#
		perl -pi -e 's,\b(http://)archive\.canonical\.com/ubuntu (\w+ partner)\b,${1}'"$APT_SERVER"'/ubuntu-partner $2,' $x

		# extras.ubuntu.com/ubuntu   -> $APT_SERVER/ubuntu-extras
		# security.ubuntu.com/ubuntu -> $APT_SERVER/ubuntu-security
		# *.ubuntu.com/ubuntu -> $APT_SERVER/ubuntu
		#
		perl -pi -e 's,\b(http://)(extras|security)\.ubuntu\.com/ubuntu/? ,${1}'"$APT_SERVER"'/ubuntu-$2 ,' $x
		perl -pi -e '! /\b(help\.ubuntu\.com)\b/ && s,\b(http://)[-\w.]+\.ubuntu\.com/ubuntu/? ,${1}'"$APT_SERVER"'/ubuntu ,' $x
	fi

	# Remove trailing slashes from repository URL prefixes
	#
	perl -pi -e 's,\b(deb(?:-src)?\s+http://\S+)/ ,$1 ,' $x

	if [ "_$ENABLE_BACKPORTS" = _yes ]
	then
		if [ $distro = debian ] && \
		   egrep -q '^deb .+ (sid|unstable) ' $x
		then
			warning 'not enabling backports repo on Debian unstable release'
		else
			perl -pi -e 's,^#\s*(deb(?:-src)?\s+http://\S+\s+\w+-backports\s+.+)$,$1,' $x
		fi
	fi

	diff_config_file

	run_command apt-get clean
	run_command apt-get --error-on=any update
	run_command apt-get $batch dist-upgrade
fi

x=/etc/apt/sources.list.d/dbgsym.list

# Note that lsb_release(1) may not be installed yet
#
codename=$(. /etc/os-release && echo $VERSION_CODENAME)
test -n "$codename" || die 'cannot determine release codename'

if [ "$distro" = debian -a ! -f $x ]
then
	# Allow use of debian-debug

	section "Creating $x ..."

	cat >$x <<EOF
## Debian debug symbol archive

#deb https://deb.debian.org/debian-debug $codename-debug main contrib non-free
#deb https://deb.debian.org/debian-security-debug $codename-security-debug main contrib non-free
EOF

	if [ -n "$APT_SERVER" ]
	then
		perl -pi.orig -e '!/^##/ and s,https://deb.debian.org/,'"http://$APT_SERVER/," $x
	fi

	show_file $x
fi

if [ "$distro" = ubuntu -a ! -f $x ]
then
	# Allow use of ddebs.ubuntu.com

	install_packages ubuntu-dbgsym-keyring

	section "Creating $x ..."

	cat >$x <<EOF
## Ubuntu debug symbol archive
## Repository: http://ddebs.ubuntu.com/

#deb http://ddebs.ubuntu.com $codename main restricted universe multiverse
#deb http://ddebs.ubuntu.com $codename-updates main restricted universe multiverse
EOF

	if [ -n "$APT_SERVER" ]
	then
		perl -pi.orig -e '!/^##/ and s,http://ddebs.ubuntu.com/?,'"http://$APT_SERVER/ubuntu-ddebs," $x
	fi

	show_file $x
fi

#:x=/etc/update-manager/meta-release
#:if [ -f $x ] && ! fgrep -q "/$APT_SERVER/" $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	perl -pi -e 'if(m,^\w+\s*=\s*http://changelogs\.ubuntu\.com/,){print"#:$_";s,(http://)[^/]+/,${1}'"$APT_SERVER"'/ubuntu-changelogs/,}' $x
#:
#:	diff_config_file
#:fi

# end customize-parts/apt.sh
