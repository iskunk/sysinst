# customize-parts/baseline.sh

x=ubuntu-standard
if [ $distro = ubuntu ] && ! package_installed $x
then
	install_packages <<EOF
		$x

		# Don't need/want these
		#
		NO:bash-completion-
		NO:command-not-found-
		NO:uuid-runtime-
EOF
fi

# Install some staples that might not be present in a base install
#
list=
for package in \
	apparmor \
	bind9-host \
	bzip2 \
	ca-certificates \
	dash \
	dosfstools \
	fdisk \
	file \
	hdparm \
	less \
	lsb-release \
	lshw \
	lsof \
	man-db \
	mtr-tiny \
	nano \
	netcat-openbsd \
	pciutils \
	psmisc \
	rsync \
	rsyslog \
	strace \
	sudo \
	tcpdump \
	telnet \
	time \
	unattended-upgrades \
	uuid \
	wget \
	xz-utils
do
	if ! package_installed $package
	then
		list="$list $package"
	fi
done
if [ -n "$list" ]
then
	install_packages --no-install-recommends $list
fi

# When using systemd, this package is required for SSH sessions to be
# terminated cleanly on reboot/shutdown. Refer to
#
#	"Terminating SSH sessions cleanly on shutdown/reboot with systemd"
#	in /usr/share/doc/openssh-server/README.Debian.gz
#
if [ $distro = debian ] && package_installed systemd
then
	install_packages libpam-systemd
fi

# Ubuntu used to have these in the base install
#
install_packages perl plocate

# Yes, Martin, we do want an /etc/rc.local file
# (https://bugs.debian.org/858677)
#
x=/etc/rc.local
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0
EOF

	run_command chmod 755 $x
	show_file $x
fi

# end customize-parts/baseline.sh
