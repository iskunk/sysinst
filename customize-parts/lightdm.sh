# customize-parts/lightdm.sh
#
# Arguments:
#	SITE_NAME = site name
#	SITE_ID   = site identifier (in lowercase)
#	LIGHTDM_BG        = hex RGB string (e.g. "#cccccc") or image file
#	LIGHTDM_LOGO_FILE = PNG file for default user image (optional)
#

[ -n "$LIGHTDM_BG" ] || LIGHTDM_BG='#1a3b5a'

# Reference: https://wiki.ubuntu.com/LightDM

x=/usr/local/sbin/lightdm-display-setup
if [ ! -f $x ]
then
	section "Creating $x ..."

	require_file \
		/usr/local/bin/if-local-dpy \
		/usr/local/bin/if-not-vm

	cat >$x <<'EOF'
#!/bin/sh
#
# Disable screen-saving features at the LightDM login screen unless
# we're on a local X display and are not running in a virtual machine
#

need_screensaver=$(if-local-dpy if-not-vm echo YES)

if [ "$need_screensaver" != YES ]
then
	xset -dpms
	xset s off
fi

exit 0
EOF

	run_command chmod 755 $x
	show_file $x
fi

dir=/etc/lightdm/lightdm.conf.d

if [ -d /etc/lightdm -a ! -d $dir ]
then
	# LightDM checks this directory even if it's not present by
	# default (as of Utopic)
	#
	run_command mkdir $dir
fi

x=$dir/90-$SITE_ID.conf
if [ ! -f $x ]
then
	section "Creating $x ..."

	cat >$x <<EOF
[LightDM]
# Don't put ~/.Xauthority file in AFS/NFS homedir
user-authority-in-system-dir = true

[Seat:*]
allow-guest = false
allow-user-switching = false
display-setup-script = /usr/local/sbin/lightdm-display-setup
greeter-hide-users = true
EOF

	show_file $x
fi

x=/etc/lightdm/user-image-$SITE_ID.png
if [ -n "$LIGHTDM_LOGO_FILE" -a ! -f $x ]
then
	section 'Adding LightDM default user image ...'
	run_command cp $LIGHTDM_LOGO_FILE $x

	# Note: This bit can't go into a fragment under
	# /etc/lightdm/lightdm.conf.d/
	#
	y=/etc/lightdm/lightdm-gtk-greeter.conf
	if ! grep -q '^background =' $y
	then
		section "Editing $y ..."
		save_config_file $y

		cat >>$y <<EOF

## $SITE_NAME config additions

background = $LIGHTDM_BG
default-user-image = $x
round-user-image = false
user-background = false
EOF

		diff_config_file
	fi
fi

x=/etc/pam.d/lightdm
if ! fgrep -q 'pam_mail.so' $x
then
	section "Editing $x ..."
	save_config_file $x

	cat >>$x <<EOF

## $SITE_NAME config additions

# Defines the MAIL environment variable
# (fix for https://bugs.launchpad.net/bugs/2027776)
session  optional  pam_mail.so nopen
EOF

	diff_config_file
fi

x=/etc/X11/Xsession.d/01local_profile-$SITE_ID
if [ $distro = debian -a ! -x /usr/sbin/lightdm-session -a ! -f $x ]
then
	section "Creating $x ..."

	# Note: On Ubuntu, shell profile(s) are loaded into an X11
	# session by the /usr/sbin/lightdm-session script, prior to
	# any Xsession.d files. This script is not present on Debian.
	#
	# Related:
	#   https://bugs.launchpad.net/bugs/794315
	#   https://bugs.launchpad.net/bugs/1468832

	cat >$x <<EOF
## $SITE_NAME config

# This file is sourced by Xsession(5), not executed.

# Load shell profile(s) in an X11 session, to match the behavior of
# the Ubuntu lightdm package (see https://bugs.debian.org/636108)

for file in /etc/profile "\$HOME/.profile" /etc/xprofile "\$HOME/.xprofile"; do
  if [ -f "\$file" ]; then
    . "\$file"
  fi
done
EOF

	show_file $x
fi

# end customize-parts/lightdm.sh
