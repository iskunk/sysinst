# customize-parts/monitoring.sh

install_packages <<EOF
	finger
	#fingerd
	#openbsd-inetd
	smartmontools
EOF

x=/etc/inetd.conf
if [ -f $x ] && \
   fgrep -q 'in.fingerd' $x && \
   ! fgrep -q 'in.fingerd -u' $x
then
	save_config_file $x
	section "Editing $x ..."

	# Don't allow fingerd(8) to answer "@host" queries
	#
	perl -pi -e 's/(in\.fingerd)$/$1 -u/' $x

	diff_config_file
fi

# Note: The start_smartd keyword was removed in version 7.0-1
#
x=/etc/default/smartmontools
if grep -q 'start_smartd' $x && ! grep -q '^start_smartd' $x
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi -e 's/^#(start_smartd=)/$1/' $x

	diff_config_file
fi

x=/etc/smartd.conf
if ! grep -q '^DEVICESCAN .* -a' $x
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi -e 's/^# Sample c(onfiguration file for smartd)\b/# C$1/' $x

	# Monitor everything, and log the raw temperature
	#
	perl -pi -e 's!^(DEVICESCAN .*)$!$1  -a -R 194!' $x

	# alternate, every Sunday at 4am: L/../../7/04

	diff_config_file
fi

# end customize-parts/monitoring.sh
