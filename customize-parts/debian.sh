# debian.sh
#
# Enable the Debian stable package repositories on a non-Debian system
#
# Arguments:
#	APT_SERVER = APT server from which to obtain Debian
#	SITE_ID    = site ID
#

[ $distro != debian ] || die "debian.sh is for non-Debian systems only"

stable=bookworm

if echo /etc/apt/trusted.gpg.d/debian-archive-*.gpg | fgrep -q '*'
then
	section 'Adding Debian repository keys to APT config ...'

	run_command ln -s \
		/usr/share/keyrings/debian-archive-$stable-*automatic.gpg \
		/etc/apt/trusted.gpg.d
fi

do_update=no

x=/etc/apt/sources.list.d/debian-$stable-$SITE_ID.list
if [ ! -f $x ]
then
	section "Creating $x ..."
	cat >$x <<EOF
# $x

deb [arch=$arch] $APT_SERVER/debian $stable main
deb [arch=$arch] $APT_SERVER/debian-security $stable/updates main

# end $x
EOF

	show_file $x
	do_update=yes
fi

x=/etc/apt/preferences.d/50debian-$stable-$SITE_ID.pref
if [ ! -f $x ]
then
	section "Creating $x ..."

	# Pin the packages from Debian at a low priority so they
	# stay out of the way, unless called for
	#
	cat >$x <<EOF
# $x

Package: *
Pin: release o=Debian
Pin-Priority: 1

# end $x
EOF

		show_file $x
fi

if [ $do_update = yes ]
then
	run_command apt-get --error-on=any update
fi

# end debian.sh
