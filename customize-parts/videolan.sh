# customize-parts/videolan.sh

if package_installed build-essential
then
	install_packages libdvd-pkg

	if ! package_installed libdvdcss2
	then
		run_command dpkg-reconfigure libdvd-pkg
	fi
fi

# end customize-parts/videolan.sh
