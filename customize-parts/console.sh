# customize-parts/console.sh
#
# Arguments:
#	SITE_NAME = site name
#

x=/etc/default/console-setup
if ! grep -q '^FONTFACE="VGA"' $x
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi \
		-e  's/^(CODESET)=.*$/$1="Lat15"/;' \
		-e 's/^(FONTFACE)=.*$/$1="VGA"/;' \
		$x

	diff_config_file

	run_command dpkg-reconfigure console-setup
fi

x=/etc/default/keyboard
if ! grep -q '^XKBMODEL="pc104"' $x
then
	save_config_file $x
	section "Editing $x ..."

	perl -pi -e 's/^(XKBMODEL)=.*$/$1="pc104"/' $x

	diff_config_file

	run_command dpkg-reconfigure keyboard-configuration
fi

# Note: Combining the two dpkg-reconfigure(8) invocations above doesn't
# merge the calls to update-initramfs(8) (tested in Oneiric)

# Note: Neither of the below config files exist in Debian anymore

#:# Ubuntu Hardy and Debian Lenny use /etc/console-tools/config
#:# Ubuntu Intrepid uses /etc/kbd/config
#:#
#:x=$(which_file /etc/console-tools/config /etc/kbd/config)
#:if ! grep -q '^BLANK_TIME=0$' $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	# Disable screen blanking (useful on keyboard-less servers)
#:	#
#:	perl -pi -e 's/^(BLANK_TIME)=.*$/$1=0/' $x
#:
#:	diff_config_file
#:fi

# Note: Ubuntu (as of xenial) and Debian (as of buster) already disable
# console blanking by default, so the below is no longer needed

#:x=/etc/rc.local
#:if [ $distro = debian ] && ! grep -q '^setterm ' $x
#:then
#:	save_config_file $x
#:	section "Editing $x ..."
#:
#:	# Note: "--powersave off" had to be dropped, and the redirect
#:	# added, because rc.local no longer runs directly on the Linux
#:	# virtual console (thanks to systemd)
#:	#
#:	# TODO: Add --blength/--bfreq options?
#:	#
#:	perl -pwi - $x <<PERLEOF
#:/^exit 0/ && print <<EOF;
#:# $SITE_NAME: Disable console blanking
#:#
#:setterm --term linux --blank 0 --powerdown 0 >/dev/console
#:
#:EOF
#:PERLEOF
#:
#:	diff_config_file
#:fi

# end customize-parts/console.sh
