# customize-parts/mail.sh
#
# Arguments:
#	SITE_ID = site identifier (in lowercase)
#

for mta in \
	dma \
	ssmtp \
	nullmailer \
	NONE
do
	if package_available $mta && debconf-show $mta | grep -q .
	then
		break
	fi
done

if [ $mta != NONE ]
then
	install_packages bsd-mailx $mta
fi

case $mta in

	dma)
	x=/etc/cron.d/dma
	if [ -f $x ]
	then
		# Don't use the bundled cron script, because it runs
		# *every five minutes* and clutters up syslog
		#
		section "Disabling $x ..."
		disable_file $x
	fi
	x=/etc/aliases
	if [ ! -f $x ]
	then
		section "Creating empty $x ..."

		echo '# Mail aliases (see dma(8) for info)' >$x

		show_file $x
	fi
	;;

esac

# end customize-parts/mail.sh
