#!/bin/bash
#
# Autorun script for SystemRescue. This goes in the top-level
# directory of the sysinst config partition.
#
# https://www.system-rescue.org/manual/Run_your_own_scripts_with_autorun/
#

# https://gitlab.com/systemrescue/systemrescue-sources/-/issues/274
#
if mountpoint -q /mnt/autorun
then
	umount /mnt/autorun
fi

# Escape for console access
#
echo
echo 'About to enable sysinst remote access!'
read -p 'If this is not desired, then press Enter within ten seconds: ' -t 10
if [ $? -le 128 ]
then
	echo 'OK, not enabling it.'
	echo
	exit 0
fi
echo 'Proceeding...'
echo

#### Failsafe

x=/root/auto-reboot
cat >$x <<'EOF'
#!/bin/sh
# Reboot if no one logs in via SSH after a time

sleep 9m || exit 0

who | egrep -q '^root +pts/' && exit

exec shutdown -r +1
EOF

chmod 755 $x
systemd-run $x

####

config=/tmp/sysinst-config
mkdir $config || exit
mount -v -o ro /dev/disk/by-label/CONFIG $config || exit

aux=$config/aux
cp='cp -v --preserve=timestamps --no-preserve=mode'

#### Networking

x=$config/system.conf
if [ -f $x ]
then
(
	. $x

	conn_name=$(nmcli -t conn show --active | head -n 1 | cut -d: -f1)
	hostname_alt=$hostname-sysinst

	set -x

	nmcli conn down "$conn_name"
	hostnamectl hostname $hostname_alt
	nmcli conn up "$conn_name"
)
fi

#### sshd setup

for hkey in \
	$aux/ssh_host_ed25519_key* \
	$aux/ssh_host_rsa_key*
do
	test -f $hkey || continue
	$cp $hkey /etc/ssh
done
chmod 600 /etc/ssh/ssh_host_*_key

if [ -f $aux/authorized_keys ]
then
	mkdir -pv -m700 /root/.ssh
	$cp $aux/authorized_keys /root/.ssh
fi

umount -v $config

perl -pi \
	-e 's/^#(Port) \d+$/$1 222/;' \
	-e 's/^#(PasswordAuthentication) \w+$/$1 no/' \
	/etc/ssh/sshd_config

systemctl restart sshd
sleep 1

# Disable firewall
#
systemctl stop iptables
#systemctl stop ip6tables

#sleep 30	# uncomment for debugging

# end autorun
