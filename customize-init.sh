# customize-init.sh

# Set the --auto=N option argument to "0", "1", or "2":
#
#  auto  |   type of    | apt-get  |  debconf   | pause for
#  value | installation | prompts? | questions? | imaging?
# -------+--------------+----------+------------+-----------
#    0   | fully manual |    Y     |     Y      |    Y
#    1   | partly auto  |    N     |     N      |    Y
#    2   | fully auto   |    N     |     N      |    N
#
# (Default setting is 0)
#

umask 022

export LC_ALL=C

set -e	# not that this is terribly useful

die()
{
	echo "fatal: $1" >&2
	exit 1
}

set_debconf_selections()
{
	# Check that the selections are in the correct format
	#
	print_debconf_selections \
	| perl -ne '
		/^$/
		|| /^#/
		|| m:^ [-\w]+ \t [-\w/]+ \t \w+ \t \S([^\t]*\S)? $:x
		|| die "malformed line: selections:$.:\n=> $_";
	' || exit

	# Set the selections
	#
	section 'Setting debconf selections ...'

	local unseen=--unseen
	[ $AUTO -eq 0 ] || unseen=
	print_debconf_selections | debconf-set-selections $unseen

	# For each password selection "foo bar password xyzzy", add a
	# matching "foo bar seen true" line so that debconf doesn't ask
	# for the password again. The existing/default password value is
	# not given when debconf asks the question, which defeats the
	# purpose of preseeding the value in the first place.
	#
	print_debconf_selections \
	| grep '^[0-9a-z]' \
	| awk '{ if ($3 == "password") print $1 "\t" $2 "\tseen\ttrue" }' \
	| debconf-set-selections

	# We should be running for the first time on this system, so
	# let's verify that install_packages() is not being called with
	# any packages that are already installed, or that need not be
	# specified explicitly
	#
	redundant_package_install_check=yes
}

run_part()
{
	local part_file="$1"

	local part_file_bn=$(basename "$part_file")
	local dir_rel=$(dirname "$part_file")
	local dir=$(cd "$dir_rel" && pwd) || exit
	local dir_parent=$(cd "$dir_rel" && cd .. && pwd) || exit

	(cd "$dir_parent" && . "$dir/$part_file_bn") \
	|| die "error in $part_file"
}

section()
{
	echo
	printf "$@"
	echo
}

which_file()
{
	local file=

	for file in "$@"
	do
		if test -f "$file"
		then
			echo "$file"
			return 0
		fi
	done

	die "none found: $*"
}

sort_uniq_words()
{
	echo " $*" \
	| tr '\t ' '\n\n' \
	| grep . \
	| sort -u \
	| tr '\n' ' ' \
	| sed 's/ $//'
}

orig_dir=/root/orig
config_file_path=
config_file_orig=

save_config_file()
{
	local file="$1"

	[ -z "$config_file_path" -a -z "$config_file_orig" ] \
	|| die 'save_config_file(): forgot to call diff_config_file()'

	test -d $orig_dir || mkdir -p $orig_dir

	local dest="$orig_dir$file"
	local gen=0	# generation number

	while [ -f "$dest" ]
	do
		gen=$((gen + 1))
		dest="$orig_dir$file.~$gen"
	done

	local dir="$(dirname "$dest")"
	mkdir -p "$dir"

	cp -fp "$file" "$dest"

	config_file_path="$file"
	config_file_orig="$dest"
}

diff_config_file()
{
	[ -n "$config_file_path" -a -n "$config_file_orig" ] \
	|| die 'diff_config_file(): forgot to call save_config_file()'

	local c=--color
	diff -u    "$config_file_orig" "$config_file_path" >>$orig_dir/DIFF || :
	diff -u $c "$config_file_orig" "$config_file_path" || :

	echo '*** (end diff)'

	config_file_path=
	config_file_orig=
}

show_file()
{
	local file="$1"

	echo '----begin----'
	cat "$file" || exit
	echo '----end------'
}

require_file()
{
	while [ -n "$1" ]
	do
		[ -f "$1" ] || die "missing file: $1"
		shift
	done
}

disable_file()
{
	local file="$1"
	local ext="$2"

	test -n "$ext" || ext=DISABLED

	run_command dpkg-divert --divert $file.$ext --rename $file
}

warning()
{
	printf '\033[33m'	# yellow text

	echo "warning: $1"

	printf '\033[00m'	# back to normal
}

package_available()
{
	apt-cache show "$1" 2>/dev/null | grep -q .
}

# Shortcut for use in calls to install_packages; e.g.
#
#	install_packages \
#		$(if_available foo) \
#		$(if_available bar-) \
#		$(if_available NO:qux-)
#
if_available()
{
	local name="$(echo "$1" | sed 's/^NO://; s/-$//')"
	package_available "$name" && echo "$1"
}

package_installed()
{
	local pkg="$1"

	if echo "$pkg" | tr -cd '*?' | grep -q .
	then
		dpkg-query \
			--showformat='${db:Status-Status}: ${Package}\n' \
			--show "$pkg" 2>/dev/null \
		| grep '^installed:' \
		| grep -q .

	elif echo "$pkg" | tr -cd ':' | grep -q .
	then
		local pkg_noarch=$(echo "$pkg" | sed 's/:.*//')

		test -f "/var/lib/dpkg/info/$pkg.list" -o \
		     -f "/var/lib/dpkg/info/$pkg_noarch.list"
	else
		test -f "/var/lib/dpkg/info/$pkg.list" -o \
		     -f "/var/lib/dpkg/info/$pkg:$arch.list"
	fi
}

# Shortcut for use in calls to install_packages
# (mainly useful with "package-minus" names, like "foo-")
#
if_not_installed()
{
	local name="$(echo "$1" | sed 's/-$//; s/:\*$//')"
	package_installed "$name" || echo "$1"
}

disallow_packages()
{
	local list=
	if [ $# -eq 0 ]
	then
		list=$(perl -pe 's/^\s+//;s/\s*#.*$//' | grep . | tr '\n' ' ')
	else
		list="$*"
	fi

	for p in $list
	do
		case "$p" in
			*-)
			name=$(echo "$p" | sed 's/-$//')
			echo "$name" >>$disallowed_packages_list
			echo "$p"
			;;

			*)
			echo "$p" >>$disallowed_packages_list
			;;
		esac
	done
}

redundant_package_install_check=no
package_variant_sort_opts="$(for i in $(seq 1 20); do echo -k ${i}n,$i; done)"
tty=$(tty) || die 'not running on a tty'

install_packages()
{
	local aptget_opts=
	local redundant_ok=no

	while true
	do
		case "$1" in
			-*) aptget_opts="$aptget_opts $1"; shift ;;
			+redundant-ok) redundant_ok=yes; shift ;;
			*) break ;;
		esac
	done

	local list=
	if [ $# -eq 0 ]
	then
		list=$(perl -pe 's/^\s+//;s/\s*#.*$//' | grep . | tr '\n' ' ')
	else
		list="$*"
	fi

	# Add packages prefixed with "NO:" to the disallow-list
	#
	for p in $list
	do
		case "$p" in
			NO:*)
			name=$(echo "$p" | sed 's/^NO://; s/-$//')
			echo "$name" >>$disallowed_packages_list
			;;
		esac
	done
	list=$(echo "$list" | perl -pe 's/\bNO://g')

	echo
	echo "Installing packages: $list"

	local p=

	if [ "$redundant_package_install_check" = yes -a "$redundant_ok" = no ]
	then
		# First check: Are we installing a package that is
		# already installed?

		local all_installed=yes
		local already_installed_list=

		for p in $list
		do
			case "$p" in
				*-) continue ;;
			esac
			if package_installed $p
			then
				already_installed_list+=" $p"
			else
				all_installed=no
			fi
		done

		if [ $all_installed = yes ]
		then
			warning 'already installed'
			return 0
		elif [ -n "$already_installed_list" ]
		then
			# note: non-empty variable has a leading space
			warning "subset already installed:$already_installed_list"
		fi

		# Second check: If we are installing multiple packages,
		# are some of the packages already dependencies of
		# others, such that the former need not be explicitly
		# requested?
		#
		# For example, if we have packages A, B, and C, and B
		# depends on C, then requesting "A B" would have the
		# same effect as requesting "A B C". We don't have to
		# explicitly request C.

		# e.g. '^Inst (foo|bar|baz|libfoo1\.0) .'
		#
		local regex1="^Inst $(echo $list \
			| sed 's/^ */(/; s/ *$/)/; s/  */|/g; s/\([.+]\)/\\\1/g') ."

		for p in $list
		do
			# e.g. '^Inst libfoo1\.0 .'
			#
			local regex2="^Inst $(echo $p | sed 's/\([.+]\)/\\\1/g') ."

			local dep="$(apt-get $aptget_opts -s install $p \
				| egrep "$regex1" \
				| egrep -v "$regex2" \
				| awk '{ print $2 }' \
				| sort \
				| tr '\n' ' ' \
				| sed 's/ $//')"

			[ -n "$dep" ] || continue

			warning "package \"$p\" already has \"$dep\" as a dependency"
		done
	fi

	# A package name with a trailing hyphen means that it should not
	# be installed (as when it is recommended by some other
	# package). If the package is already installed, however,
	# apt-get will remove it. We want to use this feature only to
	# override Recommends:, and so guard against the other
	# possibility here.
	#
	for p in $list
	do
		case "$p" in
			*-) ;;
			*) continue ;;
		esac

		local name="$(echo "$p" | sed 's/-$//')"

		if package_installed $name
		then
			die "package \"$name\" to not-install is already installed"
		fi
	done

	# Check for newer package variants. For example, if we install
	# libsqlite0-dev, but libsqlite3-dev is available, then we should
	# know about it.
	#
	for p in $list
	do
		case "$p" in
			*-) continue ;;
			lib*[0-9]*) ;;
			*) continue ;;
		esac

		! package_installed "$p" || continue

		local regex="$(echo "^$p\$" | sed 's/+/\\+/g; s/[0-9][0-9]*/[0-9]+/g')"

		local latest="$(apt-cache search --names-only "$regex" \
			| awk '{ print $1 }' \
			| perl -pe 's/(?<=\d)(?=\D)|(?<=\D)(?=\d)/ /g' \
			| sort $package_variant_sort_opts \
			| tr -d ' ' \
			| tail -n 1)"

		if [ "$latest" != "$p" ]
		then
			warning "package \"$p\" appears to have successor \"$latest\""
		fi
	done

	printf '\033[36m'  # cyan text

	# Specify --purge so that when a package is bumped out due to a
	# conflict, it gets purged instead of merely removed
	#
	# Specify --auto-remove so that when Recommends: packages are
	# declined with a trailing hyphen, their dependencies aren't
	# installed (why isn't this the default behavior?)
	#
	# Redirect from terminal is needed for when we pipe in a list of
	# packages to this function on stdin
	#
	apt-get $batch --purge --auto-remove $aptget_opts install $list <$tty
	local status=$?

	printf '\033[00m'  # back to normal

	if [ $status -ne 0 ]
	then
		die "apt-get exited with status $status"
	fi

	# If the package has an AppArmor profile, it gets loaded
	#
	# Note: aa-teardown(8) will "helpfully" mount securityfs on
	# /sys/kernel/security/ for us, so don't run it if this is not
	# already mounted (e.g. inside a chroot environment); see
	# https://bugs.launchpad.net/bugs/1965923
	#
	if mountpoint -q /sys/kernel/security && aa-status --profiled >/dev/null 2>&1
	then
		run_command aa-teardown
		run_command rm -rf /var/cache/apparmor/*
	fi
}

run_command()
{
	local ignore_error=no

	if [ "_$1" = "_--ignore-error" ]
	then
		ignore_error=yes
		shift
	fi

	# Don't use stdin, stdout or stderr
	#
	printf '+ \033[01;37m%s\033[00m\n' "$*" >$tty

	"$@"
	status=$?

	[ $status -eq 0 -o $ignore_error = yes ] \
	|| die "command exited with status $status"
}

prepend_to_file()
{
	local file="$1"

	local p_tmp=/tmp/prepend.$$.tmp

	cat - "$file" >$p_tmp
	cat $p_tmp >"$file"
	rm -f $p_tmp
}

install_deb_from_dsc()
{
	local dsc_file="$1"

	rm -rf /tmp/buildpackage

	su nobody -c '
		log=/tmp/buildpackage.$$.txt
		set -ex
		mkdir /tmp/buildpackage
		cd    /tmp/buildpackage
		dpkg-source -x $1
		cd *-*
		dpkg-buildpackage -rfakeroot -b -d >$log 2>&1
		ls -l $log
	' - $dsc_file

	run_command dpkg -i /tmp/buildpackage/*.deb

	rm -rf /tmp/buildpackage
}

install_deb_from_source()
{
	local top_srcdir="$1"

	rm -rf /tmp/buildpackage

	# Note: We copy the source tree instead of using lndir(1) because
	# some Debian package builds modify their own source files (e.g.
	# auto-updating debian/control from debian/control.in)

	su nobody -c '
		log=/tmp/buildpackage.$$.txt
		set -ex
		mkdir /tmp/buildpackage
		cp -a $1 /tmp/buildpackage/src
		cd /tmp/buildpackage/src
		test -f debian/control || DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes debian/rules debian/control >/dev/null 2>&1
		dpkg-buildpackage -rfakeroot -b -d >$log 2>&1
		ls -l $log
	' - $top_srcdir

	run_command dpkg -i /tmp/buildpackage/*.deb

	rm -rf /tmp/buildpackage
}

require_debconf_selection()
{
	local package="$1"
	local selection="$2"	# e.g. 'package/var: value' regex
	local message="$3"

	local x=
	while ! debconf-show $package | egrep -q "^[ *] $selection\$"
	do
		echo
		echo "**** $message"
		printf '**** Press Enter to continue ... '
		read x

		dpkg-reconfigure $package
	done
}

pause_for_imaging()
{
	local tag="$1"
	local package="$2"
	local level_desc="$3"

	if [ -n "$package" ] && package_installed $package
	then
		# The installation has already progessed past this point
		# in an earlier invocation
		#
		return
	fi

	echo
	echo "**** $level_desc -- installation complete"
	echo "**** Image tag: $tag"

	echo $tag >/tmp/IMAGE_TAG

	local x=/tmp/PAUSE_FOR_IMAGING
	if [ -f $x ]
	then
		echo READY >$x
		echo '**** Waiting for external imaging process to complete ...'
		while ! grep -q DONE $x; do sleep 5; done
		echo
	elif [ $AUTO -lt 2 ]
	then
		echo '**** You may wish to create an image of the system at this point.'
		printf '**** Press Enter to continue ... '
		read x
	fi

	rm -f /tmp/IMAGE_TAG
}

pam_disable_profile()
{
	local profile="$1"

	if [ ! -f "/usr/share/pam-configs/$profile" ]
	then
		echo "error: '$profile' is not a valid PAM profile"
		exit 1
	fi

	local enabled="$(debconf-show libpam-runtime \
		| fgrep 'libpam-runtime/profiles:' \
		| sed 's/.*: //')"

	echo " $enabled," | fgrep -q " $profile," || return 0

	test $(ls -1 /etc/pam.d/common-* | wc -l) -eq 5 \
	|| die "pam_disable_profile(): unexpected number of /etc/pam.d/common-* files"

	section "Disabling the \"$profile\" PAM profile ..."

	local enabled_new="$(echo " $enabled," \
		| sed "s/ $profile,//" \
		| sed 's/^ //; s/,$//')"

	printf 'libpam-runtime\tlibpam-runtime/profiles\tmultiselect\t%s\n' \
		"$enabled_new" \
	| debconf-set-selections

	# This is necessitated by the sorry state of pam-auth-update's
	# current support for automated usage
	# https://bugs.launchpad.net/bugs/682662 (see comment #2)
	#
	rm -f /etc/pam.d/common-*

	pam-auth-update --package --force
}

update_grub_config()
{
	local grub_pkg=grub-pc

	if package_installed grub-efi-amd64
	then
		grub_pkg=grub-efi-amd64
	fi

	section 'Updating GRUB debconf selections and configuration ...'

	# Note 1: This invokes update-grub(8), so we don't have to
	# Note 2: The -pcritical option skips needless questions
	#
	run_command dpkg-reconfigure -pcritical $grub_pkg

	rm -f /etc/default/grub.ucf-dist
}

customize_fini()
{
	[ -n "$config_file_path" -o -n "$config_file_orig" ] \
		&& die 'customize_fini(): forgot to call diff_config_file()'

	if package_installed build-essential
	then
		test $DEVEL = yes \
		|| die 'build-essential is installed despite --devel=no'
	else
		test $DEVEL = no \
		|| die 'build-essential is NOT installed despite --devel=yes'
	fi

	section 'Wrapping it up ...'

	run_command apt-get clean
	run_command apt-get $batch --purge autoremove

	# Do some after-the-fact checking on the configuration selections
	# in print_debconf_selections(). We check that (1) the selections
	# are actually used for the packages installed on the system, (2)
	# the selections have the right type of value, and (3) none of the
	# selections redundantly specify a default value.

	section 'Checking debconf selections ...'

	(print_debconf_selections
	 echo EOF
	 cat /var/lib/dpkg/info/*.templates) \
	| perl -we '
	my (%sel_unused, %sel_type, %sel_value);
	my ($name, $type, $value);

	while (<>)
	{
		chomp;
		/^EOF$/ && last;
		(/^$/ || /^#/) && next;
		(undef, $name, $type, $value) = split(/\t/, $_, 4);
		$sel_unused{$name} = 1;
		$sel_type{$name} = $type;
		$sel_value{$name} = $value;
	}

	undef $name;

	while (<>)
	{
		(/^ / || /^(Choices|Description)\b/) && next;

		chomp;

		if (/^Template:\s+(\S+)$/)
		{
			$name = $1;
			undef $type;
			undef $value;
		}

		/^Type:\s+(\S+)$/ && ($type = $1);
		/^Default:\s+(.*)$/ && ($value = $1);

		if (defined($name) && defined($type) && defined($sel_type{$name}))
		{
			$sel_type{$name} eq $type || print "Template \"$name\" has wrong type \"$sel_type{$name}\", should be \"$type\"\n";

			if (defined($value))
			{
				$sel_value{$name} eq $value && print "Template \"$name\" already has a default value of \"$value\"\n";
				undef $value;
			}

			delete $sel_unused{$name};
		}
	}

	foreach $name (keys %sel_unused)
	{
		print "Template \"$name\" is not used\n";
	}
	'

	echo
	echo 'Checking for disallowed packages ...'

	sort -u $disallowed_packages_list \
	| while read pkg
	do
		dpkg-query \
			--showformat='${db:Status-Status}: ${Package}\n' \
			--show "$pkg" 2>/dev/null \
		| grep '^installed:' || :
	done

	local list=/var/tmp/package-urls.txt
	apt-get -d -y --print-uris --reinstall install \
		$(dpkg --get-selections | awk '{ print $1 }') \
	| grep "^'http://" \
	| cut -d"'" -f2 \
	>$list

	echo
	echo "APT URLs for (most) installed packages are in $list"

	rm -f /tmp/PAUSE_FOR_IMAGING

	sync
	sync
	sync
}

###########################################################################

#### Parse command-line args

AUTO=0
DEVEL=yes

tmp=$(getopt -o 'a:d:h' --long 'auto:,devel:,help' -n "$0" -- "$@") || exit
eval set -- "$tmp"
unset tmp

while :
do
	case "$1" in
		-a|--auto)
		AUTO="$2"
		shift 2
		;;

		-d|--devel)
		DEVEL="$2"
		shift 2
		;;

		-h|--help)
		# TODO: elaborate help text
		echo "usage: $0 [--auto=N] [--devel={yes|no}]"
		exit 0
		;;

		--)
		shift
		break
		;;
	esac
done

case "$AUTO" in
	0|1|2) ;;
	*) die "invalid argument '$AUTO' to --auto option" ;;
esac

case "$DEVEL" in
	yes|no) ;;
	*) die "invalid argument '$DEVEL' to --devel option" ;;
esac

#### Check if we're running directly on a live-boot system

if mount | grep -q '^/cow on / '
then
	die 'root filesystem belongs to a live-boot environment!'
fi

#### Check if /home is a mount point

if mountpoint -q /home
then
	cat <<EOF
**** Warning: /home is currently a mount point.
**** This is undesirable for creating system images, as
**** directories created under /home will not be saved.
EOF
	printf '**** Press Enter to continue ... '
	read x
fi

#### Initialize the disallowed-packages list

export disallowed_packages_list=/tmp/disallowed-packages.txt
: >$disallowed_packages_list

#### Determine which distribution and architecture we are running on

# NOTE: lsb_release(1) might not be installed yet

distro=
x=/etc/apt/sources.list

if egrep -q '^deb .*/debian/? ' $x
then
	distro=debian

elif egrep -q '^deb .*/ubuntu/? ' $x
then
	distro=ubuntu
fi

if [ -z "$distro" ]
then
	die 'could not determine what Linux distribution we are running on'
fi

arch=$(dpkg --print-architecture)

#### Set automation level

batch=

case "$AUTO" in
	0) x='no' ;;
	1) x='some' ;;
	2) x='full' ;;
	*) die 'internal error' ;;
esac
echo "Running with --auto=$AUTO ($x automation)"

if [ $AUTO -ge 1 ]
then
	batch=-y
	export DEBIAN_FRONTEND=noninteractive
	#export DEBIAN_PRIORITY=critical
fi

#### Disable services which could interfere with customization

if pgrep cron >/dev/null
then
	section 'Stopping some services ...'

	run_command /etc/init.d/cron stop

	x=/etc/init.d/unattended-upgrades
	[ ! -x $x ] || run_command $x stop

	x=/etc/init.d/apparmor
	if [ -x $x ] && mountpoint -q /sys/kernel/security
	then
		run_command $x stop

		# https://bugs.launchpad.net/bugs/1878333
		#
		run_command aa-teardown
		section 'Clearing out old AppArmor cache ...'
		run_command rm -rf /var/cache/apparmor/*
	fi
fi

# end customize-init.sh
