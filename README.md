# SYSINST: System installation framework

This framework serves the following purposes:

* Create Debian/Ubuntu base system images (`util/sysbootstrap`), as a 
  faster/easier alternative to running the OS installers in a "minimal 
  install" mode

* Customize a system with particular packages, configuration tweaks, etc. 
  (e.g. `customize-skunk`)

* Create deployable images of a customized system (`util/make_image`)

* Install images onto a system (`util/install_image`)

* Sign scripts and images for secure deployment (`signing/*`)


Customization is performed using shell script fragments 
(`customize-parts/*`) with a set of supporting shell functions (in 
`customize-init.sh`) that are called from a top-level script (e.g. 
`customize-skunk`). The goal is not only to automate the process of 
installing packages and applying modifications to the system, but also to 
capture knowledge about the process in a more precise manner than written 
notes.
